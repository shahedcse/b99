<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sadmin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Admin');
    }

    public function dashboard() {
        if (!in_array($this->session->userdata('user_role'), array(1, 3))) :
            redirect('home');
        endif;
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Admin Dashoboard";

        $data['allmessage'] = $this->db->query("SELECT COUNT(id) AS totl FROM messages")->row()->totl;
        $data['alluser'] = $this->db->query("SELECT COUNT(id) AS totl FROM users")->row()->totl;
        $data['allblog'] = $this->db->query("SELECT COUNT(id) AS totl FROM blog")->row()->totl;
        $data['alldonor'] = $this->db->query("SELECT COUNT(id) AS totl FROM users WHERE blood_donor='1'")->row()->totl;
        $monthfromdate = date('Y-m', strtotime(date('Y-m') . " -11 month"));
        $data['totaldata'] = $data['period'] = [];

        for ($m = 0; $m <= 11; $m++) {
            $subval = '+' . $m . 'month';

            $data_month_query = date('Y-m', strtotime($monthfromdate . " $subval"));
            $data_month_show = date('M-y', strtotime($monthfromdate . " $subval"));

            $totalParcels = $this->db
                            ->select('COUNT(1) as total')
                            ->from('users')
                            ->where('status', 1)
                            ->where("DATE_FORMAT(created_date, '%Y-%m') = '$data_month_query' ")
                            ->get()
                            ->row()->total;

            array_push($data['totaldata'], $totalParcels);
            array_push($data['period'], "$data_month_show");
        }


        $this->load->view('web/header', $data);
        $this->load->view('admin/dashboard', $data);
        $this->load->view('web/footer', $data);
    }

    public function event() {
        if (!in_array($this->session->userdata('user_role'), array(1, 3))) :
            redirect('home');
        endif;
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Event";
        $data['all_event'] = $this->db->query("SELECT  * FROM event order by id DESC")->result();

        $this->load->view('web/header', $data);
        $this->load->view('admin/event_list', $data);
        $this->load->view('web/footer', $data);
    }

    public function user_panel() {
        if (!in_array($this->session->userdata('user_role'), array(1))) :
            redirect('home');
        endif;
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "System User";
        $data['use_role'] = $this->session->userdata("user_role");
        $data['role'] = $this->db->query("SELECT * FROM userrole")->result();
        $data['member'] = $this->db->query("SELECT * FROM users where  status ='1' AND member_id !='' AND role IN(2) order by name ")->result();
        $data['allmember'] = $this->db->query("SELECT * FROM users where  status ='1' AND role IN(1,3) order by name ")->result();

        $this->load->view('web/header', $data);
        $this->load->view('admin/user_panel', $data);
        $this->load->view('web/footer', $data);
    }

    function add_system_user() {
        if (!in_array($this->session->userdata('user_role'), array(1))) :
            redirect('home');
        endif;

        $userid = $this->input->post('member_name');
        $userrole = $this->input->post('user_role');
        $roleData = array(
            'role' => $userrole
        );
        $this->db->where('id', $userid);
        $status = $this->db->update('users', $roleData);
        if ($status):
            $this->session->set_userdata('add', 'Add Syetem user Successfully');
        else:
            $this->session->set_userdata('notadd', 'Add system user failed');
        endif;
        redirect('Sadmin/user_panel');
    }

    function change_role() {
        if (!in_array($this->session->userdata('user_role'), array(1))) :
            redirect('home');
        endif;

        $userid = $this->input->post('user_id');
        $userrole = $this->input->post('user_role');

        $roleData = array(
            'role' => $userrole
        );
        $this->db->where('id', $userid);
        $status = $this->db->update('users', $roleData);
        if ($status):
            $this->session->set_userdata('add', 'Role Changed Successfully');
        else:
            $this->session->set_userdata('notadd', 'Role Changed   failed');
        endif;
        redirect('Sadmin/user_panel');
    }

    function delete_event() {
        if (!in_array($this->session->userdata('user_role'), array(1, 3))) :
            redirect('home');
        endif;
        $id = $this->input->post('event_id');
        $this->db->where('id', $id);
        $status = $this->db->delete('event');
        if ($status):
            $this->session->set_userdata('add', ' deleted Successfull');
        else:
            $this->session->set_userdata('notadd', 'deletion  failed');
        endif;
        redirect('Sadmin/event');
    }

    public function add_event() {
        if (!in_array($this->session->userdata('user_role'), array(1, 3))) :
            redirect('home');
        endif;
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Event";


        $this->load->view('web/header', $data);
        $this->load->view('admin/addevent', $data);
        $this->load->view('web/footer', $data);
    }

    public function edit_event() {
        if (!in_array($this->session->userdata('user_role'), array(1, 3))) :
            redirect('home');
        endif;
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Event Edit";
        $id = $this->input->get('id');
        $data['all_info'] = $this->db->query("SELECT * FROM event WHERE id='$id'")->row();

        $this->load->view('web/header', $data);
        $this->load->view('admin/editevent', $data);
        $this->load->view('web/footer', $data);
    }

    public function messages() {
        if (!in_array($this->session->userdata('user_role'), array(1, 3))) :
            redirect('home');
        endif;
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Message";

        $data['all_msg'] = $this->db->query("SELECT * FROM messages order by id DESC")->result();
        $this->load->view('web/header', $data);
        $this->load->view('admin/message_list', $data);
        $this->load->view('web/footer', $data);
    }

    public function messages_reply() {
        if (!in_array($this->session->userdata('user_role'), array(1, 3))) :
            redirect('home');
        endif;
        $data['base_url'] = $this->config->item('base_url');
        $user_id = $this->session->userdata('user_id');
        $smsid = $this->input->post('smsid');
        $smsreply = $this->input->post('smsreply');
        $query1 = $this->db->query("SELECT * FROM messages WHERE id='$smsid'");
        $dataresult = $query1->row();

        $updateData = array(
            'replied_text' => $smsreply,
            'replied_by' => $user_id
        );
        $this->db->where('id', $smsid);
        $status = $this->db->update('messages', $updateData);

        $to_number = (int) 880 . $dataresult->phone;
        $message = "$smsreply " . ' .From SSC-99';
        $queryString = [
            'ApiKey' => '9WSQ1tffrnrrjNOICNu1NGohCHqHnUS5tZtfKlfUHmE=',
            'ClientId' => 'd0ea432b-1c9c-4d3e-8879-ab8712cba7de',
            'SenderId' => 'batch99',
            'Message' => $message,
            'MobileNumber' => $to_number,
            'Is_Unicode' => 'false',
            'Is_Flash' => 'false'
        ];

        $requestresult = $this->callApi('GET', 'http://188.138.33.132:6005/api/v2/SendSMS', $queryString);



        if ($status):
            $this->session->set_userdata('add', 'SMS Sent Successfull');
        else:
            $this->session->set_userdata('notadd', 'SMS Sent failed');
        endif;
        redirect('Sadmin/messages');
    }

    function getDetailsData() {
        if (!in_array($this->session->userdata('user_role'), array(1, 3))) :
            redirect('home');
        endif;

        $id = $this->input->post('id');
        $query1 = $this->db->query("SELECT * FROM messages WHERE id='$id'");
        $dataresult = $query1->row();
        $outputData = array(
            'detailsdata' => $dataresult
        );
        $statusData = array(
            'status' => 1
        );
        $this->db->where('id', $id);
        $this->db->update('messages', $statusData);

        echo json_encode($outputData);
    }

    public function allblog() {
        if (in_array($this->session->userdata('user_role'), array(1, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "Blog List";
            $user_id = $this->session->userdata('user_id');
            $data['user_role'] = $this->session->userdata('user_role');
            $user_role = $data['user_role'];
            $data['all_blogadmin'] = $this->db->query("SELECT blog.*,users.name,blog_category.category_name FROM blog JOIN users ON users.id=blog.created_by join blog_category ON blog_category.id=blog.blog_category order by id desc")->result();


            $this->load->view('web/header', $data);
            $this->load->view('admin/adminblog_list', $data);
            $this->load->view('web/footer', $data);
        else:
            redirect('home');
        endif;
    }

    function insert_event() {
        if (in_array($this->session->userdata('user_role'), array(1, 3))) :
            $id = $this->session->userdata('user_id');
            $target_dir = "assets/images/blogimg/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $imgFile = $_FILES['fileToUpload']['name'];


            if (empty($imgFile)) :
                $image_path = '';
            else:
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                    $image_path = basename($_FILES["fileToUpload"]["name"]);
                else:
                    $data['error'] = "Sorry, there was an error uploading your file";
                endif;
            endif;

            $eventData = array(
                'event_tilte' => $this->input->post('event_tilte'),
                'place' => $this->input->post('place'),
                'file' => $image_path,
                'status' => $this->input->post('status'),
                'details' => $this->input->post('summernote'),
                'added_by' => $id,
                'date' => date('Y-m-d')
            );

            $status = $this->db->insert('event', $eventData);

            if ($status):
                $this->session->set_userdata('add', 'added Successfull');
            else:
                $this->session->set_userdata('notadd', 'added failed');
            endif;
            redirect('Sadmin/event');
        else:
            redirect('home');
        endif;
    }

    function update_event() {
        if (in_array($this->session->userdata('user_role'), array(1, 3))) :
            $id = $this->input->post('event_id');
            $target_dir = "assets/images/blogimg/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $imgFile = $_FILES['fileToUpload']['name'];


            if (empty($imgFile)) :
                $image_path = '';
            else:
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                    $image_path = basename($_FILES["fileToUpload"]["name"]);
                else:
                    $data['error'] = "Sorry, there was an error uploading your file";
                endif;
            endif;
            if (empty($imgFile)) :
                $eventData = array(
                    'event_tilte' => $this->input->post('event_tilte'),
                    'place' => $this->input->post('place'),
                    'status' => $this->input->post('status'),
                    'details' => $this->input->post('summernote')
                );
            else:
                $eventData = array(
                    'event_tilte' => $this->input->post('event_tilte'),
                    'place' => $this->input->post('place'),
                    'status' => $this->input->post('status'),
                    'file' => $image_path,
                    'details' => $this->input->post('summernote')
                );
            endif;


            $this->db->where('id', $id);
            $status = $this->db->update('event', $eventData);

            if ($status):
                $this->session->set_userdata('add', 'Upadte Successfull');
            else:
                $this->session->set_userdata('notadd', 'Upadte failed');
            endif;
            redirect('Sadmin/event');
        else:
            redirect('home');
        endif;
    }

    public function callApi($method, $url, $data = false) {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        /** Optional Authentication: */
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        if (!curl_errno($curl)) {
            $result = json_decode($result);
            $result = json_encode($result, JSON_PRETTY_PRINT);
        }

        curl_close($curl);
        return $result;
    }

}
