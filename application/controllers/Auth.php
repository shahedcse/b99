<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Admin');
    }

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Login";


        $this->load->view('web/header', $data);
        $this->load->view('web/login', $data);
        $this->load->view('web/footer', $data);
    }

    function check_verification2() {
        $code = $this->input->post('verification_code');
        $userQuery = $this->db->query("SELECT verification_code,id,email,phone FROM users WHERE verification_code = '$code'");
        if ($userQuery->num_rows() > 0):
            $userData = array(
                'status' => 1
            );
            $id = $userQuery->row()->id;
            $this->db->where('id', $id);
            $status = $this->db->update('users', $userData);

            if ($status):
                $this->session->set_userdata('add', 'Your request  submited successfully,please wait untill admin verification');
            else:
                $this->session->set_userdata('login_error', 'Your request submission failed');
            endif;
            redirect('Auth');
        // $email = $userQuery->row()->email;
        // $phone = $userQuery->row()->phone;
        // $this->login_withregistration($phone, $email);

        else:
            $this->session->set_userdata('notadd', 'Verification Code Not Matched.');
            redirect('Auth/registration_final');

        endif;
    }

    public function login_withregistration($phone, $email) {
        $userphone = $phone;
        $uemial = $email;
        $userstatus = 1;
        $data['phone'] = $userphone;
        $queryResult = $this->Admin->login($data);
        if ($queryResult) :
            $userdbstatus = $queryResult->status;
            $this->session->set_userdata('user_name', $queryResult->name);
            $dbemail = $queryResult->email;

            if ($userdbstatus == $userstatus && $dbemail == $uemial):
                $this->session->set_userdata('user_id', $queryResult->id);
                $this->session->set_userdata("user_role", $queryResult->role);
                $this->session->set_userdata("user_name", $queryResult->name);
                $this->session->set_userdata('image_path', $queryResult->image_path);
                $this->session->set_userdata('add', 'Your registration is successfully Completed.');
                redirect('Auth/profile');
            endif;
        else:
            $this->session->set_userdata('login_error', 'Something error is occured in your registration .');
            redirect('Auth');
        endif;
    }

    public function login() {
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $userphone = $this->input->post('phone');

        if ($this->form_validation->run() == FALSE):
            $this->session->set_userdata('login_error', 'Please Enter phone & email correctly');
            redirect('Auth');
        else:

            $uemial = $this->input->post('email');
            $userstatus = 1;
            $data['phone'] = $userphone;
            $queryResult = $this->Admin->login($data);

            if ($queryResult) :
                $memberid = $queryResult->member_id;
                if (!empty($memberid)):
                    $userdbstatus = $queryResult->status;
                    $this->session->set_userdata('user_name', $queryResult->name);
                    $dbemail = $queryResult->email;

                    if ($userdbstatus == $userstatus && $dbemail == $uemial):
                        $this->session->set_userdata('user_id', $queryResult->id);
                        $this->session->set_userdata("user_role", $queryResult->role);
                        $this->session->set_userdata("user_name", $queryResult->name);
                        $this->session->set_userdata('image_path', $queryResult->image_path);
                        if ($this->session->userdata("user_role") == 1):
                            redirect('Sadmin/dashboard');
                        else:
                            redirect('Auth/profile');
                        endif;

                    else:
                        $this->session->set_userdata('login_error', 'Please Check phone & Email.');
                        redirect('Auth');
                    endif;
                else:
                    $this->session->set_userdata('login_error', 'Your Account is not verified yet.please wait untill verification.');
                    redirect('Auth');
                endif;
            else:
                $this->session->set_userdata('login_error', 'Not a Valid User .');
                redirect('Auth');
            endif;
        endif;
    }

    function profile() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "My profile";
            $data['divisionall'] = $this->db->query("SELECT * FROM division_govt order by divisionname ASC")->result();
            $data['blood'] = $this->db->query("SELECT * FROM allblood_group order by blood_group ASC")->result();
            $id = $this->session->userdata('user_id');
            $data['allinfo'] = $this->db->query("SELECT * FROM users where id='$id'")->row();

            $this->load->view('web/header', $data);
            $this->load->view('admin/my_profile', $data);
            $this->load->view('web/footer', $data);
        else:
            redirect('home');
        endif;
    }

    function update_profile() {
        if (in_array($this->session->userdata('user_role'), array(1, 2, 3))) :
            $data['base_url'] = $this->config->item('base_url');
            $data['page_title'] = "Update profile";
            $data['divisionall'] = $this->db->query("SELECT * FROM division_govt order by divisionname ASC")->result();
            $data['districtall'] = $this->db->query("SELECT * FROM district_govt order by districtname ASC")->result();
            $data['blood'] = $this->db->query("SELECT * FROM allblood_group order by blood_group ASC")->result();
            $data['profession'] = $this->db->query("SELECT * FROM profession ")->result();
            $data['profession_field'] = $this->db->query("SELECT * FROM profession_field ")->result();
            $id = $this->session->userdata('user_id');
            $data['allinfo'] = $this->db->query("SELECT * FROM users where id='$id'")->row();

            $this->load->view('web/header', $data);
            $this->load->view('admin/update_profile', $data);
            $this->load->view('web/footer', $data);
        else:
            redirect('home');
        endif;
    }

    public function registration() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Registration";
        $data['divisionall'] = $this->db->query("SELECT * FROM division_govt order by divisionname ASC")->result();
        $data['districtall'] = $this->db->query("SELECT * FROM district_govt order by districtname ASC")->result();
        $data['blood'] = $this->db->query("SELECT * FROM allblood_group order by blood_group ASC")->result();
        $data['profession'] = $this->db->query("SELECT * FROM profession ")->result();
        $data['profession_field'] = $this->db->query("SELECT * FROM profession_field ")->result();
        $this->load->view('web/header', $data);
        $this->load->view('web/registration', $data);
        $this->load->view('web/footer', $data);
    }

    function check_verification() {

        $code = $this->input->post('verification_code');
        $userQuery = $this->db->query("SELECT verification_code,id FROM users WHERE verification_code = '$code'");
        if ($userQuery->num_rows() > 0):
            $userData = array(
                'status' => 1
            );
            $id = $userQuery->row()->id;
            $this->db->where('id', $id);
            $this->db->update('users', $userData);
            echo 'mass';
        else:
            echo 'dontmass';
        endif;
    }

    function check_phone() {
        $phone = $this->input->post('phone');
        $userQuery = $this->db->query("SELECT phone FROM users WHERE phone = '$phone'");
        if ($userQuery->num_rows() > 0):
            echo 'mass';
        else:
            echo 'dontmass';
        endif;
    }

    function check_email() {
        $email = $this->input->post('email');
        $userQuery = $this->db->query("SELECT phone FROM users WHERE email = '$email'");
        if ($userQuery->num_rows() > 0):
            echo 'mass';
        else:
            echo 'dontmass';
        endif;
    }

    function insert_regidata() {
        $characters = '012345';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $charactersLength; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $target_dir = "assets/images/webimg/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imgFile = $_FILES['fileToUpload']['name'];


        if (empty($imgFile)) :
            $image_path = 'nofile.jpg';
        else:
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                $image_path = basename($_FILES["fileToUpload"]["name"]);
            else:
                $data['error'] = "Sorry, there was an error uploading your file";
            endif;
        endif;


        $target_dir2 = "assets/images/doc/";
        $target_file2 = $target_dir2 . basename($_FILES["fileToUpload2"]["name"]);
        $imgFile2 = $_FILES['fileToUpload2']['name'];


        if (empty($imgFile2)) :
            $image_path2 = '';
        else:
            if (move_uploaded_file($_FILES["fileToUpload2"]["tmp_name"], $target_file2)) :
                $image_path2 = basename($_FILES["fileToUpload2"]["name"]);
            else:
                $data['error'] = "Sorry, there was an error uploading your file";
            endif;
        endif;
        $dob = $this->input->post('dob');
        $userData = array(
            'name' => $this->input->post('fullName'),
            'gender' => $this->input->post('gender'),
            'edu_board' => $this->input->post('edu_board'),
            'present_loc' => $this->input->post('present_loc'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone_no'),
            'division' => $this->input->post('gov_division'),
            'district' => $this->input->post('gov_district'),
            'school' => $this->input->post('school_name'),
            'present_address' => $this->input->post('present_address'),
            'fblink' => $this->input->post('fblink'),
            'permanent_address' => $this->input->post('permanent_address'),
            'ac_group' => $this->input->post('group'),
            'dob' => date('Y-m-d', strtotime($dob)),
            'Profession' => $this->input->post('Profession'),
            'profession_field' => $this->input->post('profession_field'),
            'specialization' => $this->input->post('specialization'),
            'personal_doc' => $image_path2,
            'status' => 0,
            'role' => 2,
            'verification_code' => $randomString,
            'blood_donor' => $this->input->post('blood_donor'),
            'blood_group' => $this->input->post('blood_group'),
            'image_path' => $image_path,
            'created_date' => date('Y-m-d')
        );
//        echo '<pre>';
//        print_r($userData);
//        exit();
        $status = $this->db->insert('users', $userData);
        //sms Code

        $to_number = (int) 88 . $this->input->post('phone_no');
        $message = "Your OTP Verification code from SSC99 is - $randomString .Please set it for final verification.Thank you";
        $queryString = [
            'ApiKey' => '9WSQ1tffrnrrjNOICNu1NGohCHqHnUS5tZtfKlfUHmE=',
            'ClientId' => 'd0ea432b-1c9c-4d3e-8879-ab8712cba7de',
            'SenderId' => 'batch99',
            'Message' => $message,
            'MobileNumber' => $to_number,
            'Is_Unicode' => 'false',
            'Is_Flash' => 'false'
        ];

        $requestresult = $this->callApi('GET', 'http://188.138.33.132:6005/api/v2/SendSMS', $queryString);
        //Email Sending Code
        $name = $this->input->post('fullName');
        $emailto = $this->input->post('email');

        require FCPATH . 'vendor/autoload.php';
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("info@ssc99.org", "SSC-99");
        $email->setSubject('OTP Code' . $name);
        $email->addTo("$emailto", "SSC-99");

        $email->addContent("text/html", "Dear $name,  <br><br> 
        A new OTP verification code is generated for your registration in ssc99.Your OTP code is - $randomString .Please Set it for Final Verification .  <br> <br> 
        
        (NB: This is an automatic generated email. Please do not reply to this email) ");

        $sendgrid = new \SendGrid('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');

        try {
            $response = $sendgrid->send($email);
            //     print $response->statusCode() . "\n";
            //   print_r($response->headers());
            //   print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
        redirect('Auth/registration_final');
    }

    public function registration_final() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "verification";
        $getcode = $this->input->get('code');
        $final_code = substr($getcode, 9, -4);
        $data['code'] = $final_code;


        $this->load->view('web/header', $data);
        $this->load->view('web/registration_veri', $data);
        $this->load->view('web/footer', $data);
    }

    public function callApi($method, $url, $data = false) {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        /** Optional Authentication: */
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        if (!curl_errno($curl)) {
            $result = json_decode($result);
            $result = json_encode($result, JSON_PRETTY_PRINT);
        }

        curl_close($curl);
        return $result;
    }

    function update_profiledata() {
        $id = $this->session->userdata('user_id');
        $target_dir = "assets/images/webimg/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imgFile = $_FILES['fileToUpload']['name'];


        if (empty($imgFile)) :
            $image_path = 'nofile.jpg';
        else:
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) :
                $image_path = basename($_FILES["fileToUpload"]["name"]);
            else:
                $data['error'] = "Sorry, there was an error uploading your file";
            endif;
        endif;
        $dob = $this->input->post('dob');
        if (empty($imgFile)) :
            $userData = array(
                'name' => $this->input->post('fullName'),
                'gender' => $this->input->post('gender'),
                'edu_board' => $this->input->post('edu_board'),
                'present_loc' => $this->input->post('present_loc'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone_no'),
                'school' => $this->input->post('school_name'),
                'present_address' => $this->input->post('present_address'),
                'fblink' => $this->input->post('fblink'),
                'permanent_address' => $this->input->post('permanent_address'),
                'ac_group' => $this->input->post('group'),
                'dob' => date('Y-m-d', strtotime($dob)),
                'Profession' => $this->input->post('Profession'),
                'profession_field' => $this->input->post('profession_field'),
                'specialization' => $this->input->post('specialization'),
                'blood_donor' => $this->input->post('blood_donor'),
                'blood_group' => $this->input->post('blood_group')
            );
        else:
            $userData = array(
                'name' => $this->input->post('fullName'),
                'gender' => $this->input->post('gender'),
                'edu_board' => $this->input->post('edu_board'),
                'present_loc' => $this->input->post('present_loc'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone_no'),
                'school' => $this->input->post('school_name'),
                'present_address' => $this->input->post('present_address'),
                'fblink' => $this->input->post('fblink'),
                'permanent_address' => $this->input->post('permanent_address'),
                'ac_group' => $this->input->post('group'),
                'dob' => date('Y-m-d', strtotime($dob)),
                'Profession' => $this->input->post('Profession'),
                'profession_field' => $this->input->post('profession_field'),
                'specialization' => $this->input->post('specialization'),
                'blood_donor' => $this->input->post('blood_donor'),
                'blood_group' => $this->input->post('blood_group'),
                'image_path' => $image_path
            );
        endif;
        $this->db->where('id', $id);
        $status = $this->db->update('users', $userData);

        if ($status):
            //email
            $this->session->set_userdata('add', 'Your profile update is Successfull');
        else:
            $this->session->set_userdata('notadd', 'Your profile update is failed');
        endif;
        redirect('Auth/profile');
    }

    function getdistrict() {
        $division_id = $this->input->post('division');
        if (is_int((int) $division_id)):
            $query = $this->db->query("SELECT * FROM `district_govt` WHERE `divisionid`='$division_id' ");
            if ($query->num_rows() > 0):
                $queryresult = $query->result();
                echo '<option value="">-- Select District --</option>';
                foreach ($queryresult as $data):
                    echo '<option value="' . $data->id . '">' . $data->districtname . '</option>';
                endforeach;
            else:
                echo '<option value = "">no data found</option>';
            endif;
        else:
            echo '<option value = "">no data found</option>';
        endif;
    }

    function logout() {

        if (($this->session->userdata('user_role') != NULL) && ($this->session->userdata('user_name') != NULL )):
// $logdetails = "Logout successfully";
            $this->session->unset_userdata('user_name');
            $this->session->unset_userdata('user_role');
            $this->session->unset_userdata("image_path");
            $this->session->sess_destroy();
            $this->session->set_userdata('login_error', 'Sucessfully Log Out');
            redirect('Auth');
        else:
            redirect('Auth');
        endif;
    }

}
