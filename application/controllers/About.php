<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "About us";


        $this->load->view('web/header', $data);
        $this->load->view('web/aboutus', $data);
        $this->load->view('web/footer', $data);
    }

    public function rules() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Group Rules";


        $this->load->view('web/header', $data);
        $this->load->view('web/group_rules', $data);
        $this->load->view('web/footer', $data);
    }

    public function Support() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Support";


        $this->load->view('web/header', $data);
        $this->load->view('web/support', $data);
        $this->load->view('web/footer', $data);
    }

    function send_message() {
        $name = $this->input->get('name');
        $phone = $this->input->get('phone');
        $message = $this->input->get('message');

        $messageData = [
            'name' => $name,
            'phone' => $phone,
            'message' => $message,
            'date' => date("Y-m-d")
        ];

        $status = $this->db
                ->insert('messages', $messageData);
    }

    public function Donation() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Make Donation";


        $this->load->view('web/header', $data);
        $this->load->view('web/donation', $data);
        $this->load->view('web/footer', $data);
    }

}
