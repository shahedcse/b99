<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller {

    public function image_gallery() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "image_gallery";

        $data['all_images'] = $this->db->query("SELECT * FROM images order by id desc")->result();
        $this->load->view('web/header', $data);
        $this->load->view('web/image_gallery', $data);
        $this->load->view('web/footer', $data);
    }
    
     public function videos() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Videos";

        
        $this->load->view('web/header', $data);
        $this->load->view('web/videos', $data);
        $this->load->view('web/footer', $data);
    }

}
