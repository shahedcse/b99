<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Batch SSC-99 ";
        $data['all_images'] = $this->db->query("SELECT * FROM images order by id desc LIMIT 10")->result();
        $data['all_event'] = $this->db->query("SELECT * FROM event WHERE status='1' order by id desc ")->result();
        $data['all_blog'] = $this->db->query("SELECT blog.*,users.name FROM blog JOIN users ON users.id=blog.created_by order by id desc LIMIT 3")->result();

        $this->load->view('web/header', $data);
        $this->load->view('web/home', $data);
        $this->load->view('web/footer', $data);
    }

    public function View_event() {
        $data['base_url'] = $this->config->item('base_url');
        $data['page_title'] = "Event";
        $id = $this->input->get('id');
        $data['eventdetails'] = $this->db->query("SELECT * FROM event where id='$id'")->row();


        $this->load->view('web/header', $data);
        $this->load->view('web/event_view', $data);
        $this->load->view('web/footer', $data);
    }

}
