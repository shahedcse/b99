<!DOCTYPE html>
<div class="content-wrapper" style="margin-top: 50px;">
    <div class="container">
        <div class="alumni-directory">
            <?php
            if ($this->session->userdata('add')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('notadd')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                $this->session->unset_userdata('notadd');
            endif;
            ?>
            <div class="top-section">
                <div class="row">
                    <div class="title-page text-left col-md-6 col-sm-12 col-xs-12">
                        <h4 class="text-regular"> All News & Event list</h4>
                    </div>
                    <div class="search-alumni-directory text-right col-md-6 col-sm-12 col-xs-12">
                        <form class="navbar-form no-margin no-padding">
                            <a href="<?= base_url('Sadmin/add_event'); ?>">
                                <button type="button" class=" bg-color-theme text-center text-regular">Add New</button>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
            <div class="alumni-directory-content">

                <table id="example" class="table table-striped table-bordered" >
                    <thead style="background-color: honeydew;">
                        <tr>
                            <th>SI.</th>
                            <th>Event title</th>
                            <th>Published Date</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($all_event as $value): ?>
                            <tr>
                                <td><?= $value->id; ?></td>
                                <td><?= $value->event_tilte; ?></td>
                                <td><?= $value->date; ?></td>
                                <td>
                                    <a href="<?= base_url('Sadmin/edit_event?id=' . $value->id); ?>">
                                        <button type="button"class="btn-success">Edit/View</button>
                                    </a>
                                    <?php if ($this->session->userdata("user_role") == '1'): ?>
                                        <a href="#" onclick="show_deletemodal('<?= $value->id; ?>');">
                                            <button type="button"class="btn-danger">Delete</button>
                                        </a>
                                    <?php endif; ?>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="delete_modal" class="modal fade " role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url('Sadmin/delete_event'); ?>" method="POST">
                <div class="modal-header" style="background-color:honeydew;">
                    <b> Alert !!</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="event_id" name="event_id" class="form-control">
                    <p style="font-size: 25px;color: red;">Are you sure,You want to delete this ?</p><br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">YES</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>

    </div>
</div>
<script>
    function show_deletemodal(id) {
        var event_id = id;
        $('#event_id').val(event_id);
        $('#delete_modal').modal('show');
    }
</script>



