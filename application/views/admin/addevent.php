<style>
    .form-group{
        text-align: left;
    }

</style>
<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="account-title">
                <h4 class="heading-light">Add New Event/News:</h4>
            </div>

            <div class=" col-md-10 col-lg-offset-1" style="border:1px solid grey; border-radius: 20px;   padding: 15px;">
                <form action="<?= base_url('Sadmin/insert_event'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">

                        <label class="col-md-4 control-label">Title :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="event_tilte" name="event_tilte" placeholder="event_tilte" class="form-control" required="true" value="" type="text"></div>
                        </div><br>
                    </div><br>


                    <div class="form-group">
                        <label class="col-md-4 control-label">Event/News Place :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="place" name="place" placeholder="Event place" class="form-control"  value="" type="text"></div>
                        </div><br>
                    </div><br>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Attach File:</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="fileToUpload" name="fileToUpload"  class="form-control"  type="file"></div>
                        </div><br>
                    </div><br>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Status:</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="selectpicker form-control"id="status" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>

                                </select>
                            </div>
                        </div><br>
                    </div><br>
                    <div class="form-group">

                        <label class="col-md-2 control-label">Event/News Details :<span style="color:red">*</span></label>
                        <div class="col-md-9">
                            <textarea class="form-control"placeholder="" type="text" id="summernote" name="summernote">
                          Write event/News Details...
                            </textarea>
                            <center>
                                <button  type="Submit"   title="publish" class="btn btn-success ">Publish</button>
                            </center>
                        </div><br>

                    </div><br>

                </form>

            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function() {
        $('#summernote').summernote();
    });
    $('document').ready(function()
    {
        $('textarea').each(function() {
            $(this).val($(this).val().trim());
        }
        );
    });
</script>



