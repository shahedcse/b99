<style>
    #container {
        min-width: 320px;
        max-width: 600px;
        margin: 0 auto;
    }
</style>
<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="col-md-12 " style="border:1px solid black; padding: 1px;">
                <div class="col-md-9 col-lg-offset-2">
                    <div class="alumni-dashboard">
                        <div class="container">
                            <div class="area-content">
                                <div class="row ">
                                    <center>
                                        <div class="col-md-2" style="background-color:#79de8b ">
                                            <div class="icon mail-icon"></div>
                                            <h4 class="heading-regular"> <?= $allmessage ?><br>
                                                Total Message </h4>
                                        </div>
                                        <div class="col-md-2 " style="background-color:#e6e080 ">
                                            <div class="icon group-icon"></div>
                                            <h4 class="heading-regular"><?= $alluser ?><br> Total Member </h4>
                                        </div>
                                        <div class="col-md-2 " style="background-color:#8af0d5 ">
                                            <div class="icon group-icon"></div>
                                            <h4 class="heading-regular"><?= $alldonor ?><br> Donor Member </h4>
                                        </div>
                                        <div class="col-md-2" style="background-color:#cb9cda ">
                                            <div class="icon  mail-icon"></div>
                                            <h4 class="heading-regular"> <?= $allblog ?><br> Total Blog </h4>
                                        </div>
                                    </center>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-lg-offset-2" >
                    <div id="container22"></div>
                    <button id="plain">Plain</button>
                    <button id="inverted">Inverted</button>
                    <button id="polar">Polar</button>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
    var chart = Highcharts.chart('container22', {
        title: {
            text: 'Month Wise Registered Member'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [<?php foreach ($period as $p): ?> '<?= $p ?>', <?php endforeach; ?>]
        },
        series: [{
                type: 'column',
                colorByPoint: true,
                data: [<?= implode(',', $totaldata) ?>],
                showInLegend: false
            }]

    });


    $('#plain').click(function() {
        chart.update({
            chart: {
                inverted: false,
                polar: false
            },
            subtitle: {
                text: 'Plain'
            }
        });
    });

    $('#inverted').click(function() {
        chart.update({
            chart: {
                inverted: true,
                polar: false
            },
            subtitle: {
                text: 'Inverted'
            }
        });
    });

    $('#polar').click(function() {
        chart.update({
            chart: {
                inverted: false,
                polar: true
            },
            subtitle: {
                text: 'Polar'
            }
        });
    });

</script>


