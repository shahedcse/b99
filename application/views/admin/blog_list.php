<!DOCTYPE html>
<div class="content-wrapper" style="margin-top: 50px;">
    <div class="container">
        <div class="alumni-directory">
            <?php
            if ($this->session->userdata('add')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('notadd')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                $this->session->unset_userdata('notadd');
            endif;
            ?>
            <div class="top-section">
                <div class="row">
                    <div class="title-page text-left col-md-6 col-sm-12 col-xs-12">
                        <h4 class="text-regular"> All Blog list</h4>
                    </div>
                    <div class="search-alumni-directory text-right col-md-6 col-sm-12 col-xs-12">
                        <form class="navbar-form no-margin no-padding">
                            <a href="<?= base_url('Blog/add_new'); ?>">
                                <button type="button" class=" bg-color-theme text-center text-regular">Add New</button>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
            <div class="alumni-directory-content">

                <table id="example" class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th>SI.</th>
                            <th>Blog title</th>
                            <th>Category</th>
                            <th>Published Date</th>
                            <th>Option</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (sizeof($all_blog)):
                            $i = 0;
                            foreach ($all_blog as $value):
                                $i++;
                                $categoryqr = $this->db->query("SELECT category_name FROM blog_category WHERE id='$value->blog_category'")->row();
                                if (!empty($categoryqr)):
                                    $cat = $categoryqr->category_name;
                                else:
                                    $cat = 'N/A';
                                endif;
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $value->blog_tilte; ?></td>
                                    <td><?= $value->category_name; ?></td>
                                    <td><?= $value->created_date; ?></td>
                                    <td>
                                        <a  target="_blank" href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>">
                                            <button type="button"class="btn-default">Web View</button>
                                        </a>
                                        <a href="<?= base_url('Blog/edit_blog?id=' . $value->id); ?>">
                                            <button type="button"class="btn-success">Edit</button>
                                        </a>
                                        <a href="#" onclick="show_deletemodal('<?= $value->id; ?>');">
                                            <button type="button"class="btn-danger">Delete</button>
                                        </a>
                                    </td>

                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>
<div id="delete_modal" class="modal fade " role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url('Blog/delete_blog'); ?>" method="POST">
                <div class="modal-header" style="background-color:honeydew;">
                    <b> Alert !!</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="blog_id" name="blog_id" class="form-control">
                    <p style="font-size: 25px;color: red;">Are you sure,You want to delete this blog post ?</p><br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">YES</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>

    </div>
</div>
<script>
    function show_deletemodal(id) {
        var blog_id = id;
        $('#blog_id').val(blog_id);
        $('#delete_modal').modal('show');
    }
</script>


