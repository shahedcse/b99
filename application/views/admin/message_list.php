<!DOCTYPE html>
<div class="content-wrapper" style="margin-top: 50px;">
    <div class="container">
        <div class="alumni-directory">
            <?php
            if ($this->session->userdata('add')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('notadd')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                $this->session->unset_userdata('notadd');
            endif;
            ?>
            <div class="top-section">
                <div class="row">
                    <div class="title-page text-left col-md-6 col-sm-12 col-xs-12">
                        <h4 class="text-regular">Message list</h4>
                    </div>
                    <div class="search-alumni-directory text-right col-md-6 col-sm-12 col-xs-12">

                    </div>
                </div>
            </div>
            <div class="alumni-directory-content">

                <table id="example" class="table table-striped table-bordered" >
                    <thead style="background-color: honeydew;">
                        <tr>
                            <th> SI.</th>
                            <th>Msg. by</th>
                            <th>Phone no.</th>
                            <th>Date</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $id = 0;
                        foreach ($all_msg as $value):
                            $id++;
                            ?>
                            <tr>
                                <td><?= $id ?></td>
                                <td><?= $value->name; ?></td>
                                <td><?= $value->phone; ?></td>
                                <td><?= $value->date; ?></td>
                                <td>
                                    <?php if ($value->status == 0): ?>
                                        <a href="#" onclick="view_details('<?= $value->id; ?>');">
                                            <button  type="button" class="btn-success">Not Checked</button>
                                        </a>
                                    <?php else: ?>
                                        <a href="#" onclick="view_details('<?= $value->id; ?>');">
                                            <button type="button"class="btn-default">Checked</button><br>
                                            <?php
                                            if (!empty($value->replied_by)):
                                                $repliedby = $this->db->query("SELECT name FROM users where id='$value->replied_by'")->row()->name;
                                                ?>
                                                <p style="color: green;">Replied by <?= $repliedby; ?></p>
                                            <?php else: ?>
                                                <p style="color: red;">Not Replied.</p>
                                            <?php endif; ?>
                                        </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="message_modal" class="modal fade " role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header" style="background-color:honeydew;">
                <b>Message Details.</b>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="margin-bottom: 30px;">
                <p  id="details" style="font-size: 25px;"></p><br><br>
                <button type="button" id="replybutton" class="btn btn-block">Reply</button><br><br>

                <form action="<?= base_url('Sadmin/messages_reply'); ?>" method="post" id="smsbox" style="display: none;" >
                    <input type="hidden" id="smsid" name="smsid" class="form-control">
                    <textarea name="smsreply" style="background-color: #d3f5ea;margin-bottom: 5px;  " id="smsreply" class="form-control"  >Write Here</textarea>
                    <button type="submit" class="btn btn-info pull-right">Send SMS</button>
                </form>

            </div>
            <div class="modal-footer">
                <a href="<?= base_url('Sadmin/messages'); ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </a>
            </div>
        </div>
    </div>
</div>
<script>

    $('#replybutton').click(function() {
        $('#smsbox').show();
    });

    $('.btn-success').click(function() {
        var $this = $(this);
        $this.toggleClass('btn-success');
        if ($this.hasClass('btn-success')) {
            $this.text('Not Checked');
        } else {
            $this.text('Checked');
        }
    });
    function view_details(id) {
        $('#smsid').val(id);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Sadmin/getDetailsData'); ?>",
            data: 'id=' + id,
            success: function(data) {
                var outputData = JSON.parse(data);
                var response = outputData.detailsdata;
                $("#details").text(response.message);

                $('#message_modal').modal('show');
            }
        });

    }
</script>



