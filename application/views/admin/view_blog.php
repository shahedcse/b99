<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel= "stylesheet" href="<?= base_url(); ?>assets/share/css/socialshare.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/share/icons/all.css">
<style>
    h1.title { margin: 150px auto; text-align: center; }

    @media (min-width: 992px) {
        .social-share-sticky .dropdown-menu-multi {
            top: 0% !important;
        }
    }
</style>
<div class="social-share social-share-sticky" data-social-share="{'iconSrc': '<?= base_url(); ?>assets/share/icons/', title': 'Page Title', 'description': 'Page Description'}">
    <div class="btn-group dropright">
        <button class="btn btn-outline-success dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="text-uppercase social-share-btn" aria-hidden="true">
                <span>S</span>
                <span>h</span>
                <span>a</span>
                <span>r</span>
                <span>e</span>
                <span class="fas fa-share-alt"></span>
            </span>
            <span class="sr-only">Share this page</span>
        </button>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-multi"><div class="dropdown-row"></div></div>
    </div>
</div>
<div class="content-wrapper" style="padding-top: 35px;">
    <div class="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-main col-lg-8 col-md-7 col-xs-12"style="border: 1px solid grey;">
                    <div class="blog-post">
                        <div class="area-img">
                            <img   style="height: 50%;width: 80%"src="<?= $base_url ?>assets/images/blogimg/<?= $details->fetured_image; ?>" alt="">
                            <div class="blog-text">
                                <div class="category">
                                </div>
                                <div class="article-title">
                                    <h2 class="text-regular text-capitalize"><?= $details->blog_tilte; ?></h2>
                                    <span class="text-content text-light"> by <?= $details->name; ?></span>

                                </div>
                            </div><br>
                            <div class="area-content">

                                <div class="desc">
                                    <p><?= $details->details; ?></p>
                                </div>

                                <div class="stats" style="padding-top: 10px;">
                                    <span class="clock">
                                        <span class="icon clock-icon"></span>
                                        <span class="text-center text-light"><?= date("F j, Y", strtotime($details->created_date)) ?></span>
                                    </span>
                                    <span class="comment">
                                        <span class="icon comment-icon"></span>
                                        <span class="text-center text-light">
                                            <?php
                                            if (!empty($comment_count)):
                                                echo $comment_count . 'Comments';
                                            else:
                                                echo 'No comments available';
                                            endif;
                                            ?>
                                        </span>
                                    </span>
                                    <span class="user">
                                        <span class="icon user-icon"></span>
                                        <span class="text-content text-light"> by <?= $details->name; ?></span>
                                        <a href="#">
                                            <button type="button" class="bnt btn-success">Read More by this Writer .</button>
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>

                        <?php
                        if ($this->session->userdata('add')):
                            echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                            $this->session->unset_userdata('add');
                        elseif ($this->session->userdata('notadd')):
                            echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                            $this->session->unset_userdata('notadd');
                        endif;
                        ?>
                        <div class="comments">
                            <?php if (empty($comment_count)): ?>
                                <div class="box-comments">
                                    <span class="note-comments text-regular">No Comments Found</span>
                                </div>
                            <?php else: ?>
                                <div class="box-comments">
                                    <span class="note-comments text-regular"><?= $comment_count ?> Comments Found</span><br>
                                    <ul class="list-comments">
                                        <?php
                                        if (sizeof($comments)):
                                            foreach ($comments as $value):
                                                $userQr = $this->db->query("SELECT * FROM users where id='$value->created_by'")->row();
                                                ?>
                                                <li>
                                                    <span class="user-avatar"><img style="border-radius: 50%" width="60px;" height="60px;"src="<?= $base_url ?>assets/images/webimg/<?= $userQr->image_path; ?>" alt=""></span>
                                                    <div class="user-comments">
                                                        <h4 class="heading-regular" style="margin-left: 15px;"> <?= $userQr->name; ?></h4>
                                                        <p style="margin-left: 15px;"><?= $value->comment; ?></p>
                                                    </div>
                                                </li>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </ul>
                                </div>
                            <?php endif; ?>

                        </div><br><br><br>
                        <!--write comments-->
                        <div class="write-comments">
                            <?php if (in_array($this->session->userdata('user_role'), array(1, 2))) : ?>
                                <div class="box-comments">
                                    <div class="title-write">
                                        <h4 class="heading-regular">Write Comment</h4>
                                    </div>
                                    <form action="<?= base_url('Blog/insert_comment'); ?>" method="post" >
                                        <div class="input-box">
                                            <input type="hidden" id="blog_id" name="blog_id" value="<?= $details->id; ?>">
                                            <textarea  style="color:black;"placeholder="Your Comment" name="comment" id="comment"  rows="2">
                                            </textarea>
                                        </div>
                                        <div class="buttons-set">
                                            <button type="submit" class="btn-success">Post Comment</button>
                                        </div>
                                    </form>
                                </div>
                            <?php else: ?>
                                <div class="buttons-set">
                                    <a href="<?= base_url('Auth'); ?>">
                                        <button type="button" class=" btn-group-lg btn-outline-success">Log In Now for make comment </button>
                                    </a>
                                </div>
                            <?php endif; ?>

                        </div>

                    </div>

                </div>
                <div class="sidebar blog-right col-lg-4 col-md-5 hidden-sm hidden-xs animated zoomInLeft"style="background-color:#e0e4e6;color:black;padding-top: 35px; ">
                    <?php $this->load->view('web/blog_sidebar'); ?>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <script src="<?= base_url(); ?>assets/share/js/socialshare.js"></script>
</body>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
<script>
    $('document').ready(function()
    {
        $('textarea').each(function() {
            $(this).val($(this).val().trim());
        }
        );
    });
</script>


