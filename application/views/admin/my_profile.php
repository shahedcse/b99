<style>

    .form-group{
        text-align: right;
    }

</style>
<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="account-title">
                <h2 class="heading-light">Welcome <?= $this->session->userdata("user_name"); ?> to SSC-99 Bangladesh</h2>
            </div>
            <?php
            if ($this->session->userdata('add')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('notadd')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                $this->session->unset_userdata('notadd');
            endif;
            ?>
            <div class="col-md-12 account-content" style="border:1px solid black; padding: 15px;">

                <div class="col-md-4">
                    <img  src="<?= base_url(); ?>assets/images/webimg/<?= $allinfo->image_path ?>" alt="Avatar" style="width:180px; border-radius: 50%;" ><br>
                    <h2> Name:  <?= $this->session->userdata("user_name"); ?></h2>
                    <a href="<?= base_url('Auth/profile'); ?>">
                        <button type="button" class="btn btn-success">My profile</button><br>
                    </a><br>
                    <a href="<?= base_url('Blog/bloglist'); ?>">
                        <button type="button" class="btn btn-success">My Blog List</button>
                    </a>
                </div>


                <div class="col-md-8">
                    <h3> Your Information:</h3>
                    <a href="<?= base_url('Auth/update_profile'); ?>">
                        <button type="button" class="btn btn-default">Update Now</button><br>
                    </a>

                    <div class="form-group" >
                        <label class="col-md-4 control-label">Full Name :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?= $allinfo->name; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone No. :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?= $allinfo->phone; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Email :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?= $allinfo->email; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Edu. Board :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->edu_board == ''):
                                echo 'N/A';
                            else:
                                echo $allinfo->edu_board;
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Division :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            $divisionqr = $this->db->query("SELECT divisionname FROM division_govt WHERE id='$allinfo->division'")->row();
                            if (!empty($divisionqr)):
                                $division = $divisionqr->divisionname;
                            else:
                                $division = 'N/A';
                            endif;
                            echo $division;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">District :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            $districtqr = $this->db->query("SELECT districtname FROM district_govt WHERE id='$allinfo->district'")->row();
                            if (!empty($districtqr)):
                                $district = $districtqr->districtname;
                            else:
                                $district = 'N/A';
                            endif;
                            echo $district;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">School Name :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?= $allinfo->school; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Academic Group:</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->ac_group == ''):
                                echo 'N/A';
                            else:
                                echo $allinfo->ac_group;
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Profession :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->Profession == ''):
                                echo 'N/A';
                            else:
                                echo $allinfo->Profession;
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Field :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->profession_field == ''):
                                echo 'N/A';
                            else:
                                echo $allinfo->profession_field;
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Specialization :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->specialization == ''):
                                echo 'N/A';
                            else:
                                echo $allinfo->specialization;
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Present Loc:</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->present_loc == ''):
                                echo 'N/A';
                            else:
                                echo $allinfo->present_loc;
                            endif;
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Date Of Birth :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->dob == '1970-01-01'):
                                echo 'N/A';
                            else:
                                echo date("F j, Y", strtotime($allinfo->dob));
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Permanent Address :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->permanent_address == ''):
                                echo 'N/A';
                            else:
                                echo $allinfo->permanent_address;
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Present Address :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->present_address == ''):
                                echo 'N/A';
                            else:
                                echo $allinfo->present_address;
                            endif;
                            ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Blood Donor Status :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            if ($allinfo->blood_donor == 1):
                                echo 'YES';
                            else:
                                echo 'No';
                            endif;
                            ?>                           

                        </div>
                    </div>

                    <div class="form-group" id="blood" >
                        <label class="col-md-4 control-label">Blood Group :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php
                            $bloodqr = $this->db->query("SELECT blood_group FROM allblood_group WHERE id='$allinfo->blood_group'")->row();
                            if (!empty($bloodqr)):
                                $blood = $bloodqr->blood_group;
                            else:
                                $blood = 'N/A';
                            endif;
                            echo $blood;
                            ?>


                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Facebook profile :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <?php if ($allinfo->fblink != ''): ?>
                                <a href="<?= $allinfo->fblink; ?>" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i> profile Link </a>
                                    <?php
                                else:
                                    echo 'N/A';
                                endif;
                                ?>                           

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

