<style>
    .form-group{
        text-align: left;
    }

</style>
<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="account-title">
                <h4 class="heading-light">Edit Blog:</h4>
            </div>

            <span id="verific" style="font-size: 25px;"></span>
            <div class="col-md-10 col-lg-offset-1" style="border:1px solid grey;   padding: 15px; border-radius: 30px;">
                <form action="<?= base_url('Blog/update_blog'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="hidden" name="blog_id" id="blog_id" value="<?= $details->id; ?>">
                        <label class="col-md-4 control-label">Blog title :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="blog_tilte" value="<?= $details->blog_tilte; ?>" name="blog_tilte" placeholder="blog_tilte" class="form-control" required="true" value="" type="text"></div>
                        </div><br>
                    </div><br>
                    <div class="form-group">

                        <label class="col-md-4 control-label">Blog category :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="selectpicker form-control" required=""id="blog_category" name="blog_category">
                                    <option value="">--Select--</option>
                                    <?php
                                    foreach ($all_category as $value):
                                        if ($details->blog_category == $value->id):
                                            ?>
                                            <option value="<?= $value->id ?>"selected=""><?= $value->category_name; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value->id ?>"><?= $value->category_name; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?>

                                </select>
                            </div>
                        </div><br>
                    </div><br>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Featured Image:<span style="color:red;">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="fileToUpload" name="fileToUpload"  class="form-control"  type="file"></div>
                        </div><br>
                        <img class="zoom" src="<?= $base_url ?>assets/images/blogimg/<?= $details->fetured_image; ?>" alt="">
                    </div><br>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Blog Details :<span style="color:red">*</span></label>
                        <div class="col-md-9">
                            <textarea class="form-control" type="text" id="summernote" name="summernote">
                                <?= $details->details; ?>
                            </textarea>
                            <center>
                                <a href="<?= base_url('Blog/bloglist'); ?>">
                                    <button  type="button"   title="Back" class="btn btn-default ">Back</button>
                                </a>
                                <button  type="Submit"  id="submit" title="update" class="btn btn-success ">Update</button>
                            </center>
                        </div><br>

                    </div><br>

                </form>

            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function() {
        $('#summernote').summernote({
            // set editor height
            height: 350, // set editor height
            minHeight: 350, // set minimum height of editor
            maxHeight: 350
           

        });
    });
    $('document').ready(function()
    {
        $('textarea').each(function() {
            $(this).val($(this).val().trim());
        }
        );
    });
</script>



