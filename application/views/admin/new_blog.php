<style>
    .form-group{
        text-align: left;
    }

</style>
<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="account-title">
                <h4 class="heading-light">Add New Blog(Please Fill all required Field.<span style="color:red">***</span>):</h4>
            </div>

            <div class="col-md-10 col-lg-offset-1" style="border:1px solid grey; background-color: #e7ede3; border-radius: 20px;   padding: 15px;">
                <form action="<?= base_url('Blog/insert_blog'); ?>" id="blogform"  method="post" enctype="multipart/form-data">
                    <div class="form-group">

                        <label class="col-md-4 control-label">Blog title :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="blog_tilte" name="blog_tilte" placeholder="blog_tilte" class="form-control" required="true" value="" type="text"></div>
                        </div><br>
                    </div><br>
                    <div class="form-group">

                        <label class="col-md-4 control-label">Blog category :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="selectpicker form-control" required=""id="blog_category" name="blog_category">
                                    <option value="">--Select--</option>
                                    <?php foreach ($all_category as $value): ?>
                                        <option value="<?= $value->id ?>"><?= $value->category_name; ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>
                        </div><br>
                    </div><br>
                    <div class="form-group">

                        <label class="col-md-4 control-label">Featured Image:<span style="color:red;">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="fileToUpload" name="fileToUpload"  class="form-control"  type="file"></div>
                        </div><br>
                    </div><br>
                    <div class="form-group">

                        <label class="col-md-2 control-label">Blog Details :<span style="color:red">*</span></label>
                        <span id="wordcount" style="font-size: 25px; color:green;"></span>
                        <div class="col-md-9">

                            <textarea class="form-control" placeholder="" type="text" id="summernote" name="summernote">
                           Write here...
                            </textarea>
                            <center>
                                <button  type="submit"  id="submit"  title="publish" class="btn btn-success ">Publish</button>
                            </center>
                        </div><br>

                    </div><br>
                </form>

            </div>
        </div>
    </div>
</div>
<script>
    function validate() {
        var title = $('#blog_tilte').val();
        var cat = $('#blog_category').val();
        var image = $('#fileToUpload').val();
        if (title == '' || cat == '' || image == '') {
            alert('Please fill all required field');
        }
        else {

            document.getElementById("blogform").submit();
        }
    }
    document.getElementById("submit").disabled = true;
    $('#summernote').on('summernote.keyup', function(event) {

        var text = $(this).next('.note-editor').find('.note-editable').text();
        var length = text.length;
        var words = this.value.match(/\S+/g).length;
        $('#wordcount').text(words);
        if (words >= 120) {
            document.getElementById("submit").disabled = false;
        }
        else {
            document.getElementById("submit").disabled = true;
        }
    });
    $(document).ready(function() {
        $('#summernote').summernote({
            // set editor height
            height: 350, // set editor height
            minHeight: 350, // set minimum height of editor
            maxHeight: 350
         });
    });
    $('document').ready(function()
    {
        $('textarea').each(function() {
            $(this).val($(this).val().trim());
        }
        );
    });
</script>



