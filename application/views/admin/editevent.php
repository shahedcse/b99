<style>
    .form-group{
        text-align: left;
    }

</style>
<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="account-title">
                <h4 class="heading-light">Edit Event/News:</h4>
            </div>

            <div class=" col-md-10 col-lg-offset-1" style="border:1px solid grey; border-radius: 20px;   padding: 15px;">
                <form action="<?= base_url('Sadmin/update_event'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="hidden" name="event_id" id="event_id" value="<?= $all_info->id; ?>">
                        <label class="col-md-4 control-label">Title :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="event_tilte" value="<?= $all_info->event_tilte; ?>" name="event_tilte" placeholder="event_tilte" class="form-control" required="true" value="" type="text"></div>
                        </div><br>
                    </div><br>


                    <div class="form-group">
                        <label class="col-md-4 control-label">Event/News Place :</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="place" name="place" placeholder="Event place" class="form-control"  value="<?= $all_info->place; ?>" type="text"></div>
                        </div><br>
                    </div><br>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Attach File:</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="fileToUpload" name="fileToUpload"  class="form-control"  type="file"></div>
                        </div>
                        <img class="zoom" src="<?= $base_url ?>assets/images/blogimg/<?= $all_info->file; ?>" alt=""><br>
                    </div><br>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Status:</label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="selectpicker form-control"id="status" name="status">
                                    <option value="1"<?php if ($all_info->status == 1): ?>selected<?php endif; ?>>Active</option>
                                    <option value="0"<?php if ($all_info->status == 0): ?>selected<?php endif; ?>>Inactive</option>

                                </select>
                            </div>
                        </div><br>
                    </div><br>
                    <div class="form-group">

                        <label class="col-md-2 control-label">Event/News Details :<span style="color:red">*</span></label>
                        <div class="col-md-9">
                            <textarea class="form-control"placeholder="" type="text" id="summernote" name="summernote">
                                <?= $all_info->details; ?>
                            </textarea>
                            <center>
                                <a href="<?= base_url('Sadmin/event'); ?>">
                                    <button  type="button"   title="Back" class="btn btn-default ">Back</button>
                                </a>
                                <button  type="Submit"   title="publish" class="btn btn-success ">Update</button>
                            </center>
                        </div><br>

                    </div><br>

                </form>

            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function() {
        $('#summernote').summernote();
    });
    $('document').ready(function()
    {
        $('textarea').each(function() {
            $(this).val($(this).val().trim());
        }
        );
    });
</script>




