<style>
    .form-group{
        text-align: right;
    }

</style>
<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="account-title">
                <h2 class="heading-light">Update Your Information.</h2>
            </div>
            <div class="col-md-12 account-content" style="border:1px solid black; padding: 15px;">

                <div class="col-md-4">
                    <img  src="<?= base_url(); ?>assets/images/webimg/<?= $allinfo->image_path ?>" alt="Avatar" style="width:180px; border-radius: 50%;" ><br>
                    <h2> Name:  <?= $this->session->userdata("user_name"); ?></h2>
                    <a href="<?= base_url('Auth/profile'); ?>">
                        <button type="button" class="btn btn-success">My profile</button>
                    </a><br><br>
                    <a  href="<?= base_url('Blog/bloglist'); ?>">
                        <button type="button" class="btn btn-success">My Blog List</button>
                    </a>
                </div>


                <div class="col-md-8">
                    <form action="<?= base_url('Auth/update_profiledata'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Full Name :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="fullName" value="<?= $allinfo->name; ?>" name="fullName" placeholder="Full Name" class="form-control" required="true" value="" type="text"></div>
                            </div><br>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Phone No. :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input  value="<?= $allinfo->phone; ?>" id="phone_no" name="phone_no" placeholder="Phone No." class="form-control" required="true" value="" type="text"></div>
                                <span id="verific" style="font-size: 10px;"></span>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input value="<?= $allinfo->email; ?>" id="email" name="email" placeholder="Email Address." class="form-control" required="true" value="" type="text"></div>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Edu. Board For SSC:</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <select class="selectpicker form-control"   id="edu_board" name="edu_board">
                                        <option value="" >-Select One-</option>
                                        <option value="barisal"<?php if ($allinfo->edu_board == 'barisal'): ?>selected=""<?php endif; ?>>Barisal</option>
                                        <option value="Chattogram" <?php if ($allinfo->edu_board == 'Chattogram'): ?>selected=""<?php endif; ?>>Chattogram </option>
                                        <option value="comilla" <?php if ($allinfo->edu_board == 'comilla'): ?>selected=""<?php endif; ?>>Comilla</option>
                                        <option value="dhaka" <?php if ($allinfo->edu_board == 'dhaka'): ?>selected=""<?php endif; ?>>Dhaka</option>
                                        <option value="dinajpur" <?php if ($allinfo->edu_board == 'dinajpur'): ?>selected=""<?php endif; ?>>Dinajpur</option>
                                        <option value="jessore" <?php if ($allinfo->edu_board == 'jessore'): ?>selected=""<?php endif; ?>>Jessore</option>
                                        <option value="rajshahi" <?php if ($allinfo->edu_board == 'rajshahi'): ?>selected=""<?php endif; ?>>Rajshahi</option>
                                        <option value="sylhet" <?php if ($allinfo->edu_board == 'sylhet'): ?>selected=""<?php endif; ?>>Sylhet</option>
                                        <option value="madrasah" <?php if ($allinfo->edu_board == 'madrasah'): ?>selected=""<?php endif; ?>>Madrasah</option>
                                        <option value="tec" <?php if ($allinfo->edu_board == 'tec'): ?>selected=""<?php endif; ?>>Technical</option>
                                        <option value="dibs" <?php if ($allinfo->edu_board == 'dibs'): ?>selected=""<?php endif; ?>>DIBS(Dhaka)</option>

                                    </select>
                                </div>
                            </div>
                        </div><br>

                        <div class="form-group">
                            <label class="col-md-4 control-label">School Name :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input value="<?= $allinfo->school; ?>" id="school_name" name="school_name" placeholder="School Name" class="form-control" required="true" value="" type="text"></div>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Academic Group:</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <select class="selectpicker form-control"id="group" name="group">
                                        <option value="">--Select--</option>
                                        <option value="Science" <?php if ($allinfo->ac_group == 'Science'): ?>selected=""<?php endif; ?>>Science</option>
                                        <option value="Arts" <?php if ($allinfo->ac_group == 'Arts'): ?>selected=""<?php endif; ?>>Arts</option>
                                        <option value="Commerce" <?php if ($allinfo->ac_group == 'Commerce'): ?>selected=""<?php endif; ?>>Commerce</option>
                                        <option value="Others" <?php if ($allinfo->ac_group == 'Others'): ?>selected=""<?php endif; ?>>Others</option>
                                    </select>
                                </div>
                            </div>
                        </div><br>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Date Of Birth :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input value="<?= $allinfo->dob; ?>" id="dob" name="dob" placeholder="Email" class="form-control"  value="" type="date"></div>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Profession :<span style="color:red">*</span></label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <select class="selectpicker form-control" required="true"id="Profession" name="Profession">
                                        <option value="">--Select--</option>

                                        <?php
                                        foreach ($profession as $value):
                                            if ($allinfo->p_name == $value->p_name):
                                                ?>
                                                <option value="<?= $value->p_name; ?>" selected=""><?= $value->p_name; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value->p_name; ?>"><?= $value->p_name; ?></option>
                                            <?php
                                            endif;
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div><br>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Field :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <select class="selectpicker form-control"id="profession_field" name="profession_field">
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach ($profession_field as $value):
                                            if ($allinfo->profession_field == $value->field_name):
                                                ?>
                                                <option value="<?= $value->field_name; ?>" selected=""><?= $value->field_name; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value->field_name; ?>"><?= $value->field_name; ?></option>
                                            <?php
                                            endif;
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div><br>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Specialization : </label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="specialization"  name="specialization" value="<?= $allinfo->specialization; ?>" class="form-control"  type="text"></div>
                            </div>
                        </div><br>
                        <div class="form-group" id="blood">
                            <label class="col-md-4 control-label">Present Location :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <select class="selectpicker form-control"id="present_loc" name="present_loc">
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach ($districtall as $value):
                                            if ($allinfo->present_loc == $value->districtname):
                                                ?>
                                                <option value="<?= $value->districtname; ?>"selected><?= $value->districtname; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value->districtname; ?>"><?= $value->districtname; ?></option>

                                            <?php
                                            endif;
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="col-md-4 control-label">FB Profile Link :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="fblink" name="fblink" value="<?= $allinfo->fblink; ?>" class="form-control"  type="text"></div>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Permanent Address :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input value="<?= $allinfo->permanent_address; ?>"id="permanent_address" name="permanent_address" placeholder="permanent_address" class="form-control" value="" type="text"></div>
                            </div>
                        </div><br>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Present Address :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input value="<?= $allinfo->present_address; ?>" id="present_address" name="present_address" placeholder="present_address" class="form-control" value="" type="text"></div>
                            </div>
                        </div><br>
                        <div class="form-group" id="blood">
                            <label class="col-md-4 control-label">Blood Group :</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <select class="selectpicker form-control"id="blood_group" name="blood_group">
                                        <option value="0">--Select--</option>
                                        <?php
                                        foreach ($blood as $value):
                                            if ($allinfo->blood_group == $value->id):
                                                ?>
                                                <option value="<?= $value->id; ?>"selected=""><?= $value->blood_group; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value->id; ?>"><?= $value->blood_group; ?></option>
                                            <?php
                                            endif;
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div><br>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Profile Image:</label>
                            <div class="col-md-6 inputGroupContainer">
                                <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="fileToUpload" name="fileToUpload"  class="form-control"  type="file"></div>
                            </div>
                        </div><br>

                        <input type="checkbox" name="blood_donor" id="blood_donor"<?php if ($allinfo->blood_donor == 1): ?>checked=""<?php endif; ?>  value="1"> Become A Blood Donor<br><br>
                        <button title="update" type="submit" class="btn btn-success">Update</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


