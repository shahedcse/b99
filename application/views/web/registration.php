<!DOCTYPE html>
<style>

    .stepwizard-step p {
        margin-top: 10px;
        margin-bottom: 20px;
    }
    .stepwizard-row {
        display: table-row;
    }
    .stepwizard {
        display: table;
        width: 50%;
        position: relative;
    }
    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important;
    }
    .stepwizard-row:before {
        top: 16px;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 100%;
        height: 1px;
        background-color: #ccc;
        z-order: 0;
    }
    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }
    .btn-circle {
        width: 40px;
        height: 40px;
        text-align: center;
        padding: 7px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
</style>
<html lang="en">
    <div class="content-wrapper">
        <div class="account-page login text-center">
            <div class="container">
                <div class="account-title">
                    <h4 class="heading-light">Register INTO SSC-99 Bangladesh
                        (Please Fill all required Field.<span style="color:red">***</span>)</h4>
                </div>
                <div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>N.B : All of your information will be hidden from general member.</strong>  </div>
                <?php
                if ($this->session->userdata('add')):
                    echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                    $this->session->unset_userdata('add');
                elseif ($this->session->userdata('notadd')):
                    echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                    $this->session->unset_userdata('notadd');
                endif;
                ?>
                <span id="alert_box" style="color:red;font-style: 15px"></span>
                <form  id="default" action="<?= base_url('Auth/insert_regidata'); ?>"   method="post" enctype="multipart/form-data">
                    <div class="container" style="border:1px solid black;   padding: 25px; border-radius: 30px;">
                        <span id ="error_text" style="color:red; font-size: 15px;"></span>
                        <div class="stepwizard col-md-offset-3" style="padding-bottom:20px;">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step">
                                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="">2</a>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="">3</a>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="">4</a>
                                </div>
                            </div>
                        </div>


                        <div class="row setup-content" id="step-1" >
                            <h3 style="margin-bottom: 10px;"> Personal Details:</h3>
                            <div class="col-md-12" >

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Full Name :<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="fullName" name="fullName" placeholder="Full Name" class="form-control" required="true" value="" type="text"></div>
                                    </div><br>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Gender:<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                            <select class="selectpicker form-control" required="true"  id="gender" name="gender">
                                                <option value="" >-Select One-</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                <option value="others">Others</option>
                                            </select>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Phone No. :<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="phone_no" name="phone_no" maxlength="11" onchange="check_verification();" onkeypress="return isNumber(event);" placeholder="01xxxxxxxxx" class="form-control" required="true" value="" type="text"></div>
                                        <span id="verific" style="font-size: 10px;"></span>
                                    </div><br>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Email :<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input onchange="check_email();"id="email" name="email" placeholder="Email Address." class="form-control" required="true" value="" type="text"></div>
                                        <span id="verific2" style="font-size: 10px;"></span>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Date Of Birth :</label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input id="dob" name="dob" placeholder="Email" class="form-control"  type="date"></div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Profession :<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                            <select class="selectpicker form-control" required="true"id="Profession" name="Profession">
                                                <option value="">--Select--</option>
                                                <?php foreach ($profession as $value): ?>
                                                    <option value="<?= $value->p_name; ?>"><?= $value->p_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Field :</label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                            <select class="selectpicker form-control"id="profession_field" name="profession_field">
                                                <option value="">--Select--</option>
                                                <?php foreach ($profession_field as $value): ?>
                                                    <option value="<?= $value->field_name; ?>"><?= $value->field_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Specialization : </label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="specialization"  name="specialization" placeholder="specialization" class="form-control"  type="text"></div>
                                    </div><br>
                                </div>
                                <div class="form-group" id="blood">
                                    <label class="col-md-4 control-label">Blood Group :</label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                            <select class="selectpicker form-control"id="blood_group" name="blood_group">
                                                <option value="0">--Select--</option>
                                                <?php foreach ($blood as $value): ?>
                                                    <option value="<?= $value->id; ?>"><?= $value->blood_group; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div><br>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Present Location :<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                            <select class="selectpicker form-control" required="true" id="present_loc" name="present_loc">
                                                <option value=" ">--Select--</option>
                                                <?php foreach ($districtall as $value): ?>
                                                    <option value="<?= $value->districtname; ?>"><?= $value->districtname; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">FB Profile Link :<span style="color:red">*</span> </label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="fblink" required="true" name="fblink" placeholder="fb link" class="form-control"  type="text"></div>
                                    </div><br>
                                </div>
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                            </div>

                        </div>
                        <div class="row setup-content" id="step-2" >
                            <h3 style="margin-bottom: 10px;"> Academic Details:</h3>
                            <div class="col-md-12" >
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Education Board For SSC:<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                            <select class="selectpicker form-control" required="true"  id="edu_board" name="edu_board">
                                                <option value="" >-Select One-</option>
                                                <option value="barisal">Barisal</option>
                                                <option value="Chattogram">Chattogram </option>
                                                <option value="comilla">Comilla</option>
                                                <option value="dhaka">Dhaka</option>
                                                <option value="dinajpur">Dinajpur</option>
                                                <option value="jessore">Jessore</option>
                                                <option value="rajshahi">Rajshahi</option>
                                                <option value="sylhet">Sylhet</option>
                                                <option value="madrasah">Madrasah</option>
                                                <option value="tec">Technical</option>
                                                <option value="dibs">DIBS(Dhaka)</option>

                                            </select>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">School Name :<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="school_name" name="school_name" placeholder="School Name" class="form-control" required="true" value="" type="text"></div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Academic Group:</label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                            <select class="selectpicker form-control"id="group" name="group">
                                                <option value="">--Select--</option>
                                                <option value="Science">Science</option>
                                                <option value="Arts">Arts</option>
                                                <option value="Commerce">Commerce</option>
                                                <option value="others">Others</option>
                                            </select>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">SSC Registration/Certificate:<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="fileToUpload2" name="fileToUpload2" onchange="make_enable();"  class="form-control"   type="file"></div>
                                    </div><br>
                                </div><br>
                                <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                            </div>

                        </div>

                        <div class="row setup-content" id="step-3">
                            <div class="col-md-12">
                                <h3 style="margin-bottom: 10px;"> Address Details :</h3>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Home Division :<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                            <select class="selectpicker form-control" required="true"  id="gov_division" name="gov_division">
                                                <option value="">--Select--</option>
                                                <?php foreach ($divisionall as $value): ?>
                                                    <option value="<?= $value->id; ?>"><?= $value->divisionname; ?></option>

                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Home District :<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                            <select class="selectpicker form-control" required="true"  id="gov_district" name="gov_district">
                                                <option value="">--Select--</option>
                                            </select>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Permanent Address :</label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="permanent_address" name="permanent_address" placeholder="permanent_address" class="form-control"  type="text"></div>
                                    </div><br>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Present Address :</label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="present_address" name="present_address" placeholder="present_address" class="form-control"  type="text"></div>
                                    </div><br>
                                </div>
                                <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                            </div>

                        </div>
                        <div class="row setup-content" id="step-4">
                            <div class="col-md-12">
                                <h3 style="margin-bottom: 10px;"> Account Details :</h3>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Profile Image:<span style="color:red">*</span></label>
                                    <div class="col-md-6 inputGroupContainer">
                                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="fileToUpload" name="fileToUpload"  class="form-control"  type="file"></div>
                                    </div><br>
                                </div><br>
                                <input  type="checkbox"  name="blood_donor" id="blood_donor" value="1"><hb> Register As A Blood Donor</b><br><br>

                                    <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
                                    <button   class="btn btn-primary btn-lg pull-right" id="submit"  type="submit">Submit</button>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</html>
<div class="modal fade" id="passportcheckmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Alert.&nbsp;</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="panel-body">

                        <div class="col-lg-12">
                            <div class="form-group">
                                <h3 style="color:red;"><i class="fa fa-warning"></i>&nbsp;&nbsp;Please give Passport No Properly.</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<script>
    $("form").on("submit", function() {
        $(this).find(":submit").prop("disabled", true);
    });
    $(document).ready(function() {
        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');
        allWells.hide();
        navListItems.click(function(e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });
        allPrevBtn.click(function() {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
            prevStepWizard.removeAttr('disabled').trigger('click');
        });
        allNextBtn.click(function() {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });
        $('div.setup-panel div a.btn-primary').trigger('click');
    });
    function isNumber(e) {
        e = e || window.event;
        var charCode = e.which ? e.which : e.keyCode;
        return /\d/.test(String.fromCharCode(charCode));
    }
    function check_email() {
        var email = $("#email").val();
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email)) {

            var dataString = 'email=' + email;
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('Auth/check_email'); ?>",
                data: dataString,
                success: function(data)
                {
                    if (data == 'dontmass') {
                        $("#verific2").text("valid email no.");
                        $("#verific2").css('color', 'green');
                    }
                    if (data == 'mass') {
                        $("#verific2").text("This email is already registered.");
                        $("#verific2").css('color', 'red');
                        $("#email").val('');
                    }
                }
            });
            return true;
        }
        else {
            alert('please enter a valid email');
            $("#verific2").val("");
            $("#email").val('');
        }



    }

    function check_verification() {
        var phone = $("#phone_no").val();
        var dataString = 'phone=' + phone;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Auth/check_phone'); ?>",
            data: dataString,
            success: function(data)
            {
                if (data == 'dontmass') {
                    $("#verific").text("valid phone no.");
                    $("#verific").css('color', 'green');
                }
                if (data == 'mass') {
                    $("#verific").text("This number is already registered.");
                    $("#verific").css('color', 'red');
                    $("#phone_no").val('');
                }
            }
        });
    }

    document.getElementById("submit").disabled = true;
    function make_enable() {
        var doc = $("#fileToUpload2").val();
        var name = $("#fullName").val();
        var gender = $("#gender").val();
        var phone = $("#phone_no").val();
        var email = $("#email").val();
        var profession = $("#Profession").val();
        var presentloc = $("#present_loc").val();
        if (name != '' && gender != '' && phone != '' && email != '' && profession != '' && presentloc != '' && doc != '') {
            document.getElementById("submit").disabled = false;
        }
    }
//
//
//
//    function valueChange() {
//        var checkBox = document.getElementById("blood_donor");
//        if (checkBox.checked == true) {
//            document.getElementById('blood').style.display = 'block';
//        } else {
//            document.getElementById('blood').style.display = 'none';
//        }
//    }


    $("#gov_division").change(function() {
        var division = $("#gov_division").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('auth/getdistrict'); ?>",
            data: 'division=' + division,
            success: function(data) {
                document.getElementById("gov_district").innerHTML = data;
            }
        });
    });
</script>
