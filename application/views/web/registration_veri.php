<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="account-title">
                <h4 class="heading-light">Set Your Verification Code .</h4>
            </div>
            <?php
            if ($this->session->userdata('add')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('notadd')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                $this->session->unset_userdata('notadd');
            endif;
            ?>
            <span id="verific" style="font-size: 25px;"></span>
            <div class="account-content" style="border:1px solid black;  padding: 15px;">
                <form action="<?php echo base_url('Auth/check_verification2'); ?>" method="post">
                    <div class="form-group">
                        <center> <p style="color:green;">(Please Wait , An OTP Verification Code is Send to your phone/email.please check it and set here)</p>
                        </center><br>
                        <label class="col-md-4 control-label">Verification Code :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="verification_code" name="verification_code" placeholder="OTP Code" class="form-control" required="true" value="" type="text"></div>
                        </div><br>
                    </div><br>
                    <div class="buttons-set">
                        <button  type="submit"  title="Registration" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function check_verification() {
        var code = $("#verification_code").val();
        if (code == '') {
            alert('Please set your Code');
            return;
        }
        var dataString = 'verification_code=' + code;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Auth/check_verification'); ?>",
            data: dataString,
            success: function(data)
            {
                if (data == 'dontmass') {
                    $("#verific").text("Sorry !! Your verification code does't match.please try again");
                    $("#verific").css('color', 'red');
                }

                if (data == 'mass') {
                    // document.getElementById('update').disabled = true;
                    $("#verific").text("Congrats !! Your Registration is successfully complete.");
                    $("#verific").css('color', 'green');
                    $("#verification_code").val("");
                    //   window.location = base_url + 'Auth';


                }
            }
        });

    }
</script>



