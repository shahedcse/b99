<link rel="stylesheet" href="<?= $base_url ?>assets/dist/css/lightboxgallery-min.css">
<link rel="stylesheet" href="<?= $base_url ?>assets/css/stylegallery.css">
<div class="content-wrapper">
    <div class="galery-wrapper">
        <div class="container">
            <div class="galery-content">
                <div class="lightboxgallery-gallery clearfix">
                    <div class="galery-title text-center">
                        <h4 class="heading-regular">OUR GALLERY</h4>
                    </div>
                    <?php foreach ($all_images AS $value): ?>
                        <a class="lightboxgallery-gallery-item" target="_blank" href="<?= $base_url ?>assets/images/gallery/<?= $value->image_path; ?>" data-title="" data-alt="<?= $value->image_path; ?>" data-desc=".">
                            <div  style="height: 150px; margin-bottom: 5px;">
                                <img   src="<?= $base_url ?>assets/images/gallery/<?= $value->image_path; ?>" title="" alt="">
                                <div class="lightboxgallery-gallery-item-content">
                                    <span class="lightboxgallery-gallery-item-title">Group Images</span>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="pagination-wrapper text-center">
                <ul class="pagination">
                    <li class="prev"><a href="#">Previous</a></li>
                    <li><a href="#">1</a></li>

                    <li class="next"><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
    </div>

    <script src="<?= $base_url ?>assets/dist/js/lightboxgallery-min.js"></script>
    <script type="text/javascript">
        jQuery(function($) {
            $(document).on('click', '.lightboxgallery-gallery-item', function(event) {
                event.preventDefault();
                $(this).lightboxgallery({
                    showCounter: true,
                    showTitle: true,
                    showDescription: true
                });
            });
        });
    </script>
