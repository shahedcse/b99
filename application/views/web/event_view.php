<div class="content-wrapper" style="margin-top: 20px;">

    <div class="event-calendar">
        <div class="container">
            <div class="top-section text-center">

            </div>

            <div class="event-list-content">
                <div class="event-list-item">
                    <div class="date-item">
                        <img src="<?= base_url(); ?>assets/news/images/img_132233.png" width="60" class="img-circle" />
                    </div>
                    <div class="date-desc-wrapper">
                        <div class="date-desc">
                            <div class="date-title"><h4 class="heading-regular"><?= $eventdetails->event_tilte; ?></h4></div>
                            <div class="date-excerpt">
                                <?php if (!empty($eventdetails->file)): ?>
                                    <img   style="height:50%; width:80%" src="<?= $base_url ?>assets/images/blogimg/<?= $eventdetails->file; ?>" alt="">
                                <?php endif; ?>
                                <p><?= $eventdetails->details; ?></p>  </div>
                            <div class="place">
                                <span class="icon map-icon"></span>
                                <span class="text-place"><?= $eventdetails->place; ?> </span>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>