<div class="content-wrapper" style="margin-top: 40px;">
    <div class="account-page login text-center">
        <div class="container" style="border: 1px solid black;">
            <div class="account-title">
                <h4 class="heading-light">LOG IN INTO SSC-99 Bangladesh</h4>
            </div>

            <?php
            if ($this->session->userdata('add')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('login_error')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Meaasge !!! </strong> ' . $this->session->userdata('login_error') . '</div>';
                $this->session->unset_userdata('login_error');
            endif;
            ?>

            <div class="account-content">
                <form action="<?= base_url('Auth/login'); ?>" method="post">
                    <div class="input-box email">
                        <label class=" control-label">Phone No. :</label>
                        <input type="text" name="phone" style="color: black" placeholder="01700000000" >
                    </div>
                    <div class="input-box password">
                        <label class=" control-label">Email Ad. :</label>
                        <input type="text" name="email"style="color: black" placeholder="Email">
                    </div>
                    <div class="buttons-set">
                        <a href="#" onclick="disclaimer_modal();">
                            <button  type="button" title="Register" class="btn btn-default">Register Now</button>
                        </a>
                        <button  title="Log In" class="btn btn-success">Log In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>