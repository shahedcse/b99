
<script type="text/javascript">
    jQuery(document).ready(function($) {

        var jssor_1_options = {
            $AutoPlay: 1,
            $AutoPlaySteps: 5,
            $SlideDuration: 160,
            $SlideWidth: 200,
            $SlideSpacing: 3,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $Steps: 5
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*#region responsive code begin*/

        var MAX_WIDTH = 980;

        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        /*#endregion responsive code end*/
    });
</script>

<style>
    /*jssor slider loading skin spin css*/
    .jssorl-009-spin img {
        animation-name: jssorl-009-spin;
        animation-duration: 1.6s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
    }

    @keyframes jssorl-009-spin {
        from { transform: rotate(0deg); }
        to { transform: rotate(360deg); }
    }

    /*jssor slider bullet skin 057 css*/
    .jssorb057 .i {position:absolute;cursor:pointer;}
    .jssorb057 .i .b {fill:none;stroke:#fff;stroke-width:2000;stroke-miterlimit:10;stroke-opacity:0.4;}
    .jssorb057 .i:hover .b {stroke-opacity:.7;}
    .jssorb057 .iav .b {stroke-opacity: 1;}
    .jssorb057 .i.idn {opacity:.3;}

    /*jssor slider arrow skin 073 css*/
    .jssora073 {display:block;position:absolute;cursor:pointer;}
    .jssora073 .a {fill:#ddd;fill-opacity:.7;stroke:#000;stroke-width:160;stroke-miterlimit:10;stroke-opacity:.7;}
    .jssora073:hover {opacity:.8;}
    .jssora073.jssora073dn {opacity:.4;}
    .jssora073.jssora073ds {opacity:.3;pointer-events:none;}
</style>

<a href="#" id="back-to-top" title="Back to top">&uarr;</a>
<button class="open-button" onclick="openForm()">Message us</button>

<div class="chat-popup" id="myForm">
    <form action="#" class="form-container">
        <div  id="success" style="display:none">
            <div class="alert alert-success fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <strong>Success !!</strong> message send Successfully. </div>
        </div>

        <div  id="failed" style="display:none">
            <div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button> <strong>Alert!</strong> Please fill Up all fields. </div>
        </div>
        <label for="msg"><b>Message US for Any Support.</b></label>
        <input type="text" class="form-control" name="name" required="" id="name" placeholder="Name">
        <input type="text" class="form-control" onkeypress='return isNumberKey2(event)' maxlength="11" name="phone" required="" id="phone" placeholder="Phone No.">
        <textarea placeholder="Type message.." id="msg"  name="msg" required></textarea>

        <button type="button" onclick="send_message();" class="btn">Send</button>
        <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
    </form>
</div>
<div class="instagream" style="padding: 10px;">
    <h2 class="text-center" style="padding: 20px;">
        <span class="">Our New Member </span> <br/>
    </h2>
    <?php
    $allusers = $this->db->query("SELECT * FROM users  where status='1' order by id DESC  limit 20")->result();
    ?>
    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:150px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:150px;overflow:hidden;">
            <?php foreach ($allusers as $value): ?>
                <a href="#" onclick="show_detailsdata('<?= $value->id; ?>');">
                    <div>
                        <img data-u="image" style="border-radius:60px;" src="<?= base_url(); ?>assets/images/webimg/<?= $value->image_path; ?>" />
                    </div>
                </a>
            <?php endforeach; ?> 

        </div>

        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora073" style="width:50px;height:50px;top:0px;left:30px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <path class="a" d="M4037.7,8357.3l5891.8,5891.8c100.6,100.6,219.7,150.9,357.3,150.9s256.7-50.3,357.3-150.9 l1318.1-1318.1c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3L7745.9,8000l4216.4-4216.4 c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3l-1318.1-1318.1c-100.6-100.6-219.7-150.9-357.3-150.9 s-256.7,50.3-357.3,150.9L4037.7,7642.7c-100.6,100.6-150.9,219.7-150.9,357.3C3886.8,8137.6,3937.1,8256.7,4037.7,8357.3 L4037.7,8357.3z"></path>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora073" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <path class="a" d="M11962.3,8357.3l-5891.8,5891.8c-100.6,100.6-219.7,150.9-357.3,150.9s-256.7-50.3-357.3-150.9 L4037.7,12931c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3L8254.1,8000L4037.7,3783.6 c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3l1318.1-1318.1c100.6-100.6,219.7-150.9,357.3-150.9 s256.7,50.3,357.3,150.9l5891.8,5891.8c100.6,100.6,150.9,219.7,150.9,357.3C12113.2,8137.6,12062.9,8256.7,11962.3,8357.3 L11962.3,8357.3z"></path>
            </svg>
        </div>
    </div>

</div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=2078817172409364&autoLogAppEvents=1"></script>
<div class="footer-wrapper type2">
    <footer class="foooter-container">
        <div class="container">
            <div class="footer-middle">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12 animated footer-col">
                        <div class="contact-footer">
                            <div class="logo-footer">
                                <a href="<?= base_url(); ?>"><img src="<?= $base_url ?>assets/images/logooriginal.png" alt=""></a>
                            </div>
                            <div class="contact-desc">
                                <p class="text-light">​An ever-vibrant online community of Bangladeshi Friend belonging to SSC 1999.</p>
                            </div>
                            <div class="contact-phone-email">
                                <span class="contact-phone"><a href="#">+10872229</a> | <a href="#">+10872228 </a> </span>
                                <span class="contact-email"><a href="#">info@ssc99.org</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12  col-xs-12 animated footer-col">
                        <div class="links-footer">
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                    <h6 class="heading-bold">SSC 99</h6>
                                    <ul class="list-unstyled no-margin">
                                        <li><a href="<?= base_url('About'); ?>">About US</a></li>
                                        <li><a href="<?= base_url('About/rules'); ?>">Group Rules</a></li>
                                        <li><a href="<?= base_url('Media/image_gallery'); ?>">Gallery</a></li>
                                        <li><a href="#">Videos</a></li>
                                    </ul>
                                </div>

                                <div class="col-sm-8 col-xs-12">
                                    <div class="fb-group" data-href="https://www.facebook.com/groups/ssc99bangladesh/" data-width="280" data-show-social-context="true" data-show-metadata="false"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12 animated footer-col">
                        <div class="links-social">
                            <div class="login-dashboard">
                                <a href="#" onclick="disclaimer_modal();" class="bg-color-theme text-center text-regular">Login </a>
                            </div>
                            <ul class="list-inline text-center">
                                <li><a target="_blank" href="https://www.youtube.com/channel/UCJTnEnnLBu_LAqtUnDqfuXQ"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                <li><a  target="_blank" href="https://www.facebook.com/groups/ssc99bangladesh/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom text-center">
                <p class="copyright text-light">©<?= date('Y'); ?> All rights reserved by SSC'99</p>
            </div>
        </div>
    </footer>
</div>
<div class="bg-popup"></div>
<div class="wrapper-popup">
    <a href="javascript:void(0)" class="close-popup"><span class="lnr lnr-cross-circle"></span></a>
    <div class="popup-content">
        <!--content-popup   -->

    </div>
</div>

<div id="zoomInDown1" class="modal modal-adminpro-general modal-zoomInDown fade zoomInDown animated in" role="dialog" style="display: hidden;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <center>দৃষ্টি আকর্ষন   </center>
            </div>
            <div class="modal-body">
                <p style="color:black;">
                <h3>  SSC'99 গ্রুপের মেম্বার রেজিষ্ট্রেশন পেজে আপনাদের স্বাগতম। </h3><br><br>

                রেজিষ্ট্রেশনের পূর্বে,  আমাদের <a  target="_blank" href="<?= base_url('About/rules'); ?>"><button type="button" class="btn-default">  গ্রুপ নীতিমালা </button></a>  দেখে নেবার জন্য অনুরোধ করছি। সম্মতি বাটন এ ক্লিক করার মাধ্যমে আপনি গ্রুপের নীতিমালার সাথে আপনার সম্মতি জানাচ্ছেন।<br><br> 

                আমাদের গ্রুপে রেজিষ্ট্রেশনের পূর্ব শর্ত হলো সদস্যদের ১৯৯৯ সালের এসএসএসি পরীক্ষার্থী হতে হবে। যেহেতু এটাই এই গ্রুপ এ যোগদানের মূল শর্ত, তাই রেজিষ্ট্রেশন প্রক্রিয়ায় ১৯৯৯ সালে এসএসসি তে অংশগ্রহনের কোনো একটি ডকুমেন্ট অন্তর্ভুক্তির প্রয়োজনীয়তা অন্তর্ভুক্ত করা হয়েছে। <br><br> </p>
            </div>
            <div class="modal-footer">
                <a href="<?= base_url('Auth/registration'); ?>">
                    <button type="button" class="btn btn-success">I Agree</button>
                </a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="myModalss" class="modal fade zoomInDown animated in" role="dialog" style="margin-top: 100px;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close1" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">How to become a registered user?</h4>
            </div>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9s3ffh6moZ4" allowfullscreen></iframe>
            </div>

            <div class="modal-footer">
                <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div id="user_modalall" class="modal modal-adminpro-general modal-zoomInDown fade zoomInRight animated in" role="dialog" style="display: hidden;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Details Information for:   <b id="name2"></b>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"style="overflow-x:auto;">
                <table class="table table-bordered nomargin" id="export_pdf">
                    <tbody style="background-color: #e6e9ec;  color: black; padding: 10px;">

                        <tr>
                            <td style="text-align: left;"><b class="left">Full Name:</b></td>
                            <td style="text-align: left;" id="name3"></td>
                            <td style="text-align: left;"><b class="left">Profession:</b></td>
                            <td style="text-align: left;" id="Profession1"></td>
                        </tr>

                        <tr>
                            <td style="text-align: left;"><b class="left">Edu.Board:</b></td>
                            <td style="text-align: left;" id="edu1"></td>
                            <td style="text-align: left;"><b class="left">Present Location : </b></td>
                            <td style="text-align: left;" id="loc1"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><b class="left">Email:</b></td>
                            <td style="text-align: left;" id="email1"></td>
                            <td style="text-align: left;"><b class="left">Division : </b></td>
                            <td style="text-align: left;" id="division1"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><b class="left">District:</b></td>
                            <td style="text-align: left;" id="district1"></td>
                            <td style="text-align: left;"><b class="left">School Name : </b></td>
                            <td style="text-align: left;" id="school"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><b class="left">Ac.Group:</b></td>
                            <td style="text-align: left;" id="ac_group1"></td>
                            <td style="text-align: left;"><b class="left">DOB : </b></td>
                            <td style="text-align: left;" id="dob1"></td>
                        </tr>

                        <tr>
                            <td style="text-align: left;"><b class="left">Blood Group : </b></td>
                            <td style="text-align: left;" colspan="3" id="blood_group1"></td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=458471714941754&autoLogAppEvents=1"></script>
<script>
                    function show_detailsdata(user_id) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('member/getDetailsData'); ?>",
                            data: 'id=' + user_id,
                            success: function(data) {
                                var outputData = JSON.parse(data);
                                var response = outputData.memberdata;
                                $("#name2").text(response.name);
                                $("#name3").text(response.name);

                                if (response.edu_board == null) {
                                    $("#edu1").text('N/A');
                                } else {
                                    $("#edu1").text(response.edu_board);
                                }

                                if (response.present_loc == null) {
                                    $("#loc1").text('N/A');
                                } else {
                                    $("#loc1").text(response.present_loc);
                                }

                                if (response.fblink == null) {
                                    $("#fblink1").text(' ');
                                } else {
                                    $('#fblink1').html('<a target="_blank" href="' + response.fblink + '">' + '<img src="' + '<?= $base_url; ?>' + 'assets/images/fbprofile.png" style="width: 50px; height:50px;"/>' + '</a>');
                                }
                                $("#phone1").text(response.phone);
                                $("#email1").text(response.email);
                                $("#division1").text(response.divisionname);
                                $("#district1").text(response.districtname);
                                $("#school1").text(response.school);
                                $("#ac_group1").text(response.ac_group);
                                if (response.dob == '1970-01-01') {
                                    $("#dob1").text('N/A');
                                } else {
                                    $("#dob1").text(response.dob);
                                }
                                $("#Profession1").text(response.Profession);
                                $("#permanent_address1").text(response.permanent_address);
                                $("#present_address1").text(response.present_address);
                                if (response.blood_group == '0') {
                                    $("#blood_group1").text('N/A');
                                } else {
                                    $("#blood_group1").text(response.group_name);
                                }
                                $('#proimage').html('<a target="_blank" href="<?php echo $base_url; ?>assets/images/webimg/' + response.image_path + '">' + '<img src="' + '<?= $base_url; ?>' + 'assets/images/webimg/' + response.image_path + '" style="width: 50px; height=50px;"/>' + '</a>');
                                $('#doc').html('<a target="_blank" href="<?php echo $base_url; ?>assets/images/doc/' + response.personal_doc + '">' + '<img src="' + '<?= $base_url; ?>' + 'assets/images/doc/' + response.personal_doc + '" style="width: 80px; height=50px;"/>' + '</a>');
                                $('#user_modalall').modal('show');
                            }
                        });

                    }

                    $("#myModalss").on('hidden.bs.modal', function(e) {
                        $("#myModalss iframe").attr("src", $("#myModalss iframe").attr("src"));
                    });

                    function send_message() {
                        var name = $("#name").val();
                        var phone = $("#phone").val();
                        var message = $("#msg").val();

                        if (name == '' || phone == '' || message == '') {
                            $("#failed").show();
                            $("#success").hide();
                        }
                        else {
                            $.ajax({
                                type: "GET",
                                url: "<?= base_url('About/send_message'); ?>",
                                data: {
                                    name: name,
                                    phone: phone,
                                    message: message
                                }
                            });
                            $("#name").val("");
                            $("#phone").val("");
                            $("#msg").val("");
                            $("#failed").hide();
                            $("#success").show();
                        }
                    }


                    function isNumberKey2(evt) {
                        var charCode = (evt.which) ? evt.which : evt.keyCode;
                        if (charCode > 31 && (charCode < 48 || charCode > 57))
                            return false;
                        return true;
                    }

                    if ($('#back-to-top').length) {
                        var scrollTrigger = 100, // px
                                backToTop = function() {
                                    var scrollTop = $(window).scrollTop();
                                    if (scrollTop > scrollTrigger) {
                                        $('#back-to-top').addClass('show');
                                    } else {
                                        $('#back-to-top').removeClass('show');
                                    }
                                };
                        backToTop();
                        $(window).on('scroll', function() {
                            backToTop();
                        });
                        $('#back-to-top').on('click', function(e) {
                            e.preventDefault();
                            $('html,body').animate({
                                scrollTop: 0
                            }, 700);
                        });
                    }
                    function disclaimer_modal() {

                        $('#zoomInDown1').modal('show');
                    }



                    $(document).ready(function() {
                        $("#content-slider").lightSlider({
                            loop: true,
                            keyPress: true,
                            auto: true,
                            loop2: true,
                            speed: 500
                        });
                        $('#image-gallery').lightSlider({
                            gallery: true,
                            item: 1,
                            thumbItem: 9,
                            slideMargin: 0,
                            speed: 500,
                            auto: true,
                            loop: true,
                            onSliderLoad: function() {
                                $('#image-gallery').removeClass('cS-hidden');
                            }
                        });
                    });

                    $(document).ready(function() {
                        $('#example').DataTable({
                            "iDisplayLength": 50
                        });

                    });


</script>

<script>

    function openForm() {
        document.getElementById("myForm").style.display = "block";
    }

    function closeForm() {
        document.getElementById("myForm").style.display = "none";
    }

    var myIndex = 0;
    carousel();

    function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        myIndex++;
        if (myIndex > x.length) {
            myIndex = 1
        }
        x[myIndex - 1].style.display = "block";
        setTimeout(carousel, 3000); // Change image every 2 seconds
    }

    var slideIndex = 0;
    showDivs(slideIndex);

    function plusDivs(n) {
        showDivs(slideIndex += n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        if (n > x.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = x.length
        }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[slideIndex - 1].style.display = "block";
    }

    window.onscroll = function() {
        myFunction()
    };

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }


</script>
<script src="<?= $base_url ?>assets/js/libs/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
<script src="<?= $base_url ?>src/js/lightslider.js"></script> 
<script src="<?= $base_url ?>assets/js/summernote.js"></script>
<script src="<?= $base_url ?>assets/js/libs/owl.carousel.min.js"></script>
<script src="<?= $base_url ?>assets/js/libs/jquery.meanmenu.js"></script>
<script src="<?= $base_url ?>assets/js/libs/jquery.syotimer.js"></script>
<script src="<?= $base_url ?>assets/js/libs/parallax.min.js"></script>
<script src="<?= $base_url ?>assets/js/libs/jquery.waypoints.min.js"></script>
<script src="<?= $base_url ?>assets/js/custom/main.js"></script>