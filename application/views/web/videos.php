<div class="content-wrapper" style="margin-top: 40px;">
    <div class="account-page login text-center">
        <div class="container" style="border: 1px solid black;">
            <div class="account-title">
                <h4 class="heading-light">Our Videos</h4>
            </div>
            <?php
            if ($this->session->userdata('login_error')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Meaasge !!! </strong> ' . $this->session->userdata('login_error') . '</div>';
                $this->session->unset_userdata('login_error');
            endif;
            ?>
            <div class="account-content">
                <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                <div class="elfsight-app-a751bf0f-6390-4e90-be39-147a6183585b"></div>
            </div>
        </div>
    </div>
</div>
