<div class="content-wrapper" style="margin-top: 10px;" >
    <div class="latst-article">
        <div class="container">
            <div class="area-img">
                <img src="<?= $base_url ?>assets/images/Blog-Header.jpg" alt="">
            </div>
            <div class="area-content">
                <div class="category animated fadeIn">
                    <a href="<?= base_url('Blog'); ?>" class="bnt text-regular">Blog SSC-99</a>
                </div>
                <div class="article-title">
                    <h2 class="text-regular text-capitalize animated rollIn" style="color:black">Share Your Golden Memories/knowledge with Friends</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-main col-lg-8 col-md-7 col-xs-12"style="border: 1px solid grey; padding: 8px;">
                    <div class="articles">

                        <?php foreach ($all_blog as $value): ?>
                            <div class="article-item">
                                <div class="area-img">
                                    <div class="hovereffect">
                                        <img class="img-responsive" src="<?= $base_url ?>assets/images/blogimg/<?= $value->fetured_image; ?>" alt="">
                                        <div class="overlay">
                                            <h2><?= $value->blog_tilte; ?></h2>
                                            <a class="info" href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>">Details</a>
                                        </div>
                                    </div>

                                </div>
                                <div class="area-content">

                                    <div class="article-right col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left " style="border: 1px solid #E0E4E6; ">
                                        <h3><a href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>"><?= $value->blog_tilte; ?></a></h3>
                                        <p>
                                            <?php
                                            $big = "$value->details";
                                            $string = substr($big, 0, 1200) . '...';
                                            echo $string;
                                            ?><button type="button" class="btn-link"><a  type="button" class="btn btn-primary btn-lg"href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>">Read More</a></button>
                                        </p>

                                        <div class="stats">
                                            <span class="icon user-icon"></span>
                                            <a href="#" onclick="show_detailsdata('<?= $value->created_by; ?>');">
                                                <span class="text-content text-light"> by <b><?= $value->name; ?></b> &nbsp;<img  class="zoom" src="<?= base_url(); ?>assets/images/webimg/<?= $value->image_path; ?>"></span>
                                            </a>
                                            <span class="clock">
                                                <span class="icon clock-icon"></span>
                                                <span class="text-center text-light"><?= date("F j, Y", strtotime($value->created_date)); ?></span>
                                            </span>
                                            <span class="comment">
                                                <span class="icon comment-icon"></span>
                                                <span class="text-center text-light"> <?php
                                                    $comment_count = $this->db->query("SELECT COUNT(id) AS totl FROM blog_comment WHERE blog_id='$value->id'")->row()->totl;

                                                    if (!empty($comment_count)):
                                                        echo $comment_count . ' Comments';
                                                    else:
                                                        echo 'No comments available';
                                                    endif;
                                                    ?>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>



                    </div>
                    <div class="pagination-wrapper text-center">
                        <ul class="pagination">
                            <li class="prev"><a href="#">Previous</a></li>
                            <li class="current"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li class="next"><a href="#">Next</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar blog-right col-lg-4 col-md-5 hidden-sm hidden-xs animated zoomInLeft"style="background-color:#e0e4e6;color:black; ">
                    <?php $this->load->view('web/blog_sidebar'); ?>
                </div>
            </div>
        </div>
    </div>


