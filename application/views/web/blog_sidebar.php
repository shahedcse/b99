
<div class="block-sidebar">
    <div class="block-item popurlar-port">
        <div class="block-title text-center" >
            <h5 class="text-regular text-uppercase" style="background-color:black; color:#fff;">Recent Post</h5>
        </div>
        <div class="block-content">
            <?php
            $all_blog = $this->db->query("SELECT blog.*,users.name FROM blog JOIN users ON users.id=blog.created_by order by id desc LIMIT 5")->result();
            ?>
            <ul>
                <?php foreach ($all_blog AS $value): ?>
                    <li>
                        <div class="area-img">
                            <img src="<?= $base_url ?>assets/images/blogimg/<?= $value->fetured_image; ?>" alt="">
                        </div>
                        <div class="area-content">
                            <h6><a href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>"><?= $value->blog_tilte; ?></a></h6>
                            <div class="stats">
                                <span class="clock">
                                    <span class="icon clock-icon"></span>
                                    <span class="text-content text-light"><?= date("F j, Y", strtotime($value->created_date)); ?></span>
                                </span>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>
    <div class="block-item twitter">
        <div class="block-title text-center">
            <h5 class="text-regular text-uppercase" style="background-color:black; color:#fff;">Facebook</h5>
        </div>
        <div class="block-content">
            <div class="twitter-wrapper text-center">
                <div class="twitter-icon color-theme">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </div>
                <div class="twitter-content">
                    <div class="twitter-desc">
                        <div class="fb-group" data-href="https://www.facebook.com/groups/ssc99bangladesh/" data-width="280" data-show-social-context="true" data-show-metadata="false"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block-item tag">
        <div class="block-title text-center">
            <h5 class="text-regular text-uppercase" style="background-color:black; color:#fff;">POPULAR TAGS</h5>
        </div>
        <?php
        $all_blogcat = $this->db->query("SELECT * FROM blog_category")->result();
        ?>
        <div class="block-content">
            <ul class="list-inline">
                <?php foreach ($all_blogcat AS $value): ?>
                    <li><a href="#"><?= $value->category_name; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
