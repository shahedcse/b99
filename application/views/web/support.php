<div class="content-wrapper">
    <div class="account-page login text-center">
        <div class="container">
            <div class="account-title">
                <h4 class="heading-light">Fill up The Support Form :</h4>
            </div>
            <?php
            if ($this->session->userdata('add')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('notadd')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                $this->session->unset_userdata('notadd');
            endif;
            ?>
            <div class="account-content col-md-5" style=" padding: 15px;">
                <img class="img-responsive animated zoomInRight" src="<?= base_url('assets/images/support_page-1.png'); ?>">
            </div>

            <div class="account-content col-md-7" style="border:1px solid black;  padding: 15px;">
                <form  action="#" method="Post">
                    <div  id="success" style="display:none">
                        <div class="alert alert-success fade in">
                            <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                            <strong>Success!!</strong> message send Successfully. </div>
                    </div>

                    <div  id="failed" style="display:none">
                        <div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button> <strong>Alert!</strong> Please fill Up all fields. </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Name :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="name" name="name" placeholder="Your Name" class="form-control" required="true" value="" type="text"></div>
                        </div><br>
                    </div><br>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone No. :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="phone"onkeypress='return isNumberKey2(event)' name="phone" placeholder="Your Phone" class="form-control" required="true" value="" type="text"></div>
                        </div><br>
                    </div><br>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Your Message :<span style="color:red">*</span></label>
                        <div class="col-md-6 inputGroupContainer">
                            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><textarea id="msg" name="msg" placeholder="Code" class="form-control" required="true" value="" type="text">
                                </textarea>
                            </div>
                        </div><br>
                    </div><br>
                    <div class="buttons-set">
                        <button  type="button" onclick="send_message();"  title="Submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




