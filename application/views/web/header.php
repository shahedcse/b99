<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="icon" href="<?= $base_url ?>assets/images/mainlogo-small.png" type="img">
        <link rel="stylesheet" type="text/css" href="<?= $base_url ?>assets/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="<?= $base_url ?>assets/css/animate.css" />
        <link rel="stylesheet" type="text/css" href="<?= $base_url ?>assets/css/owl.carousel.css" />
        <link rel="stylesheet" type="text/css" href="<?= $base_url ?>assets/css/styles.css" />
        <link rel="stylesheet" type="text/css" href="<?= $base_url ?>assets/css/meanmenu.css" />
        <link rel="stylesheet" type="text/css" href="<?= $base_url ?>assets/css/font-awesome.min.css" />
        <script src="<?= $base_url ?>assets/js/libs/jquery-2.2.4.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link href="<?= $base_url ?>assets/css/summernote.css" rel="stylesheet">
        <link rel="stylesheet"  href="<?= $base_url ?>src/css/lightslider.css"/>
        <link rel="stylesheet" href="<?= $base_url ?>assets/css/icon-font.min.css">
        <script src="<?= $base_url ?>assets/js/libs/modernizr.custom.js"></script>
        <script src="<?= $base_url ?>assets/slider/js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>

        <link rel="stylesheet" href="<?= $base_url ?>assets/news/css/bootstrap-theme.min.css">
        <link href="<?= $base_url ?>assets/news/css/site.css" rel="stylesheet" type="text/css" />
        <link href="<?= $base_url ?>assets/news/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <script src="<?= $base_url ?>assets/news/scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>


        <title><?= $page_title; ?></title>

        <style>
            #back-to-top {
                position: fixed;
                bottom: 2px;
                right: 40px;
                z-index: 9999;
                width: 32px;
                height: 32px;
                text-align: center;
                line-height: 30px;
                background: #555;
                color: #fff;
                cursor: pointer;
                border: 0;
                border-radius: 2px;
                text-decoration: none;
                transition: opacity 0.2s ease-out;
                opacity: 0;
            }
            #back-to-top:hover {
                background: red;
            }
            #back-to-top.show {
                opacity: 1;
            }
            #content {
                height: 2000px;
            }
            .hovereffect {
                width: 100%;
                height: 100%;
                float: left;
                overflow: hidden;
                position: relative;
                text-align: center;
                cursor: default;
            }

            .hovereffect .overlay {
                width: 60%;
                height: 100%;
                position: absolute;
                overflow: hidden;
                top: 0;
                left: 0;
                background-color: rgba(0,0,0,0.6);
                opacity: 0;
                filter: alpha(opacity=0);
                -webkit-transform: translate(460px, -100px) rotate(180deg);
                -ms-transform: translate(460px, -100px) rotate(180deg);
                transform: translate(460px, -100px) rotate(180deg);
                -webkit-transition: all 0.2s 0.4s ease-in-out;
                transition: all 0.2s 0.4s ease-in-out;
            }

            .hovereffect img {
                display: block;
                position: relative;
                -webkit-transition: all 0.2s ease-in;
                transition: all 0.2s ease-in;
            }

            .hovereffect h2 {
                text-transform: uppercase;
                color: #fff;
                text-align: center;
                position: relative;
                font-size: 13px;
                padding: 10px;
                background: rgba(0, 0, 0, 0.6);
            }

            .hovereffect a.info {
                display: inline-block;
                text-decoration: none;
                padding: 7px 14px;
                text-transform: uppercase;
                color: #fff;
                border: 1px solid #fff;
                margin: 50px 0 0 0;
                background-color: transparent;
                -webkit-transform: translateY(-200px);
                -ms-transform: translateY(-200px);
                transform: translateY(-200px);
                -webkit-transition: all 0.1s ease-in-out;
                transition: all 0.1s ease-in-out;
            }

            .hovereffect a.info:hover {
                box-shadow: 0 0 5px #fff;
            }

            .hovereffect:hover .overlay {
                opacity: 1;
                filter: alpha(opacity=100);
                -webkit-transition-delay: 0s;
                transition-delay: 0s;
                -webkit-transform: translate(0px, 0px);
                -ms-transform: translate(0px, 0px);
                transform: translate(0px, 0px);
            }

            .hovereffect:hover h2 {
                -webkit-transform: translateY(0px);
                -ms-transform: translateY(0px);
                transform: translateY(0px);
                -webkit-transition-delay: 0.5s;
                transition-delay: 0.5s;
            }

            .hovereffect:hover a.info {
                -webkit-transform: translateY(0px);
                -ms-transform: translateY(0px);
                transform: translateY(0px);
                -webkit-transition-delay: 0.2s;
                transition-delay: 0.2s;
            }

        </style>
    </head>
    <div id="fb-root"></div>

    <div class="header-wrapper header-position">
        <header id="header" class="container-header type1">
            <div class="top-nav" style="background-color: orangered;">
                <div class="container">
                    <div class="row">
                        <div class="top-left col-sm-6 hidden-xs" >
                            <ul class="list-inline" >
                                <li>
                                    <a href="#">
                                        <span class="icon mail-icon"></span>
                                        <span style="color:#fff;"class="text">Info@ssc99.org</span>
                                    </a>
                                </li>
                                <li>

                                </li>
                            </ul>
                        </div>
                        <?php
                        if (!empty($this->session->userdata("user_role"))):
                            $id = $this->session->userdata('user_id');
                            $allinfo = $this->db->query("SELECT * FROM users where id='$id'")->row();
                            ?>
                            <div class="top-right col-sm-6 col-xs-12" >
                                <ul class="list-inline">
                                    <li style="color:#fff;">ID - <?= 'CLUB ' . $allinfo->member_id; ?> </li>
                                    <img  height="25px" style="border-radius: 30%;" width="30px;"src="<?= base_url(); ?>assets/images/webimg/<?= $allinfo->image_path; ?>">
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"style="color:#fff;" href="#">Hello!! <?= $allinfo->name; ?> <span class="caret"></span></a>
                                        <ul class="dropdown-menu" style="background-color: #fff;color:black;">
                                            <li><a href="<?= base_url('Auth/profile'); ?>">My profile</a></li>
                                            <li><a href="<?= base_url('Auth/update_profile'); ?>">Update My Info</a></li>
                                            <li><a href="<?= base_url('Blog/bloglist'); ?>">My Blog List</a></li>
                                            <li><a href="<?= base_url('Auth/logout'); ?>">Logout</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </div>
                        <?php else: ?>
                            <div class="top-right col-sm-6 col-xs-12">
                                <ul class="list-inline">
                                    <li class="login">
                                        <a href="<?= base_url('Auth'); ?>"><b>Log In</b></a>
                                    </li>
                                    <li class="login">
                                        <a href="#" onclick="disclaimer_modal();"><b>Register</b></a>
                                    </li>

                                    <li class="login">
                                        <a href="<?= base_url('About/Donation'); ?>"><b>Donate Now</b></a>
                                    </li>
                                </ul>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
            <div class="header-middle header" id="myHeader">
                <div class="container">
                    <div class="logo hidden-sm hidden-xs">
                        <a href="<?= base_url(); ?>"> <img  style="border-radius: 5%;"src="<?= $base_url ?>assets/images/logooriginal.png" alt="logo"></a>
                    </div>
                    <div class="menu">
                        <nav>
                            <ul class="nav navbar-nav">
                                <?php
                                $role = $this->session->userdata("user_role");
                                if ($role == 1):
                                    ?>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Admin </b><span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?= base_url('Sadmin/dashboard'); ?>">Dashboard</a></li>
                                            <li><a href="<?= base_url('Sadmin/user_panel'); ?>">User Panel</a></li>
                                            <li><a href="<?= base_url('Sadmin/event'); ?>">All Event</a></li>
                                            <li><a href="<?= base_url('Sadmin/messages'); ?>">Messages</a></li>
                                            <li><a href="<?= base_url('Sadmin/allblog'); ?>">All Blog</a></li>
                                            <li><a href="#">Upload Images</a></li>
                                        </ul>
                                    </li>
                                <?php elseif ($role == 3):
                                    ?>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Admin </b><span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?= base_url('Sadmin/dashboard'); ?>">Dashboard</a></li>
                                            <li><a href="<?= base_url('Sadmin/event'); ?>">All Event</a></li>
                                            <li><a href="<?= base_url('Sadmin/messages'); ?>">Messages</a></li>
                                            <li><a href="<?= base_url('Sadmin/allblog'); ?>">All Blog</a></li>
                                            <li><a href="#">Upload Images</a></li>

                                        </ul>
                                    </li>
                                <?php else: ?>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Batch 99</b> <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?= base_url('About'); ?>">About Us</a></li>
                                            <li><a href="<?= base_url('About/rules'); ?>">Group Rules</a></li>
                                            <li><a href="<?= base_url('About/Support'); ?>">Support</a></li>
                                        </ul>
                                    </li>
                                <?php endif; ?>

                                <?php if (!empty($this->session->userdata("user_role"))): ?>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Member List</b> <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?= base_url('Member'); ?>">All Member List</a></li>
                                            <li><a href="<?= base_url('Member/blood_donor'); ?>">Blood Donor List</a></li>
                                            <?php if ($this->session->userdata("user_role") == '1' || $this->session->userdata("user_role") == '3'): ?>
                                                <li><a href="<?= base_url('Member/inactive_user'); ?>">Inactive Member List</a></li>
                                            <?php endif; ?>
                                            <li><a href="<?= base_url('Member/professional_list'); ?>">Professional List</a></li>
                                        </ul>
                                    </li>
                                <?php else: ?>
                                    <li class="dropdown"><a class="dropdown-toggle" href="<?= base_url(); ?>"><b>Home</b></a>
                                    </li>
                                <?php endif; ?>

                                <li>
                                    <a target="_blank" href="<?= base_url('Blog'); ?>">
                                        <button type="button" class="btn btn-primary"><b>Our Blog </b></button>
                                    </a>
                                </li>


                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Media</b><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?= base_url('Media/image_gallery'); ?>">Image gallery</a></li>
                                        <li><a href="<?= base_url('Media/videos'); ?>">Videos</a></li>
                                        <li><a href="#">Publications</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="area-mobile-content visible-sm visible-xs">
                        <div class="logo-mobile">
                            <a href="<?= base_url(); ?>"> <img  style="border-radius:50%;"src="<?= $base_url ?>assets/images/logooriginalsmall.png" alt="logo"></a>
                        </div>
                        <div class="mobile-menu ">
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
</html>
