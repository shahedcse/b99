<html lang="en">
    <body>
        <div class="main-wrapper">
            <div class="content-wrapper">
                <!--begin slider-->
                <div class="slider-hero">
                    <div class="sliders-wrap columns1">
                        <div class="item">
                            <img src="<?= $base_url ?>assets/images/slider2.jpg" alt="">
                            <div class="owl-caption">
                                <div class="container">
                                    <div class="content-block">
                                        <h2 class="text-center">
                                            <span class=" item-1">Together Make a Platform </span> <br />
                                            <span class=" item-2">All 99 Friends in one platform.</span> <br />
                                            <span class=" item-3">Friends belonging to SSC 1999 Join With us</span>

                                        </h2>
                                        <?php if (empty($this->session->userdata('user_role'))): ?>
                                            <a href="<?= base_url('Auth'); ?>" class="bnt success read-story ">Login</a>&nbsp;&nbsp;
                                            <a href="#" onclick="disclaimer_modal();" class="bnt success read-story">Register</a>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?= $base_url ?>assets/images/slider1.jpg" alt="">
                            <div class="owl-caption">
                                <div class="container">
                                    <div class="content-block">
                                        <h2>
                                            <span class="text-bold">All Friends In One Platform</span> <br/>
                                        </h2>
                                        <a href="<?= base_url('About'); ?>" class="bnt bnt-theme read-story">READ Story</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <marquee style="background-color: honeydew;font-size: 20px;" behavior="scroll" direction="left">Welcome To SSC 99 Bangladesh.​An ever-vibrant online community of Bangladeshi Friend belonging to SSC 1999.</marquee>

                <div class="alumni-dashboard">
                    <div class="container">
                        <div class="area-content">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <img src="<?= base_url(); ?>assets/images/friedns.jpg" style="height: 80px; width: 100px;" class="img-circle img-thumbnail">
                                            <b>আমরা কারা ?</b> <br>
                                            এসএসসি পাস করার পর শুভ্রর সাথে আমার আর কখনো দেখা হয়নি। শুভ্র ছিলো আমাদের স্কুলের সব চেয়ে লাজুক বন্ধু, মিন মিনেও বটে। পরীক্ষার সময় আমাকে ওর খাতা দেখতে দেয়নি বলে ওকে সারা স্কুলের মাঠ জুড়ে ধাওয়া করেছিলাম। সে সময়টা মনে হলেই এখনও আমি হেসে কুটি কুটি হই। এতোদিন পরে সেই শুভ্রকে খুজে পেয়েছি দুদিন আগে ssc’99 গ্রুপে। মিনমিনে লাজুক শুভ্র এখন দেশের একজন বিখ্যাত আর্টিষ্ট, বিশাল এক আর্ট গ্যালারী চালায়। ছোটো বেলার সেই লাজুক বন্ধুটির সাথে কতোদিন পরে কথা হলো, চা এর দাওয়াত ওর গ্যালারীতে, ভেজা চোখে বুকে জড়িয়ে ধরা, বন্ধু...  <a href="<?= base_url('About'); ?>"button type="button" class="btn-link">Read more</a>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box-content img-responsive animated zoomIn" >
                                        <div class="fb-group" data-href="https://www.facebook.com/groups/ssc99bangladesh/" data-width="280" data-show-social-context="true" data-show-metadata="false"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><b>All News & Event</b></div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <ul class="demo1">
                                                                    <?php foreach ($all_event as $value): ?>
                                                                        <li class="news-item">
                                                                            <table cellpadding="4">
                                                                                <tr>
                                                                                    <td><img src="<?= base_url(); ?>assets/news/images/img_132233.png" width="60" class="img-circle" /></td>
                                                                                    <td>  <?= $value->event_tilte; ?> ...... <a  type="button" class="btn-link"href="<?= base_url('Home/View_event?id=' . $value->id); ?>">View Details </a> </td>
                                                                                </tr>
                                                                            </table>
                                                                        </li>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer"></div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end slider-->

                <!--begin upcoming event-->
                <div class="upcoming-event">
                    <div class="container">
                        <div class="row">
                            <div class="area-img col-md-6 col-sm-12 col-xs-12" >
                                <div class="w3-content w3-section" style="max-width:500px;">
                                    <?php foreach ($all_images as $value): ?>
                                        <img class="mySlides" src="<?= base_url(); ?>assets/images/gallery/<?= $value->image_path ?>" style="width:100%">    
                                    <?php endforeach; ?>
                                </div>
                                <button class="w3-button w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                                <button class="w3-button w3-display-right" onclick="plusDivs(+1)">&#10095;</button>
                            </div>
                            <div class="area-content col-md-6 col-sm-12 col-xs-12">
                                <div class="area-top">
                                    <div class="row">
                                        <div class="col-sm-10 col-xs-9">
                                            <h2 class="heading-bold animated fadeInLeft">Reconnect with Friends</h2>
                                            <h5 class=" animated fadeInLeft">Reunify with long-lost school and college life friends, rekindle old friendship, relive old memories.​An ever-vibrant online community of Bangladeshi Friend belonging to SSC 1999.</h5><br><br>
                                            <b> Total Visitor:</b>   <img src="https://smallseotools.com/counterDisplay?code=0b4694116d32dd7cfaa722e226679ceb&style=0007&pad=5&type=page&initCount=1" alt="Traffic Counter by Small Seo Tools" border="0"><br>
                                        </div>
                                        <div class="col-sm-2 col-xs-3">
                                            <div class="area-calendar calendar animated slideInRight">
                                                <span class="day text-bold"><?= date('d'); ?></span>
                                                <span class="month text-light"><?= date('M'); ?></span>
                                                <span class="year text-light bg-year"><?= date('Y'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="area-bottom">
                                    <a href="#" onclick="disclaimer_modal();" class="bnt bnt-theme join-now pull-right animated fadeIn">Join Now</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--end upcoming event-->
                <div class="upcoming-event">
                    <div class="container">
                        <h2 class="text-center" style="padding-bottom:5px;">
                            <span class="">Our Recent Blog post </span> <br/>
                        </h2>
                        <div class="row">
                            <?php foreach ($all_blog as $value): ?>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="alumni-story-wrapper">
                                        <div class="alumni-story-img">
                                            <a href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>">
                                                <img class="img-responsive" style="height: 250px;"  src="<?= $base_url ?>assets/images/blogimg/<?= $value->fetured_image; ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="alumni-story-content">
                                            <h3 class="heading-regular"><a href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>"><?= $value->blog_tilte; ?></a></h3>
                                            <p class="text-light">
                                                <?php
                                                $big = $value->details;
                                                $string = substr($big, 0, 600) . '...';
                                                echo $string;
                                                ?><button type="button" class="btn-success"><a href="<?= base_url('Blog/blog_view?id=' . $value->id); ?>">read more </a></button>
                                            </p>
                                            <span class="dates text-light"><?= date("F j, Y", strtotime($value->created_date)); ?> -- by  <?= $value->name; ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                <!--end alumni interview-->

            </div>
        </div>
    </body>
</html>
<?php if (empty($this->session->userdata("user_role"))): ?>
    <script>
        $(window).on('load', function() {
            $('#myModalss').modal('show');
            $('.ytp-large-play-button').trigger('click');
        });
    </script>
<?php endif; ?>
<script>

    $(function() {
        $(".demo1").bootstrapNews({
            newsPerPage: 3,
            autoplay: true,
            pauseOnHover: true,
            direction: 'up',
            newsTickerInterval: 2000,
            onToDo: function() {
                //console.log(this);
            }
        });


    });

</script>