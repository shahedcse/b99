<div class="content-wrapper" style="margin-top: 50px;">
    <div class="container">
        <div class="alumni-directory">
            <?php
            if ($this->session->userdata('add')):
                echo '<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Success Message !!! </strong> ' . $this->session->userdata('add') . '</div>' . '<br>' . '<br>';
                $this->session->unset_userdata('add');
            elseif ($this->session->userdata('notadd')):
                echo '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><strong>Failed Meaasge !!! </strong> ' . $this->session->userdata('notadd') . '</div>';
                $this->session->unset_userdata('notadd');
            endif;
            ?>
            <div class="top-section">
                <div class="row">
                    <div class="title-page text-left col-md-6 col-sm-12 col-xs-12">
                        <h4 class="text-regular"> Inactive Member List</h4>
                    </div>
                    <div class="search-alumni-directory text-right col-md-6 col-sm-12 col-xs-12">
                        <form class="navbar-form no-margin no-padding">

                            <button type="button" class=" bg-color-theme text-center text-regular">Type keyword in Search box</button>
                        </form>
                    </div>
                </div>
            </div>
            <span id="verific" style="font-size: 25px;"></span>
            <div class="alumni-directory-content">

                <table id="example" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Photo</th>
                            <th>Present Location</th>
                            <th>School Name</th>

                            <?php if ($use_role == 1): ?>
                                <th>Options</th>
                            <?php endif; ?>
                            <?php if ($use_role == 2): ?>
                                <th>Contact</th>
                            <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($allmember as $value):
                            $bloodqr = $this->db->query("SELECT blood_group FROM allblood_group WHERE id='$value->blood_group'")->row();
                            if (!empty($bloodqr)):
                                $blood = $bloodqr->blood_group;
                            else:
                                $blood = 'N/A';
                            endif;
                            ?>
                            <tr>
                                <td><?= $value->name; ?></td>
                                <td>
                                    <img class="zoom"  src="<?= $base_url ?>assets/images/webimg/<?= $value->image_path; ?>" alt="">
                                </td>
                                <td><?= $value->present_loc; ?></td>
                                <td><?= $value->school; ?></td>

                                <?php if ($use_role == 1): ?>
                                    <td>
                                        <a href="#" onclick="makeactive('<?= $value->id; ?>');">
                                            <button type="button"class="btn-default">Make Active</button>
                                        </a>
                                        <a href="#" onclick="show_details('<?= $value->id; ?>');">
                                            <button type="button"class="btn-success">Details</button>
                                        </a>
                                        <a href="#" onclick="show_deletemodal('<?= $value->id; ?>');">
                                            <button type="button"class="btn-danger">Delete</button>
                                        </a>
                                    </td>
                                <?php endif; ?>
                                <?php if ($use_role == 2): ?>
                                    <td>
                                        <?php if (!empty($value->fblink)): ?>
                                            <a  target="_blank" href="<?= $value->fblink; ?>">
                                                <img src="<?= base_url(); ?>assets/images/fbprofile.png" style="width: 50px; height: 50px;">
                                            </a>
                                        <?php else: ?>
                                            N/A
                                        <?php endif; ?>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>
<div id="user_modal2" class="modal modal-adminpro-general modal-zoomInDown fade zoomInRight animated in" role="dialog" style="display: hidden;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:honeydew;">
                Details Information for:   <b id="name2"></b> <p id="fblink"></p> 
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="overflow-x:auto;">
                <table class="table table-bordered nomargin" id="export_pdf">
                    <tbody style="background-color: #e6e9ec;  color: black; padding: 10px;">

                        <tr>
                            <td style="text-align: left;"><b class="left">Full Name:</b></td>
                            <td style="text-align: left;" id="name"></td>
                            <td style="text-align: left;"><b class="left">Phone : </b></td>
                            <td style="text-align: left;" id="phone"></td>
                        </tr>

                        <tr>
                            <td style="text-align: left;"><b class="left">Edu.Board:</b></td>
                            <td style="text-align: left;" id="edu"></td>
                            <td style="text-align: left;"><b class="left">Present Location : </b></td>
                            <td style="text-align: left;" id="loc"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><b class="left">Email:</b></td>
                            <td style="text-align: left;" id="email"></td>
                            <td style="text-align: left;"><b class="left">Division : </b></td>
                            <td style="text-align: left;" id="division"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><b class="left">District:</b></td>
                            <td style="text-align: left;" id="district"></td>
                            <td style="text-align: left;"><b class="left">School Name : </b></td>
                            <td style="text-align: left;" id="school"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><b class="left">Ac.Group:</b></td>
                            <td style="text-align: left;" id="ac_group"></td>
                            <td style="text-align: left;"><b class="left">DOB : </b></td>
                            <td style="text-align: left;" id="dob"></td>
                        </tr>

                        <tr>
                            <td style="text-align: left;"><b class="left">Profession:</b></td>
                            <td style="text-align: left;" id="Profession"></td>
                            <td style="text-align: left;"><b class="left">Permanent Address : </b></td>
                            <td style="text-align: left;" id="permanent_address"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><b class="left">Present Address:</b></td>
                            <td style="text-align: left;" id="present_address"></td>
                            <td style="text-align: left;"><b class="left">Blood Group : </b></td>
                            <td style="text-align: left;" id="blood_group"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"><b class="left">Profile Image:</b></td>
                            <td style="text-align: left;" id="proimage"></td>
                            <td style="text-align: left;"><b class="left">Document: </b></td>
                            <td style="text-align: left;"  id="doc"></td>

                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="delete_modal" class="modal fade " role="dialog" style="display: hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url('Member/delete_user'); ?>" method="POST">
                <div class="modal-header" style="background-color:honeydew;">
                    <b> Alert !!</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="user_id" name="user_id" class="form-control">
                    <p style="font-size: 25px;color: red;">Are you sure,You want to delete this user ?</p><br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">YES</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>

    </div>
</div>
<script>
    function makeactive(id) {
        var user_id = id;

        var dataString = 'user_id=' + user_id;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Member/make_activate'); ?>",
            data: dataString,
            success: function(data)
            {
                if (data == 'mass') {
                    $("#verific").text("User activation Successfully .");
                    $("#verific").css('color', 'green');
                    alert('Activation successfull');
                    location.reload();
                }
            }
        });

    }

    function show_deletemodal(id) {
        var user_id = id;
        $('#user_id').val(user_id);
        $('#delete_modal').modal('show');
    }

    function show_details(user_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('member/getDetailsData'); ?>",
            data: 'id=' + user_id,
            success: function(data) {
                var outputData = JSON.parse(data);
                var response = outputData.memberdata;
                $("#name2").text(response.name);
                $("#name").text(response.name);

                if (response.edu_board == null) {
                    $("#edu").text('N/A');
                } else {
                    $("#edu").text(response.edu_board);
                }

                if (response.present_loc == null) {
                    $("#loc").text('N/A');
                } else {
                    $("#loc").text(response.present_loc);
                }

                if (response.fblink == null) {
                    $("#fblink").text(' ');
                } else {
                    $('#fblink').html('<a target="_blank" href="' + response.fblink + '">' + '<img src="' + '<?= $base_url; ?>' + 'assets/images/fbprofile.png" style="width: 50px; height:50px;"/>' + '</a>');
                }
                $("#phone").text(response.phone);
                $("#email").text(response.email);
                $("#division").text(response.divisionname);
                $("#district").text(response.districtname);
                $("#school").text(response.school);
                $("#ac_group").text(response.ac_group);
                if (response.dob == '1970-01-01') {
                    $("#dob").text('N/A');
                } else {
                    $("#dob").text(response.dob);
                }
                $("#Profession").text(response.Profession);
                $("#permanent_address").text(response.permanent_address);
                $("#present_address").text(response.present_address);
                if (response.blood_group == '0') {
                    $("#blood_group").text('N/A');
                } else {
                    $("#blood_group").text(response.group_name);
                }
                $('#proimage').html('<a target="_blank" href="<?php echo $base_url; ?>assets/images/webimg/' + response.image_path + '">' + '<img src="' + '<?= $base_url; ?>' + 'assets/images/webimg/' + response.image_path + '" style="width: 50px; height=50px;"/>' + '</a>');
                $('#doc').html('<a target="_blank" href="<?php echo $base_url; ?>assets/images/doc/' + response.personal_doc + '">' + '<img src="' + '<?= $base_url; ?>' + 'assets/images/doc/' + response.personal_doc + '" style="width: 80px; height=50px;"/>' + '</a>');
                $('#user_modal2').modal('show');
            }
        });

    }

</script>

