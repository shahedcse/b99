<?php

/**
 * @author shahed cse @clouditbd.com
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function login($uname) {
        $query = "select id, name, phone,role,member_id,status, email,image_path from users where phone = ?";
        $queryResult = $this->db->query($query, $uname);
        if ($queryResult->num_rows() > 0):
            return $queryResult->row();
        endif;
    }

}
