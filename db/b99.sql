-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2019 at 12:36 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `b99`
--

-- --------------------------------------------------------

--
-- Table structure for table `allblood_group`
--

CREATE TABLE IF NOT EXISTS `allblood_group` (
  `id` int(11) NOT NULL,
  `blood_group` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `allblood_group`
--

INSERT INTO `allblood_group` (`id`, `blood_group`) VALUES
(1, 'A+'),
(2, 'O+'),
(3, 'B+'),
(4, 'AB+'),
(5, 'A-'),
(6, 'O-'),
(7, 'B-'),
(8, 'AB-');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL,
  `blog_tilte` varchar(55) CHARACTER SET utf8 DEFAULT NULL,
  `blog_category` int(4) DEFAULT NULL,
  `fetured_image` varchar(55) DEFAULT NULL,
  `details` mediumtext CHARACTER SET utf8mb4,
  `created_by` int(2) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `blog_tilte`, `blog_category`, `fetured_image`, `details`, `created_by`, `created_date`) VALUES
(1, 'মারি ক্যুরি এর জীবন ও সংগ্রাম', 1, 'ssssxsxsxsx.jpg', '১৯৮৪ সালে দিকে মারি সবেমাত্র গভীরভাবে তাঁর গবেষণার কাজ শুরু করেছেন। সে \nসময় প্যারিসে এক পোলিশ অধ্যাপকের সাথে মারির পরিচয় হয়। এর কয়েকদিনের মধ্যে \nমারি একটি সংস্থা থেকে বিভিন্ন পদার্থের চৌম্বকের উপর বৈজ্ঞানিক গবেষণার \nজন্যে আমন', 1, '2019-07-06'),
(2, 'থটস আর অনলি থটস নট ফ্যাক্টস', 2, '7c9b7936cb9a95f341c3bae5a815cc43-5d1203e7a0df8.jpg', '<p>আজকের লেখার প্রসঙ্গ হল চিন্তা এবং নিজেকে দেখার দৃষ্টিভঙ্গি। আপনি \r\nআয়নায় দাঁড়িয়ে যদি দেখেন একটা মোটা কিংবা কালো মুখে কালচে দাগ, কিংবা চোখের\r\n নিচে কালি পড়া একটা কুৎসিত মানুষ তবে এই লেখা আপনার জন্য। সাইকোলজিতে একটা\r\n কথা আছে ‘<em>থটস আর অনলি থটস নট ফ্যাক্টস”</em>। এর মানে হল , চিন্তা \r\nবিষয়টা মস্তিষ্কের নিউরোনগুলোর তড়িৎরাসায়নিক সংকেতগুলো ছাড়া আসলে আর কিছু\r\n না। চিন্তা মানেই বিষয়টা সত্যি এমন না, আর এই চিন্তা নিয়ন্ত্রিত হয় বলে \r\nআমরা যা দেখি তা আসলে সত্যি না।</p>\r\n<p>আজকের বিষয় মিডিয়া ম্যানুপুলেশন না।কিন্তু এর সাথে মিডিয়া ম্যানুপুলেশন \r\nজড়িত বলে প্রথমে মিডিয়া ম্যানুপুলেশন নিয়ে আলোচনা করা জরুরী । মিডিয়া \r\nম্যানুপুলেশন সাথে হয়ত অনেকেই পরিচিত, সহজ করে বললে, এর মানে হল আমাদের \r\nচিন্তা এবং মন এই মিডিয়া দ্বারা নিয়ন্ত্রিত হয় কিংবা নিয়ন্ত্রণ করা হয়। \r\nউদাহরণ দেই, এই যে মেয়েদের শরীরের এর শেপ ৩৬-২৪-৩৬ হলে সেটা পারফেক্ট বডি, \r\nএই কনসেপ্ট আপনি অবশ্যই জানেন, কিন্তু আপনি কিভাবে জানেন? আপনার মাথায় কি \r\nকরে এল,কে এই মিজারমেন্ট করল আর কেন করল সেটা ভেবেছেন?<br>\r\nউত্তর দিচ্ছি, তার আগেএকটু ইতিহাস এর দিকে তাকাই, এই নারী শরীর নিয়ন্ত্রণের\r\n কনসেপ্ট নতুন না। চীনে মেয়েদের পা এর আঁকার ছোট করা, মিশরে সমস্ত চুল ফেলে\r\n দিয়ে পরচুল ব্যবহার, গলার উচ্চতা বাড়ানোর জন্য করেন উপজাতিদের রিং এর \r\nব্যবহার, সবই নারী শরীর নিয়ন্ত্রেনের উদাহরণ। কেউ ধর্ম, কেউ রীতি কিংবা কেউ\r\n সৌন্দর্যের নামে নারী শরীরের এই পরিবর্তন কে জায়েজ করেছে।<br>\r\nব্রিটিশ ও ইউরোপিয়ান সম্ভ্রান্ত নারীরা লম্বা গাউনের নিচে এক ধরনের শক্ত \r\nঅন্তর্বাস এবং বডি শেইপার ব্যবহার করতেন যাতে তাদের স্তন্য, কোমর এবং \r\nনিতম্বের আঁকার পরিবর্তিত হত। যেহেতু ক্ষমতাধর এলিট সোসাইটি ঘাস খেলে সেটাই\r\n হয় স্ট্যান্ডার্ড তাই পরবর্তীতে সাধারণ নারীরা ঐ সব নারীদের কে অনুকরণ \r\nকরতে বডি শেইপিং শুরু করে।</p>\r\n<p>আর বর্তমানে বডি অপ্সেশনের কনসেপ্ট লুফে নিয়েছে ক্যাপিটালিজম। ৩৬-২৪-৩৬ \r\nকনসেপ্ট হল, এই বডি অপ্সেশন কে কাজে লাগিয়ে ক্যাপিটালিজম প্রসারের ঠিকাদার \r\nমিডিয়ার উগড়ে দেয়া জঙ্গাল। জঙ্গাল বলার কারন, ২৪ কোমর মানে অস্বাভাবিক রকম \r\nসরু কোমর, মেডিক্যাল সাইন্স এর ভাষ্য অনুযায়ী এই কোমর শরীরের উপরের ভাগের \r\nসমস্ত ভার বহন করার জন্য মোটেও হেলদি না।<br>\r\nকিন্তু প্লেবয় ম্যাগাজিন এই কনসেপ্ট সারা পৃথিবীতে এমন ভাবে ছড়িয়েছে যে, \r\nনারী শরীর এর জন্য এটাই স্ট্যান্ডার্ড যৌনাবেদনাময়ী শরীর কিংবা পারফেক্ট \r\nশরীর। আর এই শরীর পাবার জন্য সারা পৃথিবীটিতে বিলিয়ন বিলিয়ন ডলারের \r\nপ্রডাক্ট বিক্রি হচ্ছে। প্লেবয়ের বদৌলতে পাড়ার সদ্য গোঁফ গজানো ছেলেটা জানে\r\n নারী শরীর এর পারফেক্ট সাইজ হল ৩৬-২৪-৩৬! আর সদ্য বেণি দুলিয়ে হাই স্কুল \r\nপেরোন মেয়েটা জানে ফোমের অন্তবাস না পরলে তার শরীর কুৎসিত লাগবে!</p>', 1, '2019-07-06'),
(3, 'থটস আর অনলি থটস নট ফ্যাক্টস22', 2, '7c9b7936cb9a95f341c3bae5a815cc43-5d1203e7a0df8.jpg', '<p>আজকের লেখার প্রসঙ্গ হল চিন্তা এবং নিজেকে দেখার দৃষ্টিভঙ্গি। আপনি \r\nআয়নায় দাঁড়িয়ে যদি দেখেন একটা মোটা কিংবা কালো মুখে কালচে দাগ, কিংবা চোখের\r\n নিচে কালি পড়া একটা কুৎসিত মানুষ তবে এই লেখা আপনার জন্য। সাইকোলজিতে একটা\r\n কথা আছে ‘<em>থটস আর অনলি থটস নট ফ্যাক্টস”</em>। এর মানে হল , চিন্তা \r\nবিষয়টা মস্তিষ্কের নিউরোনগুলোর তড়িৎরাসায়নিক সংকেতগুলো ছাড়া আসলে আর কিছু\r\n না। চিন্তা মানেই বিষয়টা সত্যি এমন না, আর এই চিন্তা নিয়ন্ত্রিত হয় বলে \r\nআমরা যা দেখি তা আসলে সত্যি না।</p>\r\n<p>আজকের বিষয় মিডিয়া ম্যানুপুলেশন না।কিন্তু এর সাথে মিডিয়া ম্যানুপুলেশন \r\nজড়িত বলে প্রথমে মিডিয়া ম্যানুপুলেশন নিয়ে আলোচনা করা জরুরী । মিডিয়া \r\nম্যানুপুলেশন সাথে হয়ত অনেকেই পরিচিত, সহজ করে বললে, এর মানে হল আমাদের \r\nচিন্তা এবং মন এই মিডিয়া দ্বারা নিয়ন্ত্রিত হয় কিংবা নিয়ন্ত্রণ করা হয়। \r\nউদাহরণ দেই, এই যে মেয়েদের শরীরের এর শেপ ৩৬-২৪-৩৬ হলে সেটা পারফেক্ট বডি, \r\nএই কনসেপ্ট আপনি অবশ্যই জানেন, কিন্তু আপনি কিভাবে জানেন? আপনার মাথায় কি \r\nকরে এল,কে এই মিজারমেন্ট করল আর কেন করল সেটা ভেবেছেন?<br>\r\nউত্তর দিচ্ছি, তার আগেএকটু ইতিহাস এর দিকে তাকাই, এই নারী শরীর নিয়ন্ত্রণের\r\n কনসেপ্ট নতুন না। চীনে মেয়েদের পা এর আঁকার ছোট করা, মিশরে সমস্ত চুল ফেলে\r\n দিয়ে পরচুল ব্যবহার, গলার উচ্চতা বাড়ানোর জন্য করেন উপজাতিদের রিং এর \r\nব্যবহার, সবই নারী শরীর নিয়ন্ত্রেনের উদাহরণ। কেউ ধর্ম, কেউ রীতি কিংবা কেউ\r\n সৌন্দর্যের নামে নারী শরীরের এই পরিবর্তন কে জায়েজ করেছে।<br>\r\nব্রিটিশ ও ইউরোপিয়ান সম্ভ্রান্ত নারীরা লম্বা গাউনের নিচে এক ধরনের শক্ত \r\nঅন্তর্বাস এবং বডি শেইপার ব্যবহার করতেন যাতে তাদের স্তন্য, কোমর এবং \r\nনিতম্বের আঁকার পরিবর্তিত হত। যেহেতু ক্ষমতাধর এলিট সোসাইটি ঘাস খেলে সেটাই\r\n হয় স্ট্যান্ডার্ড তাই পরবর্তীতে সাধারণ নারীরা ঐ সব নারীদের কে অনুকরণ \r\nকরতে বডি শেইপিং শুরু করে।</p>\r\n<p>আর বর্তমানে বডি অপ্সেশনের কনসেপ্ট লুফে নিয়েছে ক্যাপিটালিজম। ৩৬-২৪-৩৬ \r\nকনসেপ্ট হল, এই বডি অপ্সেশন কে কাজে লাগিয়ে ক্যাপিটালিজম প্রসারের ঠিকাদার \r\nমিডিয়ার উগড়ে দেয়া জঙ্গাল। জঙ্গাল বলার কারন, ২৪ কোমর মানে অস্বাভাবিক রকম \r\nসরু কোমর, মেডিক্যাল সাইন্স এর ভাষ্য অনুযায়ী এই কোমর শরীরের উপরের ভাগের \r\nসমস্ত ভার বহন করার জন্য মোটেও হেলদি না।<br>\r\nকিন্তু প্লেবয় ম্যাগাজিন এই কনসেপ্ট সারা পৃথিবীতে এমন ভাবে ছড়িয়েছে যে, \r\nনারী শরীর এর জন্য এটাই স্ট্যান্ডার্ড যৌনাবেদনাময়ী শরীর কিংবা পারফেক্ট \r\nশরীর। আর এই শরীর পাবার জন্য সারা পৃথিবীটিতে বিলিয়ন বিলিয়ন ডলারের \r\nপ্রডাক্ট বিক্রি হচ্ছে। প্লেবয়ের বদৌলতে পাড়ার সদ্য গোঁফ গজানো ছেলেটা জানে\r\n নারী শরীর এর পারফেক্ট সাইজ হল ৩৬-২৪-৩৬! আর সদ্য বেণি দুলিয়ে হাই স্কুল \r\nপেরোন মেয়েটা জানে ফোমের অন্তবাস না পরলে তার শরীর কুৎসিত লাগবে!</p>', 14, '2019-07-06');
INSERT INTO `blog` (`id`, `blog_tilte`, `blog_category`, `fetured_image`, `details`, `created_by`, `created_date`) VALUES
(5, 'test2', 1, '47095637_2357543337607856_3259456900047044608_n.jpg', '                                <p>&nbsp;আজকের লেখার প্রসঙ্গ হল চিন্তা এবং নিজেকে দেখার দৃষ্টিভঙ্গি। আপনি \r\nআয়নায় দাঁড়িয়ে যদি দেখেন একটা মোটা কিংবা কালো মুখে কালচে দাগ, কিংবা চোখের\r\n নিচে কালি পড়া একটা কুৎসিত মানুষ তবে এই লেখা আপনার জন্য। সাইকোলজিতে একটা\r\n কথা আছে ‘<em>থটস আর অনলি থটস নট ফ্যাক্টস”</em>। এর মানে হল , চিন্তা \r\nবিষয়টা মস্তিষ্কের নিউরোনগুলোর তড়িৎরাসায়নিক সংকেতগুলো ছাড়া আসলে আর কিছু\r\n না। চিন্তা মানেই বিষয়টা সত্যি এমন না, আর এই চিন্তা নিয়ন্ত্রিত হয় বলে \r\nআমরা যা দেখি তা আসলে সত্যি না।<img style="width: 50%; float: right;" src="data:image/jpeg;base64,/9j/4gIcSUNDX1BST0ZJTEUAAQEAAAIMbGNtcwIQAABtbnRyUkdCIFhZWiAH3AABABkAAwApADlhY3NwQVBQTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApkZXNjAAAA/AAAAF5jcHJ0AAABXAAAAAt3dHB0AAABaAAAABRia3B0AAABfAAAABRyWFlaAAABkAAAABRnWFlaAAABpAAAABRiWFlaAAABuAAAABRyVFJDAAABzAAAAEBnVFJDAAABzAAAAEBiVFJDAAABzAAAAEBkZXNjAAAAAAAAAANjMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB0ZXh0AAAAAEZCAABYWVogAAAAAAAA9tYAAQAAAADTLVhZWiAAAAAAAAADFgAAAzMAAAKkWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPY3VydgAAAAAAAAAaAAAAywHJA2MFkghrC/YQPxVRGzQh8SmQMhg7kkYFUXdd7WtwegWJsZp8rGm/fdPD6TD////gABBKRklGAAEBAAABAAEAAP/tADZQaG90b3Nob3AgMy4wADhCSU0EBAAAAAAAGRwCZwAURkFPU0Y2cFdhMFZXYUNYalFtNjcA/9sAQwAIBgYHBgUIBwcHCQkICgwUDQwLCwwZEhMPFB0aHx4dGhwcICQuJyAiLCMcHCg3KSwwMTQ0NB8nOT04MjwuMzQy/9sAQwEJCQkMCwwYDQ0YMiEcITIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy/8IAEQgBZwKAAwEiAAIRAQMRAf/EABsAAAIDAQEBAAAAAAAAAAAAAAQFAgMGAQAH/8QAGgEAAwEBAQEAAAAAAAAAAAAAAQIDAAQFBv/aAAwDAQACEAMQAAABJKos7OMxfbUA1YpHcb5RO/Rx7yvc6ta43QVnkJU8wMIGIaRJq8+8c8g0CWPQRQTTz2lyHDmlAFBUpgptYM3OT2NonU2p78/D/n7Sg2vErTaUL+gCeL9sPZOvb3I2bchZTtEIoE4muAxxExCGUNmC32qJ55Gnynm1067RphW5ZHrd5jR9PBGJA1eO70asSSBPK5As6WW0W+lp877hT0JlbOqbh+H3cuWKV18hI91K6GjzmiSmaTtguX1RSqoshoBNCu+p4JxUNPFYNIfi1TjqMs2U1mRbm51m/wC5/VRrRRdRz9FvYVnMNnkdN0c1uU1Ka/PnxNIcww12mQsDHOOqI+h+xJQOphniQ2jszIGGvGxRe2i7woEZCzy1U1E6y2WBok1NvLSVYciyobtgqdWuqElKqnQ5t16HjPBC61IcZxry+jHxWyPI7W1dqKz7X0pM4K+HoNY4a7l9VxcvM6+Ivtd03VvVhZCtBos7x+wZL0jqShTWiUmML4uppGaI84g/SEv0UtfeAFU7PS4JbJffz2Bhb7lvXQ9WxsUVfyiN1VwfRy3nClsvgD47Z8S5yNjKtclJVN17bZcG5b4o9RWkjZ8KqSq53uaTv4RolUMJXB+BOKVVqdFLN6FSvyr9Uj9D1SKsLy/d7PEsZI3UuoOkkRo+7HrSlH0Nu1c5SMvRllMnRT5H1QILGuXSQxAYen5M7K7EpVVeIUinNH5vSjLladHnStkiVO6AeZl68iGe+VTBSrV20+j50iJH+lyOw2IHkd9XqGmBE6ZkEdhJlkvYBbMpc9tLnpEY94ssVn66DZKZa0z2DNYhtl0WUXAslbAMvs5ThbdChTnteo1E80zrNp2FuwAjcA5SuJUbFHKHlee+ModXixYgQFDRywgO+r48JUdX5zOVu5dqYt0JzdnBrxuD1R+e6r3njket45caLp0pDOHdEUWKePYdD3pdVTKtrG1SZyu5sINbNsT3oJmnJpJ9ryr3avTA+UMlXn9ZpIbXLKi2xlhZ2Z1VJUNrKYcGvuDuOhn9QrRwXq4iVac9TQKlOKWeWo6MLQsKRLJVaKZsloHXoV7KiOAH6uTYNMO5K6UeFG2bVOkmaWgXNOzyZ+96vBxebSxKsVPUqqhwekYaLNn+f7ehGT94vRFPRaZXmESChgPIKg0k6TfS8mssLuJIhNGy0AgXn7+liwDmtbAOXppAhKLXdrKZKl2gzXfwVnCX+jxstEJPg6qQTknNZoxUcG0MwyaTu9Qo2crUDAFjeCvGfeU6THlZNQYBXpeBs7cSmSrwbPDKWIfGzEXuj8FDb28rGFkrmUbHbsHb5+7TmujtaQoOCnXZsxcrXHd4wkqb68NU4eKgl9CN26djFHSFxo5vRYmJyfN9brpO7VlwRYAYcOdPRz6RmIX28Eu0y2uEtjsrBODpvMbCstK5hV5vqKpu4OiMgRVWGozd1jLFjy2k2JyNpwdtiFukB5P0AxMkcHTUscW0ytB3BJCqDaIwptfUrbylPiUsvISiWvVQBzGgHHAbiLYKziCaRLheHomVCQTc2rc0qjO8BilrJmCdBlznoM3DY70+3wl3jA688qSaitywgsVVFBTYsAoHKFa19yXWYVJb430FKy5dRanndPWKJrmW/XyHSEtK9qnPBYA3Gc32+sK1kQsGiP2JCIGyHL1i6bMvRru1mVlSaEXw9/FxaUaHJ+ZXRuae7dp0Mymeex9sRZTFaXAWcVk55iqdDRhPK1YJQZaNwjw4W5VS6bOGPfMouvUOWS9Q2uaeUjpFULDV0vKy70ZY6AQjbRXZOZO6fPdhU2PyUe9y3HVGdTMUtsJW4/Q4nH8k05O+lW7C8/1s1Nyb18TNeZluXsf5X6TnLRzxY9PTzsyUPlJ8YM6IQTO4CPLuowADNHRU3J18vSOevOGe221dEBbKoef6N3ao7PrEnijVfQ6KhuIgKzaKFir31D1q19Mpqym1iGr13AjBtDFOwZCSwJZbo8sqoyLTWMuQfU1HaW0QBNoQeRwRLHaKs9DSK4YY18kLqrAaPrRFncC0mZqGBhpu5lkYsQ7fUlw+bSHdbGXOftj70MOJjw3nkPaZY678QnnN04ujQreiKe8Sk7QWI21JuIVXKOVThtzNvM0G9SQLz3pOAakPaJi3jZTZXw99lF1YZW0jflYM7aEIQV1kbsPDrGV6qBitL4h8DXj+IJD6bMgSdFbzY+AIZOVvDBlFpa5Gft8Ia2eJOiqGkgSaICjIr6OdoxoXhlhFJHRG+/kaTuF5cApPMShjYiWDQKB2IzSQd6NyJHMRh7QKS9SRykhV53GXSloHPF3SWORMcF7tnVzEFhsHQi2i5TAUkMr3PPUUrdEvGhaGjz2nrO+jhFEksapePstia0jUR/y/AIIrPRqWonXqwqJrfcnxhlCkyOCIGD2l55laamtHkuWMsLJyDXM8/Lbck451Cw1LQFajWVydbiFTNprwDuMC1pqlsWwpK6+KMLhyowDWKtBhniBvRYlbGOrZI1VV9GFYVoFE8LRVRLbqGhXgdQAdvoclpOTpdw94FEg2uaogZwd3VznTrmBUHyht5I0Vct+wlydO6bOaOswj7miOPYXDm6eWA3ychbEZWHh2lOiFM6KCZM2T89941zwn0azAwYgBhnEs4UWVkCQ0zIkQ6hQHEsVJNwDSNuX92lRPzoxdoDJdLTPNs80uQvrfOL6Cezz6hCBMtfO1h6abIycrbrWzr6UeZZU1cItj3xEIiVuhSi0QhZyXo3tf5d1KunNSthrRipY43j9B1c5coXvNbROtiENDnJ0R6Tq42TannJUv4NTNyRxx5uV1f0Pyn0dSNXQKpfwFnbkYHKTVBhK87LR2yJxo91jj5nPRImEmwLeN6q34celVLgrawS7lIxsnzGkojrJndDVpDPAyusedrcVmtB6GgFoUU9ooneVVYWXDalWdVz6UjyccRZWwKVzrmd5ZeMQkp9QHvXS9J7nKhsHauM03Gc2i3g1ZzUKnVbYP3p5liwsFK8fsS+DslASvnuSPX1Xs9RAG2qvmfnY0NrB6l9+e6js7cvG1JyMdwM1TwisgDt1HGBvY+YKM1rcodZZAmfSyoou5uropECB7i7XSi8roWpcZn78/0UMZvWWFoaCjEHgFgkDzppMMS8as4Ql5tbuV7BN2ud20O2RBoHNtICqYrcEwsgazXVc9z3YWnFamStokA7c5Z9hpC07QG+i3m2bCfZvois1fvcXZYJ6nl6bOU+DyqlxW5yUcYwlVRa6qWXRzIY2315qDZko9LWhgjD1tp5VN7GGHraONiPevIAxe7wpJBYpcOy2XvQtXfWdlkRyLSuqgMwlkNNlurm1+oxGrsh8QpjWqyVG1NV4RSsYsV172lwy6oui2bc9PgMfd7t2lctIbDwmyIkZQJ1fJQjVyxVsuvmy3bqOPssZK7yuqbZtrg9sFIDRVuQFZOPSHzdZ1cLZ19OMkb3Pcx7X6pxwPy7q442VTrzzZ0sZ0LuXdmzata2ITVulDCvlUgTmAB+DSyi9lrxu1x+b1tV3P22Wwsk19lEsl0I0kRIDPYSzupq6YZfR2Z+8doFVa2JzOsSbKDeMiEPjXwSlsvIwlZJJg/wCpVeLjLguKyBt5a2er2GdQLIW1RvTH3QXDXMTvItRpUPF6NE6+25WrrLt8NIxQNMGFfpBkGf3GRh0etpu5uufIRBnCMKL0Hi7p5O952sPMRnCPynlaPOMY4TKBjttCMbocsEm0A2y99NeOlZJmGBWT1maLA2enz9lvpXIZWztCDwM8wHsYceY5ozOs68Fv8j0xjoMxe2enBsF1XCKdgCu0ncA0SYJDwdDqtq1HmVcWCxZc+1VOmQ/IvkEqRFvHjaq+kw6ydht4lotZnuL0UHL6Kc0jQbCNEzzrcpoSFzFaez+iXK2TsvF5e63kaDrF3Bb8nud5WUp1y2eG2kQsopa0jK5MpEKqnfjkdraO173IXhdFk9EcwyTijqs3WXTOz5FB8eyJVtqLCyy5l4RKVZQ5ZxgM2WOGQNBo1NUzZwZNCcb2aAjlXsbO0M5GVBIBXOLdlnLSk9xLw6NWizzyFcLPMAhuylYMe8aT+YrXLiwsNl0wYrGw3L0ZVe9USrRLnHQ1vn2eGhf59wpJq5M5Mg1+bjZaLGLiNfeNP0uzYQticpf3iGReMZQYS56va6IwQJioOLDvCCMQWy8fDeXYPUbMKWK/ZKcCw5+o6UOvOyY1jAntUysp0yOtPDMpOoFiIy5QE8d6s2SJoZm1SDXdcgsJm5YxGluRzryyZELQ5u0ndwFzpxddVj6BAsnEHupSljIBtWdbVafaLkUpdJw02nQR6FHL6BvFCTI0jHMudnxK07a9Q4FU4IPQoA3pRsdeT8Rt4uohNS7yTdKPE7+rBD66jGiksrYFkRdhD13dugnEEZeekvxGHPUKy9oKwh09LEsylepvonp+647dWUVkVQRSXBTA2CLNaAGfcwPyzB4NTQWjxpIAIBKVFXBcz1okTTS7DNdfNIA9MpZ+Amj2iEgK0KpXBmJFwnXymTJKVj15dKNVnNPlkcYB2qlQf3vNiWyM/DRHI2BDuYRwKvEfR8ijIpdk4LocKRhDnAilNb67bRN8fp1awNnTlWzLH2r53gbsozw8TVPYqQtp3s4zSJZkavN57kyjNk52UmEraput5ghlJ2zhOkvDE07ZiFhCd6eNrJlzrlFy3JpbsO3UbO9cQNYg0lChOuixpJCGwrIGhwpSGKSGtIGBNHVyLUdfnjV7ytyGjV435t5nEZoAVKFkUSKDu3USOaN841yPWqNnieraUI/zzrDjZW6QaoA+PhkZZJkJileradtrasZ4Vjmw2XWX+2r723aBHbCI1XLgc+MJYtnBS8rn6TiF5YUmyi2k5yrsYEmD2WhdbVdROVW0DZY9Mzl3rdXlR3TXoSKq8qh2CqcV7DLNhtHYKMm4vcZczrn28bM13UBpVTjtXpE2grIYiyx5k9SSVnkvebKUPvBpMfe56hLveB573m0z/ewcMfeway95WRJPeSinXe9SU6felRfP3s0k/vFG773gT++86xj723p+9tP3vbCJfeU5wr3hQ273ufquv95Dbd7zpfd7zoxr97q5bY+9tYP722PO96HodznvWne396/Ivn7xFDv3lZRX7wTSZT3gGLP3pHI1e849H3sGD73ryoo94ofnfeU//8QAKxAAAgICAQMDBAIDAQEAAAAAAQIAAwQREhATIRQiMQUgIzIzQSQ0QhVD/9oACAEBAAEFAhadq2zHAmLZyX4mdpbL/wCWY4AfH5Vy9ksLQHyPmL8ifUfclKba0aKeG7srsLZBuRY1yyqzeQcvU9TuVcmQDQf9VyFsK2Te5qcZxmpxnGa6DqZYdn5XnpEfbWgEhfOjAkA6b6ACchL2DKdJlfsD0PkVnTWftvYlsbyXA7gUIOh8zXFYQu+M0RN6mKRymWR61GrttdRwbnVhIfxppsg9FYaUq0XYYHxkeaq96PuhEIiDVjiamIoNj/KDzioVpM+oWFcfZQ15wlV4MWwGbHTXUt0EMMJ8J8KNC9xKPNth2y/uDNzmJzm+gHi3SrlGyuUWrY9ROmhg8r8M5gbxuWfB/X++uMnK7oG5OniL5exRtF0R8fUTwsqUBbE2bmeu1ynBVKYu/P8Adac2XUsuKwZfsDLZEt4z1SwMGm4PDP8APxMXwrfNA5WL8P8AGYTYRQXqatlhES+xWT6i6mrOS2d+d+d6c4rCcxDaIbRDYGnOHybLg2Vjn3HZlY88tTnPmKk10a0KLLXiZaOLu2ctG43MIRBDN+P63G+D+n/XXHcVzkGG4aCti/FH8j+WTzKv1+rLyyOy6jHY+oRubtrjk1lLbdg+pTeOOUPtW54WJOPZxGcOGVOTCd2yVUawSvhhK/am5hjbVvLf42QEYK8cU1KZZhoZfV2o6TyIubaIM4QZ1Ri5Fbs2SiRs469Zfuu3uynEbVmMhXvcKapijVO1nkwVEwUiBQOhaFuK9/kK8n8lmNVfKEc3WeFrbmjDRPyfsPx/X9ibg8mzwrEq4y8gQE8g0rUd1F/IPBx2n1Ucsl+SqHdMNeBQVG3IRvUZT/Fi83ppWiqxvF7eVWD5zd2XOvBtSirvX3HnYvunAtCOEOpRybHpv9zvyptbSYo/xemTXyrxVW/EtxisNcIKzY39N/2sgBX3MTCsviBMZbMpJ6lpk3h1opLnn45iLeFnrFE9fXFyAwVuc46me3HG7hE9NkvVd3scY1XbrI2uI/hoYfsPx/XVPEJlwBJSDYKjUqO7ajt+2TKiJ9S/nduT2kG4+JQCkxQSH5hsOpnsssCjIv5QDZmwgZvLeTqfT6gig/irX8JadoLXZWm8ZO3W9Csy+1Mj+PHHHH6EbGC3BgnK2/CKq1XGNVufTNCo4d+TkVfTqKZdbGbyWWpLLXvbHxzYwqRF7BMNDTsT0256MQ4IErstpaq0PPqp0aEBtyL+1K172RB8r7LifafIP2EzfUQgiExvPTlEPivj3EXRX8dSsDM52FmM+7aVpY3K6pa3Cir+HRssbVaZFh2PLfAUTJbTE+NQDYsTjTlH2P7BQvO3kNqK+XXI81j7P4/qPc4pj2c676OLPX5x6d2C7a35GoAbCbUVmBstpxhDaiQ5ALY9D2rVTdjn2iaE1NS6oEV+Jn+7Kx3D5z2m5sb/AFp/1Z+1LckY66mEwtOUDCc+Rr2WaNqa6AQDxSPdX5j/AOsoEznAg9uGgAXi75Ftouy00tFNXaF9420HiIOTV63Y22HuPjjjADJbwW9pcFjVV20NDNFr49BDG82/Z9Sq0wsBqwT7bRtDWXa5wgd2sceS9bsrYPFMYqqPY95p+k3NKacemNeqxbFsCW3UNVYNK4boYV0fqB/K76rZvbWOKxow5V1WQ+W1ppvwT4awbrpvcrj4amzKqqU5QyF+Y3Ujc14q/VP11/jKDrPAL217dwahjsBK12mEjvdkZHnXOOdke5gNDKbt4m5WJw92FWY5m+WQAIvg8jDvY6qPzTcB6ZFXepoqFSY5Ndpfa329mWW8io3KQIqFiuMRBh4qk3qgvzGnfYMtt1hbJ7MrsG6Ley1VoBWxW6NreS622lyJx2d9DAYfxWVvuXjU3FPusfilF1VdfqU7dtdDxnoarHXhTH+d6j2ajfH9K5BX9T/APi9R3kX3NbZWrFRiJWvCra4r01atcGHzEHAIZlP3bAPK+GmOvDEsMIJNW1UHY+DNdNQDRZuoM5eHJY1U+7IvWkWWFiPMxqpj43apcXmEiqWZkNrNFxrbjX9PInYr4WfS6TLcVsdnsHKizULbSm0vXlcjTfVTRh+BMMB7WXiN763ryWs6iflpbwXOjanfuP011FWD+XLcpOO4g0rHUJjGP8cdqCeO9xfj/wCGvGexWmpga20zZH+w2uNNDV491s+YBOMGjbfy41qYE0aEFt1jQyu5eSgPKyJxmupsAjZGzA0LaiE2Nxhrl9var9O9s/8APJlf05UPerqDZce20wtK6bHHqAjY1zQ3KiJalghGxm4XZiErK3LjEb899rWEuWMwf5n+FhHQeYfa1VnBshOQbyGcqlWZY8avI5HH/Eta89aFkMYxou9BBorqB3R+X+P/AFn2FIM6uUX4i2q9KnFRFqycnmSYpnKUHkH5rfkfyJ4i2V9zGTs1fvGXyikTlpFuKsmSrTnDYBLsvUN7safMbeu8UNJN9i6C7h8wahZVlufUkszLLY1mh3YqXXSnE4Hv2aRMd2XGrSNi0tF9OpNyLOa2BhyGRV2L1FjCwpRL7A3TXjDGkI8fBB2DBL12AZTdxN1PGPXuI/px6sWFPeuKTY7eA8cx3gPuVtRG9o8w/PtNXErMxCa/Th4MRImLXLnbRIUBWadtt5lfbxsHJrS127JLiLWzTEpRrjZzsXwHPtvOhyO/mD8c9TK7gZ2q7A2EJUhDcY1AaU1ChB5nLfS3NQFyjztR9iJg5Fxqw6aQzATukTuQ3KJ6wiWZLlmuZypveBrMc12CxMxOUrb8qavsysT8h8H5ip2z/TjykI6KRLENbyu/jGrBj1tDRsMtnLDr7VNjeGMteE+cTBNo17lX2688vJ+Jnlu1SSAvmcV0yk1pjR6eTGtZnD3x7u4iU6Xftxm1F+R8WeEuB72oW7cax2gLQMwlGZxlVy2DgIVgGoF2eW4zLStlzZMXErCpjVmemhNFMfM3GuMa0mcnMPIzjqVW02Wrg0y2jsyvLas2lbFxSarcrZoxbqa5jJ+TNpJUCYqcrTpooaWCHU+YRN6OhehHBiOUQvXOXMLyZ2VXf4DtuWP4/dvp2L3LchuNSv5U+3c/t18Ly1ms3YVrJWxI2Io93Ew7A+GzBvJb2z+wo1rUrGos5ahfa5Fv52uJK2eaDS8OGs9HDhSuuyspZ4+Z8TjuW5KUzt23stJEPaSNmyy254dzeoiloeKSu6lX5YtwbEHK2iY6GuooCLPp6RFeoa/yWcJfbjbNdXarcBzl0rRMW4pEau4++ubSyNSRASJuEwMULqL1B4znqFNxLu4KKTVazeHaWNsqNTGo9Nj5dm1Zmpt744BxOULQfGWnOtKiAqeGUicdQ70dwLqOedphleQ3ZXk8UcUSf1426F7vTtDQ0QGs0ZJ2HDDiIyxa4TOQUHuWxaKq4WCiy2xiMZ3hxWWcWWMrmcW5LRZ6X02VZLMfIoFeQVlOaVi8Yjh1Wzc+ZbWGHD3fUPaUyAJlMxwnsb0WVkdxKRquziGW++mLbVfNusLcpqFYVMDMhZVyIyvUUsNc9OcoVYLoj4ZMP0/c/wDLIbHxKqWtyeUvTItfNxu4umUrY21uIgyNkZAEyck2LVZkRTdDy3D8Ey5yKNajQzG8hF0mzyHibjkEUaDBap2EjYgaWYTymu1CNmeBCZxYznTXDlmWZR49+9iHcTuWMVx3aDHM7M9OrTt+xjYhXM1GwsTJU4r47V3MlmHeFRm9wtm9hvn6n/Eg9u1SvPTivHlZ7VhcajIpi5F1cXNpaKUecZqNWphoIKuzmv6fXy1odeSzMo7iMjwXW1x13MrG8BfL7igzflCkr7Txa9TRnmfMs5TLOq4fMMxP5vEQhnPgwRjxleMHU15VUXPtrlOYlsdtQ5Gh6i3au7Bq3aLQBNCCpIe2kewQuDOXEpfYsGa09VuDKnrAs9RVZOyCXo71Q+lXJYabK7WPtL+aG2LjxmavOvuOEqBazIH4ayO6H3P+fJBEJ8BBYGo4xbr0lGXdZYckLO9U0xknn7D8O05e6xK3j4nS1NrZXwnb5S9zQyZlNkXGrcLjspG4xMR221hSd7mmS3O6P8zD/wBizwlAlo1YPmW/G2Bp+oOkW6i6V41dUyLPKVM5VPHwxBlnLfKEvNwgmanGDxADO0+jVZotYIGIZcjQTKHKnN5P3FMygqqZS/nJJl126i72DGZVvv8AfQp2w8NsxHEIEsoLD31OtoYMFJq9llr85h4/etRdRra1nqFnfcw2Nt7Ds2zxHUtGxmKgwy+rcRdHJJ9TxVolHivuCBmm5uWNobbgTyf4jHphJtskfgwwxW3fIdG2V9LeSmAzPTQlC33eAuytyVt6lZZk84LmjPubnIz+xNThpTlIJ6pjPUkFckl1yknHHePg1ENg2CceJFjiWXNO4ZTvuZegvfPfqZeVVaA5NoKV/qo8jxAI5ANdwMsqDixTXA0rPlVd2qK1L2LXcU01znXOa6Y6AUmdvx/yTPMrfYhWWpo7Fsr0Yq8WG4s/ryI5MuP4Fhh6Yi8aMhdzFLVWZo9886bZlFFzwAVIeRhAMtuVYzEnkfuC7leJa074S7Ks7jjqp4nvLFydCl/Yx2Gr958TkZvcT9r3NwbAsaIr0XK5GGzkxF9tSiGb1H2YdiUZXGOBatlZWL8Y+J21VWWduGupQ1uPWvrcWN9Srn/pkz1hLd47A0LGeyUWRTvpau1tr9Pc9eoreR8CHo37Zh1jDUeGa3KBo3FvUp85vmrcrxbLIlS1SpOK/EvvWuWWOT9mouJcQuC0GLUsI9myBnpxvMXoqgztmEag/ai7i4t3EIaXCbEL6gbmD+LHJyFW/b1XD/C14/6E+YYAOTVgq68ZVawjWK6/T6BZf4E3PENSE+ko3dh1sTjAQ0M0TBshrpxF5c3suNkraUtsCNMugOtYIgMWD2j5LvqK/v8AqB9ojQyv96Ae3azcqoaWuqqxa6YZWstuWuXWuUIJjeYetdBslSJXCev9P4Xud/Di9F+VHusqBbiUZf35alV2jz2zvOUoWU20WjLq7szW/wAk+6L4ZRufAYx/nlN+DNea6mvsRBVX5m/O5vzsmDeuAgllvZliDlZa1jqJSfGOfAPRl2Muniw90BPTeha3is+c7+UrD5jSry5bigw3tlVKUjcJnMse57X/AGdxC0JhPSqnzAfPyDAYPjK8Y29dBEWAagEJ0TWsZBopqBNluSx/EqXkxsDNWrCPlDGXyT54KDzxnlm9P7hG1oqYZ/f0+nt0wCHxPmaA6MdDVrkg0lSEXI2qonmw8ZW2pVZK33B0srDrbSaXXzAdTXKXysTJblkH4EaYv0+wlESvpuFozxGG3f3Fp/Zm4zARZUZuLP6s2JW24PjKQtixRFXcpp3GqYTyJuc4zbjk7CxjApdrfZT53i2ZF03BuL7pWg5tRxndYh38c4bFius8TGo9Rf42Tqbh2IdCctDYE1ou4prfwtjMLSS7NYFm9xfmo+KmimCamUm618HfgfFp3O4Kx5M86x8ey9sfFro6c53IXhYxmgaFupMe3yPlE2F2IDFMBmoq8TA0+pY1SIvxX80We8ruOgjCHc+Zx3NThuVVamTU1s/891XHCqtqcLVAie0LHY6tTRcHjyh8zU1qfT6e3j+J4MKTWp4EKbJQ8tco55N72d22S2p8wfKDcqTUQ6KGA9HGw9fFhH2K2aXPuL5mP9OngAmG3ULb6c5uE9S2ozx7C3QSl9QKGHbImoNweZw6a1PqI3ij4rHjkVYX7XmT0ZYFgUwJBXNalOUq5qrGTsvloUyq/eSRtF41kiN8MdQiaGjKqWyLdQLCNRfIJ1PBhB2plpOs2zjSH44rPNxKWtpxE5X1GJaBEbcqeAwQzIr3F8TIb8ZPhEa+zGxUxgW1Gsmy01NwtNzfQkTlHs1C3LpqVopnYgLIRZsbEWHaxbDsQmZg3i/81Rl2EHjXTW4qwJAk1Ld8KFR5RkdqtSl9WdjWLYmtgjjW3s9pLGWEnoRocfGFjWVKVYFeiiHzOMCzwIW3Mzzde/t6YBIoQhrYGlDSo+VMHRxsWLpso6nB7bKKFx62aHz03NwnoehhM0zk+ZqARddpF8q2ow3OE4GIDtUM4LPia85A3V/yo8jzAOm4ogg6EyyxK60PnFU2xCOHKaQzt0CZGMDNno/mcdMzeKiO9RvtzXTUHxC0azuxX5yx/wA9r83+OmPY3YQcW6VtqVPK7PCnoZdXMlWsyaaEx0aybheb+0zfSx9TEurKjy3CKkFcRSIlO52VnFRPZO4gnend3F8zUYbnHRG4PMHQDymp4hOp3IfdMihBjTBvKMBq+wkTmJardsk2Rz6kA7nypn/OHiM9geb6bghIUN9Ro33zkEKbZZZwDt7dQ9KG41rbpuglT6iNK32B0ddgqKyz7haFug+09HfUJ3K24NKfMCDQCibEe5lDZVm/UPPUNO8YrbiRfiNLxxzAJqCai6nieIdQytfN6dwGjuZPbNF1Vtl9dXEJXqjLuXkndeFyrH/IrBDQ7NuJ9PFcNeyKdTtmDx0yfqFeON3Z19icTzNS0gIme3FYYZ/aFBERHlq8WgiGUNtUbRQwdLV3LjwIfZH3EwmWWATe+ijbLTuKq1junbPBbKgLBZiCPj8Zw1AoiaiECIdiGZv+8IOgMBm+hn91+Ix9uLRvIzKOSVOVFN7NMxDdUHluG/c9Iwgx2QWUm2UpXiVH6jjJFzKHi2BpdndjKH1DFMyvqu4lVljH2Sz/AB0xaN2k+M5gcj+j0HkmJa6TITY6CU2aitsVPAZuNMmnkANEddzc3Gltmp89aK/HLiC3nc3535S7i1NwcGtWFuPHUqQ+ojbNTaA8wzN/3RB0BirNTjCIqzjNTFX22LscdLXvlVkLdXRynEge4Tl5P429PVe13dqiW4N0sx8WtX4ZC5GFdjstPGaFVlCCXhvUYp5l21VY3Kz+m8dKl2wnaLSxZYvE9FMqeI0rbcHR12Lk1YIJub6FtS277KU5vriD9oMS0rKMoNPDS6jkLqihS0LKbO4qN7jPqA/Os1B8KsE4wiJXucIUnxMcf49n6ZFfC5YOdbpl12qpEMOprwg9PA4mdhd0ADOw8a2u6qnJauzIp9Oo/CmJZtL9+oxV0Mq7iAITGO+lI9qr5rHm5PFy7+ytpW+5W0UwQy+vc+D1Z9Sy0sT9mInsKwjcYaPGcZxhWaMRikx8mLYGFtC2C/FNbYzEWctOjbmegKJ8CIIBqCCBYF6sPFa6qYTNX8mhyqcJBTKtheh0JSpcsNhTtc6t8a7LrDKAPqGLig+nycU12Vk926vln0DjTc5ss+IetQ1WsV9SzRFq6Lro9AZU0RpW3hfgyxdy9NGf0zRm+6ocU1GWcZwnDU7c7c7QhrEOliZGpVlAwhLlNJqyeBJVNTKG8ZYgi1mdowVwIPtf9f6MzF9jzHZeQ/GW2IGDKDP2KnxG9t13FlqxXoyUFuLan1FnjMhqbkkv/wB26xkr/smMeutKnmVrHXw9e1sX7EaVGVEll+CYRMhPDDizNqFt/YJqVJzsHzqanGcZqa6PaFlmROc5QWESnMIKW13DtzhLx/jBPKLxizfkHoOgmoyjtQ/N67rYeG2kxr+5WNrNamRamPVg5XepHt6ZZYV1FHVbV7v1IFs6ohZXbvGt/KmS5V2cuQkaNPmIvJ38SthqtvGtm7Sm1dFhrqp0a2lPiK85b6WLsZSaHkzwOuoIfMxB+UH3AxnCwWgznOeobdR747liEJi0Ez00egrOOolzI2Jmc18GZZ40I3lTuLNTZggM3NwR/wCOGONhwxpr42xabaDUzMEtBHZ4ZmJi+mrCqvRvyTHuNV9o5DO9yrqs1bmEzXU5O3Kr43oNG+ZTy56DgLop4iftYWZrV2rr9lbalVkVyYhghEvSXDg/z0101K6uRsHadLvcrRq+QO0PcnOF5osVp3EqnHUB0eCsLMeNjxayi41hK5x/Gi7ib0H4nlvpr7E+fluj/rR5jc6L8bNS2PSlw7TVWAixK1KIDvoeVV/1KmJfZ2FRitoG8TzbTf6ZsptZKeejeSeldRSqtfLiJ8pviWKqx5p8hx1BlbyptysxT0tXYy6diDovydKlVoMyfdXuY1uxWdi2rlHoInAzt7i1RU110IPE+Z2RBWutaOYeVlY9oHjU+IGgb7EHhf26W/oPC56f5GKE5tddiSjLS4/MfJSpuY5i3bsgsUeDjn0ebmntpev5sYgOw3YdkoY0J6Upztf4psdGWxGgrraHxV6fcCcCo8WL9iGUt5VorRT0uTxanFx0x6mtfJ0GTkITtYpKvQ8+Yyw1zhrrucpvos3OXTl3bF+F+PE1NQQHoJ/Q63fxjzXkDniY4093qcN2uosZM2w0PlU4tYajIpXYAltfcXPq7+LTauXg2e9RoM2+4ojApGjdMX+fzDoxa1Kdvidpo2LvXN6/h1jro9BEaI0UxGgjrsZlOgBP7ptTFxaqu63pxxsp1OHnjqY9mmqfYMIhWa6anGamoAZ5g8jNs7ePX8KYsH2iJ8jz9lv8S/PDuU4yeoovxmIfFBGOr0Xk2ZNmDjvbY+ZVW9dy2ruOmjSPSZ7oFX/vI3ZiLcELuti70pOzMddUU5HGNXynL3MSxeu42rUwav8AdDpv2FqQ9VPmtorRDEPTIr2tq8bEXcdtzAX8eoQDL6uMRQUfa249uwh6MJ8zXv1NTjAIFmp8DLt7l1beQ3jl7Vbf3J8fAHWz+PerqPmlDXlI0ehHH1BFxsL6fWd35VNCVUveycVVdxdzMxheln5q799jJ/0WnkTlsdKRxxrKQZj8q1HYvnZ4h6uUfHyDKVaFCtiGONh10egiGVtFMqaCWDYy6tGrdeLMJdYphbUVg0artS1PZQ/B6n2B5mpxhX3EQb6AdDL241xNclM8wQNAfsQQ+Svx0u8U2nVlP7ZnszDc/cwss2D6kBaKuNdfb5Mb2WYfh+6qC7MFR596nGY93mt1lq88c9PmEGU0tdayaFbCJxByKHRqM9ga7i0s0UXl32BLI+oDtLV+wStorSoxTDMpNi324co8Y3zLJviz3e1vKuvE49m1rfrqanGa6u8yX/x1/ZdMeIgOoINzcB6D5/VZy1O5A0yG/wAa8+KCeH1ER/acdh3cxq1prIY3aVP3GIQUsyO7n2pyv+n1cUdjVZ9Kr5jJrVcZhPieBD+2GT3VlmqytnJxpkvp4H//xAArEQACAQQCAQMDBAMBAAAAAAAAAQIDEBEhEjEgBCJBEzJRBSMwYTNCcRT/2gAIAQMBAT8BY2U3oz9sT1VWLfEqQjGjhDGSKfQuhnESTKvtQv4WMXhFZPU/5P8AqJaOWMSOeGypL2smxsjHnJRsz/UpHpZLOWeytUPUxf1FGJj5J9nBYJtw6PrvJGsm71kZ1bFlg0YEYJeMVm3rJ/u6+Caysj6MjeiUjkehoucuf4HEyIp6IcI0d/J6aivvKbk6vJjfGJGLfZg9Q9YHLZS6yKeCM8rIq0ctS6JYzrw4iRghSyT401j58k8RKtZwjyY5NvLKM8xcWVNCGxvNvTUvp01E42iPRqdOCHCVKhooZ47H7nq3wVJfItsRLyj2TWGQWSNNLbKlb4ibexROD7x4dn6nLFOMES6Iz4y5FXKORCDmyn6GcyH6alJNsV0xlCnKeMHqqsk+JJ/tqKFjpGMlaWIlR7KS2IflBEtkFx0VZ/6o4EaP5PbElWeMIavE/UJuU8fi1N/DI+7MCMT0FNKORLdkSeDAngZ6Opx2z6vOfuJvnP22nNRROo3tnbKUcLNsXxbFlPHRFTZxihyHNjkmYfwjHwx6v6iXOpLBH8CeGb7Hv3Hoa0Vps5azecrZGZ0JtlP1UFT2T9auPtWyMnJZZL+yK9xgj5xjk1E556FTZ9MUTMFtH1cdEm5PJNCRUlwg5MjHDJx4yJIgzoenmJ6evVzwIoq1FFFP37GsGbPq2LR6JkPvszdsnYkLC7NsxH5MnLWSVVGE9wZNvGDaIyyTEV0nDDHQxuJUefuQ/wChYFvs4fKPRw9/J9H1YlVSqy4rojiCGsmDAzjZLdpdEexWZsUGzhgwYQjH4OMhxkNM6eST5CjkSwybIsrPZkcYy0yXpIPo/wDI0Spyj8Hp6Oucrynjo55tNYHjwgtiJ9CE7YFHL0Y/AoNihgxbodaKPrn1DMWOjGQlx0SeGMgslb7r92/q8peM1iytTEVehFPaEhkEYxZyS7HVSHWfwSm3bizJHaExs+5lSCxlENDe7LRJfgWxIY2xPIxK81leEOhE1lkKTIx4mGRiYG8Eqp/3wRTgSpZ6GnEUngfZRhFol3xRJaH4O2bZt/XhNYd0tEYNigjiKIoiRKookqjl4O1JCG2iWziSWEQTs56GrJDd8WZFayJY8JLJ8kI5ZCn+RRMGDB0TrfgbyJeKILQhq2So9mcHbGMZkZi2DB27VoqNNS+X4yjvRSp8UJGLtkqnwYErZM3RCztgqd2QtjGLYr5JMprWbVpNtDWHgYrR7EISs2VKn4tFecOxWdpEujIj5szRpGSUsmcCWbyhk9TDfJWYrUn8CMjkVKlorxzen2K2TJyJ9Cv1Zqz/AKs+yG2IZKWCE+cOLH4J4Iy1klLBObd1oz4YvDuzkOQ3aYjOseLFoqa6H2RETeBlOeCV1anPWCUs3XY/4I9+TtOGBO3QhrDwZO0RiuyNpyH2PRB5V355yLyyU17rNGLMduydLjZ2e0T7I4Zx0IZyzu0iD/jfnS8ZXzpMrv4t3eotkEbtN4Q0djI+EhXV15xdmrzul7SssxT8Zker1drHhDxVvgVmfHnTI2Y7rsh0T/xjtH5tK6J9nyRez//EACcRAAICAQQCAgMAAwEAAAAAAAABAhEQAxIhMSBBBCITMlEUMGFC/9oACAECAQE/AeBE+z+s0oNKyLb1OcRImo8obpmnyxlFeaPZLwkzS/QRXorgiuSOG6V59moanVHMImk/rbxFexSlZD79n+OmrJ6NIo/9Gi6K5LNxwVlm4gex5k8aMfqLjjNCRRrz2qhPDJ8o5czVn6J0oULslL0jo0FzZp6Ta5Net21DjZJbWPSdWiN1z4bhsuiWqacZakuejo4ODgaKtkNNNlGoubFhIrGtPdJikMkLk/WTE1OZ8h0dIXCP+GjDpEntjY+fORB8EpUSm2ael7ZFpIlqH5VYsIR8dctiGrVESh0ifyIRJfLtcDeGMRqSSNKKfJL22Ps6RpRuR8eFLcfJf1rC8pSI/wBHzyyEfbNw9X+HLFpf0TxZZoJJY9kvq7x8ufNFjxFFjViNZWbKjwS4jTKIQcmQ0vSP1R8ie6VLCebLLKNt9jpFSYlRtjYk0WvbL9oXOY/VIZ2fsiPH1Z8rS3K0bX08REsoiMn8eT1OOiHxXf26JRUXSITa6Jv6l/6HKjsqux6gpD5KlfJ+MSUeCDGyCt0N2RdoTOuRpM6NSEP2ZI0oWzU+nB2ViOUT7Idk/wBc8YrFl/w4RuENehaR1+xGsT4IdY0e7LIqus9Y+RL67UfjZpOOlHcx3N2RlTLOxcDZZfGF2S6ysOaRvss3MbEOhNCaN1oXA50N8Waa4GaS4KLPyM/IKRqanpZUbNn8xGVoimc5n0Mh+w2PvFjlRf8AS0NlHRuPxyZ+IcDbQtSSH9uRK0IZp9Z6LL95ivBM03awxGo8aXYyfY5HJIQmUbGfj/pGCWLKJLkrko6Iv0PkXWGrIyGkN4SRtRaLrOnKnlI1O8afCJ6qXRKe4tDlhI2+LJMU6E7KWJtrkh/RCflWKEj/AL4ac7WZO2OaQ9QcxyL9YjAqhi8JschKxcFifJNrC7IvDYvFE580N34QdPGrKkT1f4OR3lKyOmdDeF4SfOIs3FmnGxw/o+BCEV4WWXURml9pteMJ8cmtqbn4pCjhvNeE/BGnwPkZQsWPNCRqS9Y0lUReEuih5USEMSfmyWFiJDsWEyscnJtYo0VZdZjqVwaT9eM+BlCiQhiTKzZWEMeKKNhp9kRrLFJPCf8AR9Hol1hEYEo7XYvCSsaFEjHMufFZ9DNrNrFGsREUPEppPaIiMg74K4Jc400eiUbFl4lHmxLL6I5srweL8I4jL0NYnBS59jsi9ysQuHY5eh4043hE1XixZkNV4Pwl0MT8FmMrzRH6yIPglwbs8R4GyJqeXvNjF5ywsrK6NMrLRpvg1HmCtm68I1Oh+LL4H4LxY0NCYsQWYMh3RyUPEOHZJ286Xd4vGphePsllC8pDwvB9ECH7ZkLD7zD9Rkkf/8QAPhAAAQMBBQYDBgQEBQUAAAAAAQACESEDEBIxQSAiMlFhcRMwgUJSYpGhsQQjM8FActHhFHOCkqJDU6PC8P/aAAgBAQAGPwIA12Cw6XOZzfip2TbOeEAXPtvcbI7okFBmHCTy23dCpuNzeQUZrdRrUBUF3e4ouYZb54s+dT2XJElQj5EnS4sxQNSh4RJGRlAqt8IO2B1CDtUY1M30WHlebq1Xe6HAFsSZRe5rgTWhUNeD3oqito/6BBYzkwYr5VF0Uq07IQq7Tz0ulDFmboGblLDhKi0ofe0VHg9j5rnnN32VaprfVDyclTMrIlvRNOs5KOWxNwvZ22e1bzP0uE3SLrXLeAass1ATbNjjFm0Np9U4lsdWp5G8HuApy1RF0HhUC6NUeSLHZhRMLim4XvPO4C7OOqz4ck17cjfR7gocJ7LddXkc/JhVQHNP5A4R6Iu5C6u3SqNpnc19mIghd/JbsuLsualpBHS43eiN7BiilFu7wHIysT8mDGUSc81CsrNk7gX9FDj6p2q5bDsq711DC4kwuP5kYihOwXclCNwB9533WSoFK1Uj6KuF465r9J3oZVcY7hBrHy45CE4F1QJhbjf9y4pbyCizBceWqm1OH4Qps6O+6fb+6KXEnXbhOccgFIVTCJYcD+iwuiG6rHy8kdkdiixNJB6L9Z189E4xewVnDK7qtfEd7VaBZYSeSa1hkTFE93xfS6AJJogweuy3KjVhNzLP3ivDFGDNWjjwzS6Lnizzoe6qpus+om80TQ7MBSMuew5/uNWDAJb7et2I7lnzOvZYWWeBnNUKLvkm2NnwtqTzK5DUqBkMrs1xBcbPmufa8j3iAqKWs3TpKDXne6KvEalELCfIHkQqL0T+SNcrmx7iDYrkvDbkzcXZWtrq0Q3uUfE+eqgVC8Y0AyugXUWIonxGuJ6KtzvxDh0b+6fanXJM+dzZz1VAu9VIMFYZRVkPgGxaM916LZ3CJU8QXMKmat5z4U5zQMB9s5LE/wDNcOeS4oW6T6lY3/JVy0ahRYeSoFwrJZLJS2h6KHmRzUHNWTe6s8QnEYhDKnsp1seEcM/wdc7vRP6p7lIVm4H2F4lpZtdgGOclILhHvVVBI5hWLNT+Yf2uDG5lYW6KhXW6Sg2XUzgLOe91BU5Jlgz+VFjchQKFUiG1VYUgCdggbLx7yB5Kqj5XG1MtDc416KtOQuqsLSHFWhPIKTkjhcAAnYocFuWRa065JuNxw5RMhb0DvtMnh8NNflZ2YJRtDrl2Tb2u9Fh8itw5bMIpxRpfZg54V/mu+gVE0MOdKJ2JoLQac0ADSq8R3EVRTfJybUpxIIJzrfZzpUd1PIQE2ecr6oNLamsqctlg67LLYaGqd2Rqp5KEG2RgtyUuQmaotBw80X8QAyGae+035gQhZ2dm5xHFh1QNu8MHIZqLNonmalZrCdUBimxyrqoZQHqsxOxZtg8H7rA0RizuaOl5QcFPPZjVCLN4B1IVXl0e8VMj0W7wjZEXORvswcWVIQsmPafDGGMqqXAgBWlvlgb9Tc6f0m591AWIig2ME1edjF7xz6KVJ0apKloptdtlzdUS91eSyNdTmfREL4jnsQ0Lef8AJSbMO7qGiOyIBWJpqqNqiGvxWn0CDTvN6qI7qYHos7qouGiI0QbFXIbDm9ZF2Ia3QnIBjQKKS5YntrzyWFtnHU1TRtwjfZvPDZtLliUNOfyTcTK2pxbvIJuF4JjWiDW8bqlYczqVAENGQvkoRhOEXRcwakKFalvRqEmuu2TtUH1hYzE9F8aJOtwdhnom+LmBkFFlhYxS90/EVDbuEreKwxRbss7IYm05jVbq5lAyW/EocW4xnhUNOEEw53IJzbMlzrQ8Rud0GyDdh1ulMaDAtNeS3PxM9whitHOIr0RbilATskRXnsG9oGRNbsAzyWGN1owhH6IY3w77KBlfKAfwjiVSwhmWhuqhZ/NUuw6kkqduBsUyXRZqLMbxWJ04is4Ul5UMujGY7redKxYHYeagMjqVLjUqXIRqFSqqPmsdn+ny91Z1Cmsxnr6LC/dfB3SZVpbeJuaNU3P7bMKLvEbld1BlcB7qRaMb/qWKhCbGzCi+IoUVRWUdVv2S8QlzS2sGslSfxGL0QtiKmo6KG5Kt1SvF3cAocSn4InUhRiD49pSt56xu4n5dr/E7j6r67NLpN8aZlQ0U2N4gLdqul+61Ynhrz1W8AVv2YnmVu0W+C7uVAa0XFVqF4ZIw+yeiNkN2D3lWWYad0RyRYzdjMXz721jug5FYm1aVIVW7qhjarDafiSI0TjyMbM6rpsEKFZ91ld7KAUDNTkFDR80NXE5rAC4Y+mStIdhwjM1oqVKlywkNIzKm4pjG8oN+azVTdRRf1Od3RdFga6Oql1rjKlboW9uN6qu8eZVFV0dlvKhXEpDlOqpJQL5qpCZaNjxGGRKL+DQ9Qi7CHMikouY4TqF2uA2oXS7C7hWJuV0t3X/dZ1QB2Ra2u7ZfV11bovbHvLO7JQ1S4jssR+QUwAmjRdUGuGWXRYrrTteVXag7OJ3ooCxPPosLaNW8PVbrVouZVBCzuzWagn0QZvCdV7RHdCDgOjjVrv6LBatwcuRRY4J1iUXD1BTg4P3udU7T03Qji9nIjRZLeHDVUz241Cg3Tot3PkoWARnUctkueJaz6px6LPYojKZGeJaFZX8S4lUqhpCIBnrcO1zuuxPS7eUQqXUXTYkrCziWJ63fqpe7EVDGwuJVvrmqsxIYmgJ9pS0DhyqgW2eN2rhmmh1XarmOS3QcP/bmiw/qWekiCO6Y6M+eiDX8Ds0ZsziBocw0FAuo+dP3RyyyVCRJ4Cnfly2arcfB5FVqqXVF8hSOP7qq6LHZlQ44X804kqt0KIqsOubkQocLgFmouZHvLK7K+ikpzp1vkDopKrmb97JODaga7EO2KqAuZXuhZSboGSzXHfUKbMDGRK/Sd60UvsnNHPRVW7UckbaxdHNqPS+brJ/VDmQnuOooFZuacL9W80AK0rqmAa1UkSqnGzk7Nbjq8rs9iVQ4X/dQ4XY7EV6oBz29YX63/Bfr/wDjU+K09whaW7pcMmjIKGtRPg2kfy3YSbs1NzBZaVJWnzVW/W7JZXPPTYw3Ck30UeqiRN1M1urey2OS3nS5brYC3WfNZxdnCmVULJVCAYcMKrXdxVEHe6IkWZs3fCnWRbiJ4HDVSM0cXEXQBrst7rC1pLufRGyBLi9oaKZZqump0TWnImDCoDyqVuho7IypyWYeOq3gWrccD22KOQs3sLlifX4VQUvyuxWZh3RbziVuveP9VxcBUXRdBDoVQ5Z/Rbr9jhQbqTsR0QC6bHdbtpvKhlb4WcLot1UCyqquKmgULO/OFR5VHFCVldWVhci5ls5srw3OE6OGiLjhcOi3rNw+KL63N5NqUy0mScm9EGSaESUWe1p3QeKRVGVTYopCpaH1WDCx30W/ZFvaqo9vZYtPuqUWexCqVE4fRUe24i6E3AKr85mHqFis3ghC+q3VyKM6bHopQRvBUiih9QhLanosUVXNST6LK6Q5SVQriVTtTBXCVhN8mZQaVmg9twAyuLTm9s5KGgANVm0GXGpT4zbvtcnHUoRsUXIqqAKNaRcHES1vPUrmVVwVGuKpZfNZKhQClVKodiCEeQWS3S4FCXTfQ3UpKJ2HO9Fn2XZZXxFVVqw6qgrzUC+i5FZ7cmg5lbgxLPD0AWKZ6LOeQCw0K/Tat1zgqFpU4ohVc1ROVVJqUA2qy0UmcR1WHReLZgARhlFsneM9kTsSoldb3fyoNbxOyXgfh2eI5oq7RTaWpjkFoqFZhSj/AEQX7o/Q7MonqsJ9ENmE48hcbx1qoua6MxfTPRfmPIHKVDAt7JVooap26VWUd1gIoFnQZbAI0W62OszKbw1+ikyO6Km6ilDunAwW9FisnDsUQ9pa5ui425UCg/JpomhSuSF8HJSFEJ3aF8R4nfsEGtAa3kqvJWaq9iO99FLWfNRgVLsdruj7oxDWDZ+FyBAVVQ7J6m7O/sFM05KU13IxdXcC3RXnzXU3cyqquxCyA7lb7wOyynuoFOylYxk+u2Fh6oNnNRnrcEVCgcdod1vRYnyArBx4jI9E0h2Pn0QPO402s1yKNscmaxrfqgSsRat1vZUBW6xb1AqnE7RvNeJaO6CP2UezoNkhFh027MeuwFiOqqgizhB1VBJ943Sv6KdCqlVM7HIc1uD12T2T2HjYcTdnO+VIKMqppfJgNATPEG83IhWbGEEOMSsLOCzAYFkgp53hV2G2bMyhZs4W5dVncUZKhUXVRMI0l2jV41raYpy+L+yn6DTbxjNSq3Em9reTdkBrS48kDaHAOWq3R6m+AiGivJBw0K3cuWzJ+W3an4fIna6Lw28P3W6nNBHin/iqmq6dVlVBomULqjZ8Q0fafQKtxVCq6LNUiUTMoU33ZNP7rxHmXH5u/sqnfePldGu1C6HacdMtgPtfy28tSt0bOIqW0Medage7sZXVvGzhbQHiUrA3dgb1oBW+gNEAMhqpB3lX1Q5KQ1VF7Wezm7tdmbqXUFOS1rkLsbuwCc+0lxcchr0WJ1X/AGUkyfKrcTdiv3BTVxyCkbz/AHj5lLp8nxGNwunS+L6bbLOzCxYgW6mMkMIgJ7DoYu3hvLFWEZFFkF12cZ4rSvpsUyUGq3XLkjWixmjQNdAiYim78KxHPzw1REnosX4j/Z/VACgGn8Lne7oReD5Tg/hO7PK4EcB+itOUzKFMWhoqc9FQr2SeqqOyNIGw2yGuZ6KOWzKkSoI9VhHE77KB0/somt5tGjI1Q6eRNzRzROgWFolxVN6097+GyvhWg6eY4jkt77prH4ngZOWRwnmgS3EIDcUfVYRw6nNTQQFBWUKuhUZ36o2j/wBR/PS7rtSnPemMNY3io0v6Fyc7nsTtNHRCyZxOqsLfV3PzIbsYcNZmfIyvePhKHlkmFyXhOzzQjIUuqxvyX6Vn/tWKxNfdX3Um4LNWdJ3gpdn18ilWaI2g4Bw9eqtncqQdjBNFQenlNs2VMI1knidz83DEO1vqqeZhOYPluJEuuDsyKJ5HBaQfVAsOWfRTNDqsTHSc1OI4x9VT9YZj3v7rryuzuZa2gw2QrXVTps7xAUYkWNFPv0RsmndFHu/9QqDdYEBrrsd1kPKLoqfOnYqqKl2d1dq0HxeTncBoE1nvLCRK8NtpFqMj7391gFO+qNnNH1aE5gOE5grfz1QezNG0bx5uaNetxaGuLtOZQfbQ5+g5KZVDdndGb+SGM55BeGyCfaKFhZH8w0J93p3TbJun0QAyOmzC0n+Pp5L/AE+3mG0OTQnU0JXRSIcfbb+4TY4muzQ8SDyKdh4Vwk9FibRy8WxbVxhzeSNR8VoVDrSPRbjw5UKcy1sjA4XA5qcZb3Cw2PzWKCao2dnBfq7l2TQ3jOXQc0W6hRp7ygCgG1mVPl18iNiT5NFXaf6fbYp5Dz1RHRAqQ6LQZQiYi2bxNTi9mFjsgVAdTTpdULHBhMtSZLRug8KxFhtu0fZQ6ybi6sqoD3WBOW8hYfiYBP6dq3IreEtOThkgTqiyxraHiPLomtDZESJ+/ZPxVINSsIy+6IMA6Jx57VER5ZO3TYjyM1B2KjYaebfIrsBO7It0uFtZcYyQdMTQzoeWzPsFQJ9V4lnS1GqIcPz7LIL/AAttQey73SnfhvxVdK6o2tnUZN+BGkxpOf8AZGavJqeatGnLGUXGsI73ptR50eVi5qfJrfkUG3sdyMeY0dLmlVRcGydQja2Qo7iYoc7EeexjfloLx+Ksc8n9U38ZYjced4cihH61nl1WG0ZAjIoWgM2XsiMk3E70T2xMmaKhC+EKNgLmqil3Ty58preQ8qmx0v7FUXXyTfPW4TmsqFYm5ahSDIurlf0f91vCW5Hsnfh3S6xtaEonFAaYlVMsbxdU4OqIqiHxu0hMImoCwZE7QGwduPNaOvlHYqUJzveFl5Tr3XSFhco0U5M1RtHZaAao+MIIrKkGW3HCKjeCDm5GoT7LkKJwrpHyUHeePZPCP6q2YeItmVZWrfaEOKszlDNoDYKw9PMJVduenm5X7ylOUHyovK8RomKkc1qW5du6lu83mFESFu58l41qZs3NwQcmEog1JzVGxc5moQsH0a0kjqE02ZDXCrSm2jKF49Vqi2fZdT0VpYzWJaFZQf8Ap/uuqpsSLqzCFe9zvycTeeJZRHmEbVUORCAKA86ZUFNHM+W3veVgOT2wi6zMOQFGP93Q9kKluEzATn2YxMObNR2W8N13NYS7FhOd5PMyOqb+Js/VTZsxD3jk1Pa/PFiEjiBURVMJECddViaKg0TgPZos9nq6qi7NFVKg+Zi1GzW4XQTUedlcGzkFXzXdk0rEPaEpzLVssj5IYvzrHR4zUN+tzMZjGaFFvtBFoDjGsUUFOsLTgdRf4e04XfdNcGyQfn0VKzkrPWHJ1NclObtVGw1uhKMKqhy3SEVOJYZ80i9rG5uRY04gDE89if4Bz+Zup5juyd3TXasMKzcRRxIX5RIaKwckLSyPgWuo0KlrG48uLLqvDtBjbyTHMtHWeHI6hZ0u6rxR+pZ59lifxWZqjho5p+//AMUwCauTi9pEk57QPK+o9ChBzX5gJ6KGsHdSDB82b7V4/VeIaoVNjD5xAzdQee7srRqePeb9U6y1mQmuaXUEQqLE5pwpzzUrC4ENzcsJdVEi4viR7QT/AMO6tnbZdUxvop6yvw9rrtOdzosD8ualhE/QrWFTJGbTDXJVeUK+cRcEXdb5GV08kCs9iPI+FtBdTzXdl3CI9Va2bTEGiqsgt3ic6JX8yNmyjipdVCNN2AoiB1XEgJwvFbN3JMxAgmjhyKdbe8PqrJsVbH22mdV1UHhnJH6qmnTNaKd0hDGaSnDkfNxK1tOmG5vWTfBRLeFTqaqNkbbozwk/wDkCpWL3mp0PyNKose4SFYNmklOHigRkt6p5oBoW9XEFvO1hOjAcIky6EQ7Af5TKtbKc8jOUIsHCx2JWnadkM+Z6LdFAqwiCaHNeI3RRa16rJQUzund/Oaz53WfbYAOud48u0PSLstimzGy5SsfzVm7uFK7qwGYLijyF0wqAkhYzAAIAX4n/ADM0XnPJWjxnwhWto6s0VoRSiGwf5VyU6KW/JHp9Qgv/xAAnEAEAAgICAQQCAwEBAQAAAAABABEhMUFRYRBxgZGhscHR8PHhIP/aAAgBAQABPyG7jhuAuZiq8cQWeOzELfHzAVDU6zfgP7iafhlWuWGsmiG4S6x54gfg63hiS7PmviHNdYlVImAxuZxiTjhqhiis4liVuVZWyJtsPeUI0Cwy1piDOoCscED7mvaeYY9e0zD4ECo4jpLCi7YAVtJDeYDSJYw+iQQUixZjFmRFuvB4jDA2iLlepXg5neTMUmMAxOdlM1H0XgjLEz1DRY0fqO9N/wAESn33MWh8y6fEVEvIwBwxoS5rK2CwNxVLCw9oepcm4+lqAzACNCo6mPvQYIOo7DqDNCkT0cIXLkMFHXsxHjXzZ+ZnWLVZJRXCLvPye7DeC8xsC3v41+fQxA+8KyGZNk2vVQPfs/WYrHul6oldPUNLNSornEzMEWxG8UmGMc7Iyq8zLcRm9YPjmHv9CYnjht8cSuzfk9EGmGUlEsIEu4KJpFO5AodKJ44H8/MxTtbT14gHvv8AifzF8mFLMTFKRDmJeYWl4m5eYj+lLtFuePvPBKV+5etxpLHP2RdR0RaIsE9EYVyskOZXPrQrrNHmbimw0com2bvMIAKdTALi0mTMkzNS8bDXvz+paX3IxZE48zdEBYXw92OsoMYvGpQSGu1M4fUbczjyRayJYs8nNXMuyuiakv2hUui2vUxNzxOPlq/mU5Xd9wawR0jubCVtFAz3QivKdArKpqVH/wCCMNtFMiwXX7m2PeYbTM/Jss+mY8/eLMV5GL6cyvMrG0L5WVLWI8k7ssbhtcnPoWA3HlePjD9z38suRS0VG430wFTywITmdVJTIa7CXRaj2PrpzMfx6W6YWem9fEGNorlfTLbeCXfpedTQhweaqVxpCQQzDslfzFbL0ozE0r+Joe8x2Ybt9cixublS9pMQ51uNai3flvtjtebi+gH7cs4Bw4diGhnwxKnAtFcwtFAJgmAMGp1K2LK0rRX93OZwL2TFv8zCIrbzej6iXWJg3FaEbsfhDCk/TH4zpXR9puxAboYremV8SkGlZC2xs0qkhIeawhQqVeFGq9z/AETv6MWy5MxkyniOf2v+D+4o1HIdfmWfkW/8RY92f5jBUuLs8I3D+1o/Mw953ngexEmXomaWczM4yYJmpAZ0pYl0x4iLWtoTDCFu7m+SUpT2jnqOkbVyifQMIfZCGPTZNzD+iL0FI5YcOgRhy8umVNUeQf3FJu29xt4ZR4DFlM1fES4DVtSwph6oyHvDgu1TO6sRg/7RtVuvmfUGxWStd4g0fj/ngmCViFedPeAj5XbA2gKTuXbmICWDOkT7/uYm10ztmooPAPg5/E5IP/ESmVhHmCqKmxCK1xEhmnh4ihVAYcyymb/6G/UG5YYd3YPEY2cP7StzhJsyyCQDDsV/31K9VLrdvEU41Fy5eLI8IWDybL7wGbfaF9VFYXz5iN238O8EzJphQGIsJ2MGqnyQ24CY+6ksBQ7VkHRqVihf9q/qDeDwS3A5MA+9Meiyy2c8SipADvY4NkFiw0xQnPpsl/RLx7w1DcrdfiXjMIKiEr+TEodNvMZby8In3kbQCQbTL32rbGNwegRTyy/wgD43+blLX/CH5zNsOv8AVzbZCFonvSgOeW5ZWtwaXZnD4lViLDR1BQqM3XBA2gFg2upUOAfa5fxFcehI5Df7yi6CJQjy7zKhqzxN/hcXjc1agQsTeZ+DPGv6PW4HmLzDfTFNGQvgmZe90ka0TN49Jfsb2Pej+4d1Rl6RkV6ij8SzF/b/ABLsCd2LEihjA2x5ZOJo/uBAK88QCEDa8XHUH2YfaIpxFIC7ExI92EDM4uZ/cF0ftKC5u/iGNyl2NwFV/ISAFbKkTCGrjmyAl4lLfQelzGzSXFrE2lRK/QrFVM3TDHmBZSiuNQrQ6VLR+242q2/iZEp2jGPF5V7yyENgtfBjzFL8XWC/eEMj5iXWYMPnH4Stm+anMx30StwAoI47JbktCpXwxKOehNAPDDvLHVjAWJQ8zu2lvBt+WE6lqOWYXQoJQPdxheJXpPXEHDLhmVC5AuIKx1j1SCt4rZNhyq+GVBlwyyGnKlFzD+4Ln/XXMK2nmbJSxt4llh8A4/53GSXv94A+1KJwF29w9KTm2WHmQ/8AUIEUsMj5+Iu6vliG4pHSeqrazAk7Is7gHPuxFuSXqpgOtIV2Kv5hFoTH5CZxxKyXLsgxYmCYpW8RC2oMr1Aql1xLhT5VKwo3GjKxBstXMUS+NSvghutR24OMRGlVb+4gv6qWryRfzDY3eDv/AAzhJcy4YUXU4bzMUMbloTZZ3C4boOiWgs9Sz5Tn5iWOoLKKodGnLZ5mJle/eUhUaMywUvh+YMH/AB2w47Of41EquWEtHKeW42KoFreZd+hQsgS4Po5/ghExi1Mehsjb8wj5KCTi+eiGo5LnuY42PGJbIHSVJ+kbY4TYNlRail8TuFDHA7deDuKcqMn9I0V//sTERh4mCSq0bo7wWEKWXORe6riVBq5B9MiNE7iG9PlMwh9niZf1PZ/6DTKKb2e8q4DADwEUpFuFvQMSz0IaKWalHeYQya6wXNPQagA3sc9s0uYzPodmGocGbg1YSVzEbYvP8y04gL2sLLmWHgDZ23jcorHkmcOFftEGwzfiAfDowVmhb7qmc+o2rj08iwUl8RtdYt41LzBnuFPCXiuF9gK/f6lfs1KJm4ntZ+aVE3i21YJzOVDiVHUtD0uLUevpjcofYx7yhVpTL9SvtemD+BD0jySnRUGQudSEvljpi8zIGeB/MsQLux/Eq8HQmi/fEyiB8z7VGH1BsglzNry87GGjdI1dysfAGqJSFM6lVLYExkh8cSlNsIgwRKcBDRusX6aSqOxwP2ZWDqKjR6Fz76j9y5S8BaczWueuJeFGaMJwp5bD7z7wl4iYYU65gsUnELDZjxEYyMPkgrLEw23Fiiz8uKPuM2um0eWWO0qrf0grQlaugx73OdQJl7lV/wBww1zyxVNY9ELrByQVs1Cux1LUJ0pHJcbewwbLYyew34W5mgJs2Hxn+YhBtnwhUbl6IZhFRtPMGJl3ibhKJQtMkqvBijFE8Q/zAt0nXib+otqRhZcjtMpcj7N/MSKRSPcCFrNrv2J7BNczEE8yhSo4My/l/MBx6Nj6jwr2sf0lpdqsZ+8xtBD6I6LRwP8AmoQUDZj3msQdu8dw0orUUp7P9mJRz5jg7/ZPgIURJqeHcRcI/Eaga2IA7uU97izhuBBIS5y5ik/FBALkdfqUp1KkuZlqogyFsW14jn31QEJtgnDpuap1EGlS1G8QKXpbwf8AsIWZeoRrv9mC+MvMH/tzOFilUzuYfbWNOmWW68nuXaVlsbZagQq88o494kQCqhQcfcRioBV3Lb4c+zmZ6w9oirZf70z8fxHFYCIaOPQUlSwm/Sdey+WEmPR0TVtmZUbeEW4OC6mkrZOZ7dXNArgKjjbHBCL58T6x4W7ZeWIjyMIYvxznYpbbYOiag+8aKVZKa4oaGBwg3M4snPZ/UFunsYjkOX2+yUgFm4C+zHGoXlr1VxLMW2cF/c4Gv7QW/RRqc5lCvTCrpGBuCO5s6gvOSYppB6YVQt4yxFHuSLBAtiMrbn7iNSmz2hox5xH6SvCOq2J5C4mB3KwsGe5R4M17zBaA1nbaos2p2U/ZMh8EacEWIwoRZuCAdqacRMujdTmHx3B53GmaCDjJGhr2OWLbvJC5yErhBkgq1gaJnKXTWCBblWBx/wC5WzqVWdEqQC4PtGoDlym9Y5mANwKXDIlgXGtuJgg9McZuW6psdEqqEZTJXEA0T88rLAyTkK6E3Dcz0RzPXfEreiE4gv0rEttr5JfVC8E0BDYJRDa11Uw1hAEMJQWFJ3LogbDlEiFg/gjTM4KB+SeeJRIDw5H5loV1Oa8y+rlWX1GyfUA07iqYR8+kHucMs+AJLOhi1NZ31G0z5ahKoC6GD+4yUSgkUFlJRMjGKdMkZZGAqpxFlKfSGR8kcAWTQgCaZStNvUul/mBrkxtVBKPMrZfaa09jzBqq+VQCuD314lFYArZHT5i1PXyFcXyzg4G6OiBxmwrgmX/E2NsPN1PDR7s5ncpf6gDezFGIYNUbED3mUwg5EXp1OCzGhyX/AAl8nU6CoLq3EMhrTJL72KqUXCziaSr0EPIO/wDpKx/k8Qyq37xTm4gZXlRxfrBt4tjZfDMTo+pjAvguVmu7qA28M+sST4jU1pg5RP1KboAKtF8Ff64SoFCp3/uoFR91Zjdgd4+ZyDQEBZYlDhZj5l0xgvTvxGGIGyJmWtjCrbf4iSsJ0xNE9GBL4T4VU52bfmCKZWLdRcoMCWa/xiBp1xOAi6ZqUyHEpaoPCyqushqZJz7gfK3THYF1LwZ4FiWIeOpRJo6BjXw2IwMKW47lVkuk95AJY6ZWBaG4inesWjD8ODrlefmB5h6As2ly1gm3jKyAadzlDcGbSYIupOEb+QmcN4Eeh2KzyglmdeYAC6Q1VI/hyLq7zk2LLW9QXXAFaurpuJ5JVbEtb7kWaN2L7J7Q/dOH+tmWA6HmWgdlzfkPdTL6RTgK0moe7pvlzWU34h2xEIGu2DMV5VAAtBanmAMJDxMmqviIOswQY9O9kfhVEQNQRZF6ZgNtez/iI6xXiXIUR33UtOblMW7KPEKs9fhOovJDqY4LuPtMgmGia9EYoYhUVYfxKwvyEEWdcS7DBcLz5jwKEh0gu2Kniiz1CME6oOfOYKb7lSM5Qoqo1+tLiijHoYRSnOX4xLRsTBB8xDdIjknMqhLpqWZ7g/8AuaZjtKIEncHAnmD4C94EWBjxxFpP5TwmcrjquKYgwM8J0yavmV7kRCqZngVY7DWoz1lkWGooeDPzHBQm1piN45f0JlMsr4A6RZZVpy8GGdYMcGzo0ww70+1zmxTLjjpcfm7kWr/z8Re1gwmMcJshC7YNMVUD4kmTg7JUyPmZoLPExEGQfSDoWDUIqFQtyWo/J9QE4n/7lwtdJC2YhA1MS2xRs6eZQOr5DliAuXFS0DeGXWxq5hzsm+PpBsW4K8VlKF7NSiWPiNcrWLJQGXak8H1Bk4l4wnDrccZZVPiFrM2jAKzKZOG3iUxdBRYY6DEVPcl+OF6IR7SyaYb0FrCFWCWN4HXcQoa8zdL9k0dv3Fb9yzUCIr/ZxNgoJdL8syS7OpcYxLsioK/7hzviFxPN/wC0eGvu+xHgzIZXkuZKMN1SlNuoGHUGHHKWD0StPqkEGGwHgUb/ANzAGt0GotNTTmQO/wBfcJtlbPB+Y5K0Xli1aG9TW1OJbxo58yOsqfmOuHvLVfERnAlBYgN+B4i+WIWfU9njgS6S8jMIs/zzNSx4/wCpwvmBCA2dbfk8zYyaK7ldThlBTuD2UeYPnN06IJrEoFdbhbuTApxv8pjU53UdBjaS6BeUDRMWTSGLP1E1w+jK4XcpZYBRpzr0mhKFAWsrUJbeNcsJnsITmgwiWp1Hg15h7ka8Sjv3RTpuCMwfk3Alz8sNFg8TyLCkKuiE+XuB5KlOax0hhXDSqYLL/tpYCDmlMsLrLx9ybhx++h9MNwgVkFn6QoLF+k5oqc9rHDWo2drNHybnkdIC7MMFFq6n5X9QGlvwfEBc0CmDHU9wSsopSSXvzFTbwm4PSD86m4nyWT82CCPMr1Oll0zRww6QzA6s1ofMAAAODiOvTiGimDcT7cQ7WR5cRihIEZhoLZM7jiAbYlqNa5bLUMWDwQUWhO4x7K95VzMDiLwp9pZVD3l4o2qg1jM4rIJlkqKohhc3CFYtXpi6XMeWO8NIS4XflMJyeyJUycsO5PMMRcUyOLhzWaWOU2a9omr5JbYSojcEcBEtFExEnJt+GLXS6ufukusj4gO41QZczVBvuDLQpLsl1kl4iu5cATTzfeYBTFoGkqsZ9o+F57IkgJfmWyhTd1cpspyuAI/xDPYzuU/6xmqYY8Uz+YFF4HTLNl9SwgFnZ1KjzK6w5WJiPuMy7C65Q8sRcnCDCU8sEql57YZUraeu4zNICF+oxuBK1WzmJp8hxDWjsQG6XljmAwbJdcQKpKEO1zDW3G0lkK1EKzMJMzgxlMo8lPMBYjls8wQBsgPHn+JXUY4FS5q90IS6WFn3z3PkEwo6hRcw26ZvxeJiuQ4685M0P4dQNMmakoE0QCoai7gqu3KOwfM5WDFsbjzqL5hYhTqI0TBFTzMjDZldJLxWYlFzJlOiYjU89QiAhtpCIWxO6uKUOGfEe14qXI6r/faBF8NmIdixa3nP718S9xkFeL/giEbbYkOV5QIUamlXEV88zWFvBMC8G45rnxMADUtugoTwNEzijX+VQKWg8hRMMd/Mw+F1KDYda3KQaXqOoF9krLK7myPjGppDJLpd7suhxM1CDhCSg3dUoXj7Si3hTURj5fEcXT8RuXLmaGZHDccwZN8xLq7bmmcw3iMahwFJezBcQjoPCYioU16KvMvTZpR23DCF+81B3zr3l4B3a69pUt7LS2ZF8xptZ6ncom5eJzubzywuKcxK5IHFd5Ubr3bgjWQIcY33KV49JxnxFBa2LOL/AGw4YPKmCUwLa2FV8yx+J3NGH1LBQluYG1FB4D3imyCSGeyW1XW7mvz8QHQRaNvt1LuufWWWA94Mmv6g8sCvYAr2xcw0LcS15i49Qb595gBlO6fMxa/KLU6lhTklOXNquZ30CNSTJzc5YaiGqiMa+cM1OMYiuNUF98Yg9rq9EVsxqK0HWkSgBmmA+x88yol2S4g4kV102hbvxCf3Jqu8QUUYxFTb7l5EGWxATo8Sc8RIU4l79MpN4alTXEYGx4eZxJwzLqWEjEVWsYG7ixbN7vnqaLefMbWKrguO3iIQ7u4otKHcv8wYSpior2XglDZLy6i5nlLyxg9gloRiJkchNk1X1bXZ7uINe1v+EmMqPHLzBAuPaPfgOJZe0rLTcwmAHBFXjO2vzNXEV4P8yxZeWpgGBt6SqVexutxtbHkfhM0w7qCtkBXbubQ2ytXfXMKcxKPcH6Einkli4Xg+SPUia7b/ABdsNKxpmF7+SZwH7ss5qOGAoTRyxTyHiGyATXyueIWRMcMPnP3fZFGOBuvtlOGVpslxEv5s+GbAXmyYFdczRyQ5vDOGZdW/EvQJTHiIqXA2aHoLA7lCLgiG+UgCIGEv6UK5YEObymX4gdMrb3GQfJMQNvwRgWPEC7hq5gjnFQagFXQcwQ5XGSXMd1lCN35w8U9qo+DQ+M6V3zFZA16YFu+IXVEdMa+5Ey8dojDJYOpYXEmUAYkmsvMwGD+5rG8QiqIM2ab/AFLUIdallLK5+n8xOVkGLPx8QVrSrDAMCFwoLNM7uKnzyRtlwNy9WVHdcx2m3vDgvtzLxjMFyIYJkxA5YJhsMZmKBTVzBsT7eZq88t7jgN/Uc4Z9xz3XLKvbhwOv7RjLCGHcqZpDiIw3Dke2YEqosHtOQ4YbXRKG/aWmBm8sZ4uBxn0bHUNYy8Sz+rG0Vjk9kz9TJ9zt9AORggrtxdRXBmwntiA3ZOZl2+nEsrL8Ru3+dMGd7VlZRFtl/UJVt0RRn9oOSPoCXy5i0i5D6CqGvc/n8fNpVbjw79IquavxHTN+8xB2JlCRd0oo/wBLD8w1roGU0BfmvmLkbjK3G97h8/tFHm38x1grqFFe0uYszhSPda3g5Zr0qt9p20hc7qIvt7wyAPEwrGepkICWDSHl+ZbYfCZpb+b5eoUXPA58DrtES3owA6CdriX0ggtmOOfSkAcI7A3Ln4Q4lGEvi6/mJf8AYZVmKhjMFAcQ0Bupa48AgvYQzFMz9lrHKUx5FZmqoWQi+FtCvxB45L8J0y/Nw2DdwOS8RbZZN7UYJPTVwqjtLu5zBUu8z5RKK7gcOYi1tIRUG6wx8huOF0QUvEwRS42gd8TfeMFM+0Yk4gvTb5zE2ZNr3A3VheFX/qgjlyyQKJ5NsxOv5m6cO4mQ8EGDTfvUoH2eZhirgbOouuHPw/e5VlAQ5J3lc0sTpx4Y45Lcbhm5xYg5Yr3jU/0JUCV3kxXUKqPJn5i1K7Ry+OvKBbYDiqdHRFx4YOg9EoqpWzFKuOFO0ckLSdCNBcGMQrOGPvw0+JlGTXEsoZV0Edvo/wAqgAGOr2/cv0H3DAYJJaOPeWptw8dy2/e4t5TKNMTlptb8QKOJSMVuFMIMajFGOCZtViLepslyU6KV2TYmp5EouoiZxzFUzn8zVnUsxeIZeIsDzhyQVU09nENXDdYfD3lm0cXFbuuLl6KDKwt+o5LHinMv3/hHkHkRymvLqZPQ7jjSu8QDm4q2kJUv8BKssro6JfaoeSU1ZfTKVAc8Sw+Of/ZtJhmm5YvC1USl/im5/ZlP9RLKjOzqf56I5E5WseB4l1YbhNG+I7HLGwbSZpYehsSxxkjFFNr9Rqw4xOzctXsMRci5c3Mk5qNtGATC7d/wcS9rEEp3mNFNXMG8zUXPui23FqUbldHbHdncfCzG+viXmZQwkiGcj8TgnFKDqI8G74lSKAhZNw5KPMrOkpxEKauXetTjWpfGpyJkFVt8EqOhUCQj0ERlvGN5qKAfhl2Btm7tzAF2XC+Xn9TJJWcShasXd7lQl/CVN5O+5UVtiblUu8cy+uPpcYa9QMRVQb5xiWG0LhX2cRjaWtTGXRN3MwfJjUt6dZoH78eZaLJTRB/fbGdMpQQjatuGweYroics1KyWnpXiXw8y5E9plR1HYNTb1PYmWFNFwAyzww5C/tCijWGAghD0irMGsriI4Y3mCZuWShCrMwBqVNpij6OWeJcbJUWan0QtkgNRCuMrtfx/MNOXdGGjGOMVTiEzxwUFCAVj09RmSEEPGXrLt1EqkWn/ALuFySvuH+zOR4mLNLfMwKaG1o19zX7A4m88DggUYrzG49wZsxNYHwOUQUMAoOiDx+5eo44lv5JiL9zM246hRkRT5EErnIRwY3g/55fklssTuXMtuCQcAOPMtaWK4I1MivEv33LimYoobJUpBl4xKiwdzELC+BwSx5Bmv4Ie8yYuo8hm9sqEwY9Dnj7m9+gaMEPLHWYELajucSgYuVricjUJITcSfwiKTS7iXHyB+syq9+plaYrcCHYlqjZxMmCYdRr16RTiPQESRNA55jqwBuumKsx0gqUVkFj3/KbWWrJgcv7liXK5fMGjlHjG/MUDQGaZhoNdl5lpZyxMB+PeLa88TLJp4hHLx7H+5YGYBcVb3c2MAC+PxKJVSjLGpWiAAb664P8AeY3LQfdZyOWai3OInagD5Sqo6IMYdsyeJR4pfF6EkZDUqc0V2HI6DzN2l3c/6lOolWvpgR5xGC6iomZ5+gckS6b4h6HBqk1f0jDVwDBT3ZtiPLlRpfcwA4jvnKFh+JYpz3GEj8QOEA0fHUYojOYe4O58Qo0ei08+JUcxbFQoPRAh/F1DNBz1Hf8A7mf+SRTA3u79pgA46fX8z5AeWYKclV7RVDbncXHTr/bmdU3KPENaLhb25lzbIQI0xOyUHxM/GNwKeZRyvmcwW27fw4JQyk7AxLRNcETlDTALE26n6iS/EuPymGzPZLgVUsJcE5oVSgwceYQFnf6HtM2PWOMu8RgDO/SwpnmHpEA5FpTptg7xD/p3Esj5SNtqBbfTCOogsqNZmgwZTuFg6Z0yg/DKnGpUU79VmkpIYYZyX5jhio14X5mLQYXhlZpgWvWXsjatD4OH+JRNa3yCDFcawZxLys4Jz7SwPjPymbSaVqVh0DtrmWYmrYay06ZRs/LEE+Cr3g6looMsUwD4nmMZ5lmB7WHIT5CZUuabd6cVEeBdq/8AFsuGi6ea4qNeu5bjZwS25xCIS2A2HxOYMwZeLcevishmWZAJhUvcR7gDxLCczbWIMZuLFFldzmMrY9LpITKLsgB1zNohbQmJYzZIux5JkpRN1gbOfRu4GtTGNWk+czJX/IaMtKVmAf8Aky4mEblNYwslWYkprjnYtL5QqxMTsqXw6vpMYWMv5IENCs6rw/xFWqZnZt2HfmNl4+faBD0Wm4ft3NIryVM2RoBaXzUwoPs/3YVhLxfER5vPmedvjxBRwqY2RtfirFcgaLqBmyYS6zX3LKXAOLw/Zm0w5rIumGjzN9v9iLeYiooZB3K5Nce0YBtw8S1x6qX0DOy0ijqUGJ457JFczAWVUbiwZbLWuYu/b6zYqUsy8xDOaazKmURq44vKKsDHqZiE7pz/AKYb9I4/8EVkVw8TNKNQxlvxNfEBTREOmA4Tkwq92Xss/gGP1Me3y1v3GIobI8n+rJlPUSDdVoXbzKXXkX1wEu8J4w9iy01MMaAuXftEoH2NxrLdLPn2mf5bDKJFHrTH3oKOkoVc4iVja5e2CE523mYbULLFORfzFQg3ay37PHiFQWNrdYjWvdc6Px37wUSi2ecwrCIz6YszzFcQwPjcqmGIp8D0TjZfNILJ+5jV8J4QcSw9DuxUWNwcTnqKpVz6b1KKm5qkV2zUcrl/KXPCNeMH5zNQh1hNmSjhjgXiIWIuHoFIm7fhBWfzFkvEvERxC8ynWvSeq31PDmHNxKVRKbjTIcYqbFZaV7RZdmsa/wCyocPt+TxK8ksaFvA7VnwmXiorDFKUsCmoltUZw+O5jDeXMe1NQMsDoC3xUPLKkKr9mcvDR9D/ABG4SO8/qPUq6FbmB3IcHp57YANwgWG8v0OYmwpi8xbi+XfhcRRVcNB5g9qiZHLzB2v0uvBAdXmOJT2mRDfrNytmLmZpSism/TOrLzGBUWIxao2cygMoxv8AqWra59aCs1OeJfMSo3POfTZHikVKRMwo4hohNB1MIlwArE1L95kSrw36WakpPefaKwfMIWsZiu9IekSM/wB2/mYpHhBjxiDJfx5hZOo6TkfEwesQ/MlKjjEBBTMo+6WsvE8M7wektgMwGL8P9zONE5WuJ+9z9hMTgaNKDpjs3nM27lGWzU8Eb+L9sRAbND/fiDy4fDDviwq69rjax0AghRKotb0FHol0NswKRAOvfmVWMs3olPoSmi5ztytJceg4l5jariW1L7jg6gjLKgPq3MSm3ekXPSW+SLRRjmWVZfp+I+MXqVZ5iTH04S8xzjMeUBPOeFn3giljC5Pt/wA9IRtfmNjZ6EsuzUwSqiStTxfFxXbKQ8iSgWK7g/DTcnZ37Sn93/k8kYf3ETi4UOcx5QwSn1RnXp4emEwhyJ+yaKj/AAH+PiUeMBPf9pl5TmunTCmhZZMdMcDUGUvg/wDYXnqa8e87uF7i4ioG6ez3laW2xio7UhxcXM0T7DdzG0MNTZ85YWEvA9n/AMRVEq5oZfGcaSkzT4OKlxadR3AxlX6VeScQc1gG5RP1BWWvi+5V4qVGCUc1kiXjMzaheJkKi7sFLZXHmABNMMV8svbZYvQyr5S0xnoSt5eZ4LlRzCcQxLlyoZGkOZceRAqU85OPESNDx8QaB23/ACSlPsNRWwgUGpv3gOE2RodX4f2fqEZYeXL+moAoFA10yvx9+epgdkSUjvxX5lZv0pxLmaXuJOdFjis0R18SsNKpJy68S7RO46lHDgmcvF8wslYGorRdQkfJMDjXESoemsltGoZGvUrCWrEagIO0fVxHEZpgzGMYXyhyhSZzA1E7CpXfoUOJcRFoxTFYzF8LiEiGNHSJ2S1y/wBhj5IXijNrmZGZXR+Jkgj6KguAlw8YhqGKp4hZTdseJlBs5mGMWu+461JkVqZ0x5HsgiHLA0Orh5I53BmVnP8AtxHYsPiY4FtDhj2v4meQ0BzTjuPp2LPkV7Rnyq7lq/4lHWdjcnL8QJ522d2kF2e0YyJUx6jqyNwoEwXADEqx760QU1StxlLw8+Y6nzL9E1xlGBUyTj0WhDUfgRQ8y1Vhn0A5xFbUos9DwzaE6GV7iV4mV6HKI3dTaS6XeolFuEvxZMF25mCmZavY/M3jzLGJnzMGCMWZwzOYRXBfkQ/PocyoQhaX4Yi34NsZf8PMx0puPz8EqVGycWY0ncksBjwwqUrLhJni9kMVKgN+PMXKxGcnH3DJ39jye0vj8CuAdeLlyAG6KZdfSwBzllikvT1kj0RtXugtnA3LUWOuoqvipS0dxA7YWDawSD7BPFG/lLNuJQ2ZQBn4jJeJcuMbiVLqGJc+ZebPEcy/EtI7lhCbvmVRzK2ZXEwIZhbE8Q5mEVDU7pluiFMqGWrDFpuNLCpcLIRxCiBQk3RMTREMhB55cSqDG9648Tc8xUcVibrS4AgaVAKK9JCYgv2RKfufqVGb/hlEeU46SWHz4YWp/mV/EsPaBWfMR28WTv8ApKbeGhLJdRbri/RXGy3ieHUJM8eHf/sVTXVp8FbfmA8qXJYz8MI66cjmoE82H4KlfcRRf9iYj5uBrN18XHVTLzLctajuVxF6GFP1COWDjqCAhVOYmiZGZSnUvBWuYV5mkvuIRrX1qZR8QBnEp9C7PVlC1Nrlcw3SK8cwEMFRjxyZmU8JCCEMXWYhdR6pfaAJcDQhCKZ9AxrZL3x1BGhKYpUQ2/LMIX5he7xA/HUMvHEQlkw+hKYO3R64e+jp+Hc4ZRf3jSSO+/IjCdh1B5efmDUdXwfqYF3iBmTFGD3huOxK47ITEbiv2mrDsemEC8o6X+/3LQLT4eD8lSg7A1wqn5SkYgZVhOJejg30S4Oiv5S6a8hbzG2cRUTI59Af0B9pmgydTA78TDhuskbEzlnvFSuR3UxgJEoa6uYGJn1pZRScvp2kSyXvEb0w59oP+b68sz+nhqLNkephCB7zLzHMMILiHcqtSgYy0F6AzuKoS1Itp13F3nV7cSh7J8Rg8ohN/UtA+kQZhikNHqM8YDqLpLu/mDmoM5QJFa/8xESG3t9qbHwxtk1YrUyjjdy/mPDMrsHXknbHkeo3lh4ddefE2Byjb/4/uGC2B5I9BijZWRAeQDTwf+zPDcAzuW1z4iBxLph+5ixmGTzYOVBnUYfOSXDRpI6g/SOTYaBB0TymgOu4sfP7mJv0Bmo8kphuOvT4pmQ4PRNSxBalakUxoeLuopaU5mCD8Qji5hW4UPUC5vZ7SogsuD1OojNH16RbhfXp4U2h2wf9DriAUZlxqXHOAivN0vECs8wlSpaZxklw9Bfupn2KSAI6n2QrKhf/AHNrtdpYNjdQ5V+eMaiUdrKgQiJxKtexABSmoWyahGWCu8mmwp9Kx/URji7+evyE0DoNHR7xuQtY1x1CGJ7AmmmpkIxwzK+ksW1bFNQqFcuzWP6jUXa9Wx/to1+4252A1LbWu7jpUPcuBxllwwX1A36G5TFaVC7/ADMpLSJZCTETykyc/PgzcGocBzMzOvoHRIqSvlDktjtSvNO/RVEqPXcw80pCPCJefQTNlS7psvk5YG01NGeGc0g37Q9/QYMxtDl2zNh6flIY/giAl1YIiV2eGblnCIBK+QzMZYOT3lJW1zwTEaF92ErB26jU8BOGPpXPKYNmvEzzfyXXtNrYm2EKvG3HCw183MMclqimLKuVrybmYM59D4NLuG3/AMR0AcSjmAaGeN5v+Yk3PoZCcXTmpc4Xi49QHkzLA4c/MCiuJ7pLjqV6qGXILj0bCKyWpL8Spw8vn0sHb8lfxFjMve8wtEassudRW3EGe4H9wwmDz6MXEyFSouAb3AfRMenKVCx7EXAc6iAx8EoOKhgnEaeZcbxAuXeoOZc1XozFkn2Pq7PiZ607isZZK8Q0Xqpv8fxLrYWQ/NiUYjzn4gxyUuXuLtZV8iY3q71LJdOa9EUEByW89QOvAV7LWqjgapxwItrcVrW6fRDJ1mndV+blY+V+5XF9Ru8RcLM+IJrcM2Bz0cmJQ1WC+CaFPvv/ALFYDY8RMtcqdeYODHvk/wATeWInYFIADVU92IVu/M0LJqpiiUxmoszIMxErTz6OZLjyRUhyfylYmMnbMJUWTcQOsf0j4vSEZzNcuVK/UyZSBJUcSvBKvHfdl32Q2MG4Vc4c+8qdsTd8MaeFwX9kQg6hhKPkwy1DY8TP0TVN6l3ilEMg0ICUf4uK6uGkJQW0Y6mYAwa1qEwdiy/pR48wo0KJZdz8ECgBCue5pVqiz5lHS7pKSSzColKZ/mXg6MS8xzUSkALWPLNSol76h2J0faEOywHUr8u7USmW9fiqYBmzFfkn/9oADAMBAAIAAwAAABB1s59mN1WBm9FtlgGZj/P/AHimkT+qQvWuz2jss6QGfXwW/wB50Y5X8ZzQdTR/yxjU8eidwolEfumXFhpgafIoRPtcNjpBPJ/xtuSIzTkBug/SQdnY2dvVwmcpx6rG+nJW8Q1uwAcrBkmWRmeOFlmFMIlsPCEb1kOSwynmu3pUqpn5uA2aAknA/wALLTToJRpLx0s3lqxdrP8AHIQ7qZM/Xj8Hz1Rp3Gf1K/JhI/kWUtxPbFUXb+9nE0YMvFsZj/5/vYXa36dLJH79OhbbBe7Y4DTHxz6ypkjfJsIK4fB3PM3ZQR8SlTsRIy/woIqaKscu7crFs0c/o/U+cmiBdTJsUUw+zVQ/x3PiRjEHaPJe4GW4hJwaZRvLnaKE37Ads1UptN+xwSMwpsVqMxytww5R1AfCcdiC0kdKufbcxNAAaywilQWLEyfiRWaTI9rAh1fW5RvsC3EsTVFggT1M8BAPjMkL0Dr+Ok7WQwFPQz23kZau7Hryx8RufKginp9fnIRqoGGJ6ddDiJ3VRUW6f+r7ohgu2D2ii+aFI1bUpbBZI3+4CqkYG2pJMgiWENvMrqpYJcrrNe+ZQhxdW0PqSZJyhdlK6MSI5w522+YnbAofcRL9mXSKSxNl3ZegmOpJ7KyxNsbDzkkeO/jb3kMMtL61OMos0w+4b6Llr/iNg36Iq/iN/PRRqEwWuEMQWl44nVWyMy2DkfKYTMWPFITHkHSsfykY3mmkGs+XQVa9GLO1BTV71gvu+xkmDpshaxFRPW6Io6D8tY7e7pqxchlbHr/8jTAGfFU994x1c6ZCF6y4J0RrVVCNgDhEHn+KWr3yQb6wg1TJFy3vgshS6P8AIjC6ZnEqKGF6mDlCr+MP+K/O9powIyMR0Kai+fQI3HIfPonPnHHAYIPPAw4PYHAgXffHQYXY/IYH4f/EACYRAQEBAQADAAEDBQEBAQAAAAEAESEQMUFRYXGxIJGhwfCB4fH/2gAIAQMBAT8QTszy0p65zCBI9f7hPFf/ANhFzwJUOIDJvaEDhDs9fOf0ZcXp/RL6pmT5/kGOp0gH6q2h31bM4H3wJ+rq1R4GXw81/wC/xK+BPEXpDYfAs5mEtE5Byd9fIxx46lR5Je4F6mAfV+m/CewecSwcwgwe0Ej6b2Fvmz6vU/t/EFNtj9fyZRtWXP3hrZBV3/n8TlD6kn6j/fkCLIbDHYcfURxB6fZvG2TurBHp5DYnWUs4n9r+Eu2Mf0Az9Hz9W+rmb4i/BLSMOyw3S/KWa/v9kN1S9DZaA/Fmk7nf735gf+/ibb9P8Xp+hJjLmm3WBAuQ8Bw4z/R6r1crw8KcvjInguXwkx894j+4f9f/AGScfJC+XjaN+SsyzgbfOwuSQ7n7RyyPdpkOn7WJ+oOvQWPEfv8Av+bljhdP08Brg+W24eD5zxs99RHh7hAPcmQf+rTsxMWZ6Nh7ssdy56xYHP5P/E9OQp8oJT37IFz8zlnv/Vjwx2Fiup+mw5+0fdkZ0mL26x4Xrl+qrtkqiTaPb4dkjBs4gj7YWbgXsOWrGDeFg7/ia9WjkC3DIayAe+Cb8NhNXspA4+SOfv7+j+f2ZXk3+YDg37S4XaxHl+DLeNxFNYC+weoo9z8/P3t8dYCbND4DWTLZPpb+Y65JzCX04R8rhxI93pDswDHTEEXSwj+SlX6NpQnT1O4ff+5e+lqABPR/xsEwWxgfZXHV77cYlcgficXIAC9J8sfm0erNyJbuzE5D8hhXL8zbg5caWDE0g/8AZMrSFIBcu2lvePbY/wDH/kCzt+S/D0mPszjr/do69mPS/wBrPgxSw+KyRp7JyR6t2WhIL2Twy+eNb7v03YbUB6WD+XfolxL02G+k2MyRDR7Zu3AtmyRuYtJcQ2S7yfSu6QwD2+S5LzBHsMi5L1ScIdJXMvd8F3D7aAy8sLJhODJ7Kf2b9E2HSwOHbrkfmXu3WHJ/ZDpLXWRc+SVQ9nnZT2sfsAGIU9ct57m9SvthQiTSRT68hxfBcFyXDa+5G4Er+kH0kfzcBX6tOPib6wQnq0cs6W52CPZfEmGbdNhtpZYmxIxgJcwtXq+iHwk2DzmYyQc1jlEwBIO8s2EgXrSIavkS6xJeeCvLA7E1npYMS8SPvLLo0LDVxd29JvhdwtZ36l5acwnTA+wigndjx3CfeWbyYBAawvkLw9YDre4kIMmfAOwjr5oG7zLVfsXdYUZdEL1s7E+7As3liSj1YzyNCg9J1h3xmgzF2Sx70+EJ6inMoIjkR62HhmYdtYDIm4TzZiGtjokcJQ1uPcGwlnLv3OOSYn5gtXeDAfPNiPD7ZRAsyMIlhqx7LzxZ3x2x+QzntxJY9eQVwtLIRz1fRs5h457bK4WHb2nu9QY+idlDZP3ww0yHMvy8BGW7ng4627s8IRn8+G6mct5aglHEt+iCuEC4LN5DJ+GwtLOckh6LTjBs2jSL8J8ekotfBxADsq4eN+3vwvyTOkYtk8IMlyYkJ3Or5ba7yNVgmM56tx7HkOdhOxbHgj54MGOT6gSLbsTAi9L544TNCcvs3p7i4MOQPuCVtlwvbyOjrYps+o9z4yFrDWx/EcM8Exke5O9iPSb9G4LbCGWsvgaC45YvLIOdhfRZt3ZPpemfU+5LGRvU/wC/EOOxbQ4/4sOZ7tXje3LA7aHxIFPCBHbXGDLBMj7ksFwnxnZMMOT+WfA5HoZYw/bd92w254aC9QMo8LCfc8YPH6RLT1bvUvbZj6/dxxELCCEsNtDUYuRH3Yf0b2G9mWc8Bse4abbnjYnwMdmZZJZPex7jH4loAbUgVZrF/dR9jDnyDls2aDALkNvaPXkffAbO9l2O2Yy7J4W3wGxiBBp4Ukl3Dwey0x4GfXLuYQTy6jmmO3qzH3yBTSfceR5fAj3vaPV6RH1P9BK9J8OBn3Zem9kefEnk+Q6Qckur3WRif//EACQRAQEBAQEBAQEAAwEAAgMAAAEAESExEEFRIGFxgZHwocHh/9oACAECAQE/EMxDPI5cMWSa9lT4JZG5f7SWxwly5Jbwv4sJkotbf8DnY65BEzrZMB5Bx/plt0M7FgIZBlvfx80yOXmLkft+/wDIdwivJAitDY0Q/qJdiMONmpgWN+kMimpjCsksfsO7bFvst1sECfe3hf2atR6WewNsphY/WFLLZtmMzHhKAy5KG8WlLjCX4qIfiI5w2SeLcD6+rkM9tSCIx5aD/wDpEGJUuHSY5K4OfliR+3b2STYx81E5YvTkMKxvBy6n8gAEvL9Z8CTXFqjCn8S1rHbN9I/w6G0uHtxLRnCWjeLZab8UAdtn3q/24spl/Ug3cuSutqA9/wAIM1Z8f+2qscj9mD17/wDf/wBSenrCf7Jt3wui9bE/0379HxYbIwknXkdx2pM8wNCLd7tfLbAazour9Y9jmo4Hyzbk15Oyecl1+Lj2wci9/wC2bAvF7melGTlxXkRxuCPIfwCPgJLab3eskT+iRc7Y92wY9tPX/wAxXtkRn1njbc58KFuOyYjOkn/8SbC3LAwnjjHb9pRO2fyCQ0gHks/SV2/8WROErn7n0zVt7ZFmyZHOsf8A1KzSjeBJ+2oc06exGJQum2eLN/G4d+TPf8RnY3hnCPhJXZci/wDZCDkzcJGbKbL2GHstbM9RFz2Afb+L9gW5Peyn+CTgwvyG+w6CL0bXgf8AsNbIMVcu6hk//KVnWT1g/W438/k/0i2vYfrLPWHGmwlEKshB34yNl5eUNRJPxz9uTf0tnkq+/Am+T8Z8Rv2FksN+TLkzGHcb1QZYbsB7JbfmzCrz4G36rJEvf9tPH4dGDtzn+yjok8hiv2wdiGsaY5cLb8+N9Eb4EdOZ88MjXtxXsn+TPg3JPGOGbmZO+LOyvEg+9gP5D7GfkTYOw03qF5E+w6Z8jjiPIF1Zd58F/JT5CwD+pvQmSWHs3ifCFLjJ906XSOE+YOWLjEdJGP8Ac6y8kJHqXnzkT584Xb+LRbidW7WEdtt2NZ8jERBBksN+LjsEWYANlT9Wg0T7A+S2uc+HnwJH8j8bYbPmv8EybbcNthmcm/sBIs/+EdmbM/0nES6T5yw5FczO6ws5gJSBFHIVlPIfvzSE/bkZDDyRQD/hm66SYbfl4mSWblks5dgB8n4Pg8sOpAbBndqypOmQIyPdkywnRkp5bjb25jXY1Mtm4HD7nwP+UuJ5LKWx1nlDZcjt1kicifn9+EtblLHYc2D7DGGeLS6/R+tty/L+fB0Jab/hpx8KWDZm/ZvyLMLH5mfGLx8peoZS0EXZw9iDsgPY0cuuxpy6diiOrpsxCNHJU19SYajG7+dh1+Z8gX5ZsY43+lmyy8z7MTt5EEGvzX9Yxv8AqI2Bzn/3/l3/AK/vy284m9wPtwUtPhNiyxct4yPnTY0tbUl52SRk+zB8hmKZE1lO3fjOXUO/sjT/AHLDf2CmE41GjL1MCqwzzzTj9WtnPkCeQfHibon+R/bryx62hYN/25GeMLIYT6Jnpi9/lmS3H/UgIewh+oKDLKNmJgc+ZHbcMjq27SO/D4dJfhHzpvcWSXnwnzssi1mIRiPY8+LzST3JntnOyP0sZPS01NxC3WWDXJCAfDQQ0I5z/BvPjX27ly4cvbPmWfOy8+Ahy7gyfLvF/tMw9sc23P8AfxJyUT5mwBLjLYcXDsPwmG/Dz8XSTS6k+ZZf1cLtYTtuGvx8bhHXADkm7LFg9n+GHFEkDjBjHk3j/Bt4vB/g4f8AH1+Pce/A6x89PjhT7s9IeXZfj/geV4iTt//EACYQAQACAgICAgIDAQEBAAAAAAEAESExQVFhcYGRobHB0fDhEPH/2gAIAQEAAT8QNi50wfiZY4XKc9TgFFh6SmI5VzMRgExHYvR5l27g8sTrjeTnUUCFjfoVdZalLJJsq7rWpRTWXUFTi8rxxEjA4S1u7Of+y5lNdiKDKvqBYJUCu6YoAPiNk3OCYiJdQbGAVUXnQa/mX/lH2uChsuXRSxzhW4IlH2/ESEQlNFVf5gVeYAYXRfaU/THUljEVe0oWS6WdcS0xCoZP8wvuyyOQ7gUsCFaFBoKzcdONo0VKcRcKyLmYrMC9RuIu9RMYncQBCIiupiTBMnnHEAFCyrINfatRb/BZQYDglRDA42v+YFN4ZYCtWLb3L3HNSmi0iXyShUxtMlETuNRMzeGkv6YXMripD1vV0idSuPgBppM9sUrv5YtK/aAzIvuIpbrG+IzDji+4MBKL/UC0DiJsODXiK73j1G4aihZqZ9LxelvHxFDDp2am0dVKlwkArKuCGDRR8Ed+UsFlar7I5hEy9ZlFBMoOpri/UXU5FlzjUAw5CbgFxoCuh0fmIKCgC2sbGeHiVZbEk+XeN+YC0gYprMNZM+JnRV6dXrPqAdeF1dcR+H3Bm03lmCo0nmE5KSvGp8Ywg1Sl3MyKWpMwFS7U7tT9QKZQ7E5V+TUBCUuw/iVRPaMm95Hx1Ha3F1kglBpZY+oL5z/EzgfmVAtHHjzEqmseDg+oqZfcs2PXcgKR3g+EcJL4b8BtfOy9X8QXFVgK+jMMpcwGxGJ0f+FXEQbIC6Yp45/8Cgv/ACAS0wtvASrQwdmfc2/8IVnsAKqt27lms3fAf0RT+rfUdU0DXxEUHAwKqGoHyTYAmot6YrvJ7ndAbkXO4qFWA8OWLHTgrfRCSALIClV8IxTfQmUorgiAiAeiD+ZFMdJl6mckssqWZKEpuLb+JmPEfmONzExpuXU6i4hlKzAOWaHFmo+14ltg3EEGKCyDyR+9CadRoiGxv368Q+KrD4gEHgvJhl02yDcWwNwMByz4p8wsS+ilCY0kABbXr3ANE2TtFvJfqWgDNNRS9PqpU0pSpRORmkNTRb68kRhg7jUEFZyNYjDF6Xo9svXoReiDxbS5XZDWxwxWK2fWkfOorFK1kov4g5Sn36m1Vp7IOARmmcoNqj3CIWq5iLxKPnEGsJ5mEMAfUVAAo1EkoHD9pj6Q1sNlrXuV1F+B4fDcTy04DmUCjaqDUxxqsFl+CUMjtKPhw/ZN4DoUf42WQ5GuY6Rz1O7RxEUCyC6gtVfMJisPkRN3F+ZaPfKH8zIsZc4isUqrvMKhqoG9C+EL8y4nEe3BGlbt4IKxV3GoBUOxs83MAX4iIKn5mdcdwbpcVBsN6CBY7p2jwSoxaeW/jUqwVMdgujiFbDsZjsjJEqWYXXEDRvzLL0vBlzLyOCXQ3io7Ety6ePmXVPZ8zIsuX02arqZ+0zlSX/csaHAEVeYBEQiaMdYlIsEu+cteIrpFQ1ktL9/9lenWZb9JcPYCnk68Qz6oquoWEUywkdZNHDAKrUEqeK78Eqx4OYAse1QpjFXXtcuz5lQKYtOWpcIHRpHPxoRIy2vvEyebHM99QBhWWcCu+tTEYsAxBeVt5l7SnzCz1WivMZoAWrQO4aXnziIMAavDecOULM9j5MDtOqpKYomF5O/EBVCJRrXLLS3Mxdv3fK4P3GZOTEqY5p9wWgU3GyiknNkAXaiRfgYOTAoVfbERtFlhgH97lSFv75MqyGwaOrOfKQEVtQBdZsJui7tn5MvkwAFFDkMZiVnAltVfD4uXgGqWX90GwBWJnNC033CzS1el0u/k4hXzwZa86HxfuA6+xOhyvV9wMU1fPD5LEVES+TLzaw85P3UxoFPM4dp6ito+YKm0BFAhRiG3csvsEnwS5iNHIggMBX+A8/1LiUtSjo0fMTMwLsafefiLR6SrrmGKrZzEW3D4icL+I3BkxUZk4YrW3XiGo69EGj/mWVqZWvoS2Ebwtt/cCvugeWcjPiX1uGBC0fmxcTGZsHHmWDiBgzKEDvNd8RrUKl5f7EyWqt7lIhfh5lMhToVjAre6lk8Iae38wciENRuc6unPEZPBXvTVJtPTAoKkzyas6vjiIEm1yijjD4IKdy/MrciNb4f9YXazDPYw6WVWLnLwsXFK3DioWwJtiEqN2EpRn1GDPbJ5ikQdDL5C0B5V8BZXYauclaeAxMVxJbOTX2RCFPLUWpidZKhqg8pCqiheQbryj/K5vhIsGEEJpwSqxlH8n8pWJxKYMJOytS7JTCpF0n4j5RjGY+HDCYCrd7jOR9rnHxLpaHmOXooc0pGaQ7BVWwqOU/FVL9UAxerhJ2RiM2J+3HuVstgLQ7e1fOupQhvxnMELI1PC3X67mO34MV18Bf8AFy17v0nZ3E64jwnfliGTqfljH9EEsxXSfxGsB4/tVDD9H/dAa0tcwqtW9sE5bUXwP1VT5jsuWKNnEBmyzYLulH2ZzLBq40MBJvUUDA2rbbxfiK7QSMDgVl5IWEvi5gG6d4l7VxZi4MEn4kK5NCNDWFKxU4+UKvLceBCJYeItSV7qUaDkgYYsAviJtdLJqBAOTTVkMS6XaqpWbx7d2xoow2oc3FTlQ7EXTPDcLrqhFFM49sR1rUpaUvNpSquwHZrBFAcBIFEfFnzBMFxigVoF505rmVEgYMX7jnBqyx0148wVs5zGc1lhwRL32RpCLhjyD2XRK9VQ3MwvnECbgU4lYc8DRAG9gQH5MD5l9ATfO2CI0BY7c/1R8Qi1YAO1aJm+u92Zr41EXTTK38RUBkR0aqPWJum18cRIFuXaKm84iQjivDS//KgAsCmXEqk85NfcPlemXgPN/qKiRNPspChsLqrTxHKvblw0yrqg9yhAWVNJjlbOCpmXUZx45/K+oYxpu4KGvBg2JVu03bZiEgAOxrVB87iJc8vH57e5fEGrVEPXS08nf4hj3eCOpcZT/sGsvqXKVUFyvZGq506j5IBBuQr3f2h+lNMJdqH4c/mZadppAs/RGZwGy1o46iMISy1y+v5mkdhBcwfhMG6KNxdb3MbSOXiJ5g0eIPC+JVavUtFtxWA5iIuw3FTWEitdeY7lq1cOC2Xq/TxNWGsyGIiVkwLeeJei5VXBfviAFW1UYo1L54HZPiHo8fki3GTfTwWLsM02RFxHRb8f6mWY3w52lh81AMxwiqdg8D8xgFuXug/thu4C3Bt+C/cHv5h/mZaA6gUtvBqAJi/EfdVyBiCYAKbJBN7qyMowgiltYPmIWcalQzgcpoPuNSHFPd95LiE+GOTPlmmhWziAXgKJ09+8/EKQuoqCv+RIMDGNepe2yLlEBqHpRoe8QqgoB/4Mzqgi0BLq0f8AsoxgVWv/AKVLVlLuzdNQSrVSeOn1BSlHBx/SEWoq98UguhzyfMpmbaBfR/UusOIt15i1rtwvH/I9IxK4vnwVfzMZ8xfDodagVAthG04K5jwooAtzXb6r5hfm1S2y7q7W7xAqRS+QNZ250RdQuEUtWi9bYzD7HKWAvmNA8lkD5IHgqUSEwAOoAGEjMAFqxm75wRwAYFUHb7VJiC4iZBq5Vgpl5ts0jUOSCqwUqfZ+ZcUrpcbjDVRuW5ldjONMJuCLOdRsOhGADWNDudfBhhIFmdlYXEnxOEr5hoDZcR2jWBUBDMzrXRr4lQuBQsh2RUobUnTMQ1qwl48WIkJblxjTANo6miRVHDlflfiW66cDxrG5prXpy7rxlZb0FWFjeWryQqvYOg3r4/MbqlkfX7Z7wy5zM94TZcoVOUdariXntbsFGiCjdbrqyPWoa40Njmq/ionVSHXMzCHMAHU1YzMMq2mhvV+YYTXYz5Ye6itcluuiHIc0ngmqAqXKB3uojEnzCqLOSBqGi8lTsM/xMB7Zg/8Ak6iIhOjpYblKjmd/uLxVqtxvZ+E4YoZZbX5Jf5Uoaty7uXWFijT4qFoYQBdnmAEVNA9DwTLyMGI2NI001yRogocK2yuh3u73BLqFERaaUBi3mHVbbe7nS/mNfDX84iGKzliRBGAqU5tWBTkbxshaKkKPM2wYOC5LR91xLSZAm4jooZjyprG6DPwy0Axdc9F4lAGwwHF/1PC/3VMVVxEHZG4x+FAT7HOdPJCwDWfDM0cMvMwEXDpZnPI4FRPEASEqTb0XiPLVQtx2VT6YkB3gjPgJy6iFUeIuQWytjiNHK7jgh1B3kmszesbGGEB5BhFYZyTCgqra2QRtN1h5mIEOllwx7rmM3fx7z0Vp08RVyAdNY2Yc9RWwEtkUY35WPTCq9DzvLMxQLMHJAe+/RKCF0+JmYNSjfg7gK8AUIubf6SsAbc15j6qytAy3+CZ0qZb5RQLW3Jlg5Vcp9DLfNEF4FPyJdWAChFq3fo5H4h0ezf8AQlWBbWU+P+wcpWkDzNO2DsWwMPKWGXbbpcf3BCstuO9hiY1ZLdUyfmFWodz9Ubm5Ujz1dxnmonboBNwlehgmiMio851/cuWLdBuCgpyr+c5/EBgkoce+pjA1n+UqHCZkFD5FDo3KAOYAB8EQEmygirN0wri9HAP5lqOxlqA+cR0HZfKnfL/MHBebOVbWAFXkYiBXGxi7c+g1iNVKwqEzQtDsixG42TPhNjvf3F7amLw7eA5ZzKouVXpxgiSSukFvh5OyM6BowYq1vxuCBKAHlUbKoseZTGMh46hWLDWux9xE0bGJrTDIYjjnmXq9bcMYNGlOEaSYvXIWqyrtY7ejnlPEv/7nJa21i5lp1d5Sm6xFDbobPNtxtxhFdB7mdmCgLHM4fG185jWsq18oYumukshthYL3nmVYKjgXbAMAAW0eY58RWgEfixxaUzNt5fljnMZUqN5Wdy9bOVWx0RSmI9AFotinH5grSFycm30UfEs4alOXt8QDwgWh/LGULYS1bLMLQLL5lm1HNNOf4IFS3GYaNFd3z9TKPF6zNCYyxZ85BpBPpjNBZq4okSiaLL5wmpv4Lq8XxR1KTwgls/UARqFDnU1uISVBQX0TAwbS6DVS4lTnMsnoI3RlIXHy9fcCjxVt8u0WmZT2N7iEtlotxO5AOJwZobdddRGDeX3aqr5ekQooEt0u/G4r1mGCeA6g5y+1R+2FeLZiSJVsZ8uqio7yaWRwKTRI0sa2fYvMUd8B775cdcP5jsSUWeLwMBLvgJrMryN2vnEppselJTmkWwZJVQNcU2lLCyyXGIyiuyi2nHRcRreeTN6AoqiKyVTliE1hgV6P4JrNOY3OCJW7luPUOlzxcXzKJLZiAjzdvFDZHUTFuDKqKajG3l49wRQHGIeG4kseumarefxELGWHgQgBph8nabDoUQcP1LRbYBVld73EHdHQBmnOzMViacoJVC2kdxLWYmD1Chg4twKWgGd1mKZAcO9y0ZFhge6XjI31EwCxUVdaxCdWBLq8H8wDTX2s7DfUA77lTJwADwQUS+jRdeaXFxzZm1lIOfcAwPUQ20NFwhRlFog3xmzWtQFXCzDsctrfU7gXIsrY8OYMkrRWjtf+3CuyArw8EuQ7vMM8rsFadviLjn03y7/mI7l2mTZKRmAjYtAIGuFxcBcRd9wpVlZfmU9AwLXVRgNbvR6iyeAc8+5aZbFboipiYbr5J4NU4COeIeWCBEeQw+7uXkYat0dQAXlwmNihA0x0Qr5msc91KWiWtL9DCxkD9EhbsVlHEAY9sskc05eop9Ic98fFMopbrM0SosenDpuIMT8yrWb7Yy6lDXA2rvpxbuuGtxQg5rZpQINUEMIF6EDBUq1jX3BWQ7zRRgfLc4CB69SlShRlYjRGlea5ig2tnMY0O9RLKaYqSAlY0eGPgUNQfKr2Qsxpq5xUPARAY0AyWn+5eCbFL+fEHROuHRgV+oFLnJkmFHJgU9Lr6SoMMSgo0S3Bl7JS4/eoqFV1R/EuS1hK4iSui2nMsspyczSd1tE18SzNVAvhZBOhxdQ2bDsrFDvuCkAYBoqiqs+yBD7wzFEBxd64lHnQjZW4S6h+jtraaw7TPzEzWB3RVUAu2Iwn0HUOxiyrqAiHh2gCtt0Nl8Q0qjMDWB4C98wpvV7Wh8FHxPevlghRbbI41Gmdwe9vqcYl78RjtoX3Lr2XfZed4xiM+PGaXiDm4s9mUoAtZlwNFSwi34jbPCo3ULiAAMwzBZvfMbUwbCDhopcX9/zEazADXzCpr7iaiB2wOhe2oHYG9DUD2PbpgNKXJgiOnbiXtY8+PUoFk9wHtgEKc0j8Ji/dniEDCULt9QErvpu+uI6t5Bj8xYawBJ4BPqCiXGCz7jOuwBxBSVw3zBi2TyCOuNdKKdmbLd9Q1JszYWJ0UGUc2S1aajwpWxnctszClQXlA7FSurENSkC3p+5dKQN3uXihVjRaPvLK9B8VMoeYoDGsbMVXKAQsMM5gXXMRGJVqV/cobcjO+GdStImH3HzuKC3x8nmIKk3gJwrkQL1nJ11GrTOoa5CVK8Ym+Y7hhrECgBvGIS2GhggzRoy4gKMunUNaVdx1iA0zAJCuw9S6oLleYBAygu9aeJWbPLag6qXzeBMwEaNn8TIUwNA6qZek9CZXe33HrsgdpZFKgzg54AN0uqyZ5ggaphC5Mbb8Rnz8p4Spdv0MSmYyri35h7HSwwfMT0SRbC211qo7CBkFa8H6mTsYq7/UvmGnqVaVLAxYF/cGqjjR/W4EmS8eEskbsZrxBkJDPdzZc3DVst9rlRQVrDHkWtIkW7VDVx7Rg1nXfQ9EVl4e4gJ5DuBWANq6mSyqF15lKVKDEdeYHXppTMtsrIiBxz2/Az91DBI5aaX8IcbgMAIDMM0e+lv5gqAO1RlM/UaohvLMZEjqPk83ODo03KH2BSzDvYyr5h8vgKY9OcMCNRgHTHo8kgb9u+8V5lEeAuAK6LLTW2+AiZ8KAqdC0HIvDZCO0MHEugKvWE74yyistVmHyXcogW8FUV3+JZrQyuMSpK4MRVXzF7g8SiwqK2fcq5Xod4vPsNHmLVfnDDW5Og+TqM+nDfixvdfBMErtoewRhUscg8Ok9MAJdVvO0KBL6leSyApLvkgcF11C45XSjODj/TuFwm1AgibFMEu2AbK1FgjAzeXMaaltWajcmm8aYBz0BYwHa2KaXULAowA6FSzcf1qnKf1O4u2p6V2QKLKH/wCA1AlAZWqvDe4R4B0LUl3L2ALsxBeEbRryDu+4xtkeguVURmFKIKYxwM5TEKyDn6lNEBqq4gU7cg+parM2q9nj0E0EiZhmg8DuJrYeSAomrPExgTGX+T6g5VJd8bjCDTGdyxR3s1F7LqCVnEHMe41v+2G53wGvcsiTOwS7lh5E/wCzDk2ravxEADScxhoHKQAELwXmOkO+EtymdgqYEC9JBNWDLNSxPmrb74gsB4BQevMx1kconjti7IeKAvB4xmVrhoTxy/LS6hSF07pN97nA5rnq7mfmuKcZ1/0YsteBS3mXULLcA2C2GH8QFYM6ja8Aaa3tDqaBZnSKsByF12qJ1gEBMWzK7L0rwXChOQdMD6YGXKw/X1Dzd0cQ2kp3DQlgytwSDBzMlfc+aGGAcxbairzIkDKqZZCmWNQwi3K1g+15i1Jq04EDmwiOaZsfxCWCDxLkIGo7VhfMwNgcouoPLCGr6gm8Z+Qjv4N8MRYirIdsPd3gC9xxL26xKILA0SxCG/MFBo1d+YARLK6tGtA55LixgWFGPjcQZnG9cQppwaxlewDM9FJcSwJbadV/c4ap8y1rjf4l6M4IeIA/YQACDBhrwjYMF4P1BR6U1dfEJco6cmg/uVI5w5TmEMtfzHL64X4iasBcHmrPyy2NOyZkUwstzdaZhYkPiqmpZxf+4hBhOq1CJzMpDrlFFhj8pZbQrhhFSGN8S33ug59wGnfGvqDncbo42NdGreCVRjpGD4lU5UpoqEAu1hig8QlZVL9S+CO75IZxSliB5dzbpFgUbOH5lI4NQgpWqSt6yS4S5XQyCF2efyQOtyrs3R6MShe0LYJyMKtIck15DgpbrBfUEXiiHYvpRw79y6UdAUxv1Zp5mTCymzGK86+ISEEOEBsAZKoPugOzMEBs4LtSi/qYTNCsKza2i09O4DPAjZJXA9AbgM4BNOAXD/2HGzvxrnUsAV+fIRQHNJxjji13kRFQPZKm+IRkCCUSs4i0CydIY3A1TipSvJvEIFAyWnQigwvUHIIWrKu7lyOA/HcAVRwXEJbUCCQkQR2arzxGDLq5eDwYPiDGSM3DmW9PoEL7tlvjj5iChQ23UpatsQRxPAPkdy+BxGyJUDccAkArEvIslF0dhVnzLSTWoAqItZFKCzTUtKUjazQ5P5lCIYNUaPxLQ07lC4Y3MIG3G61+4wMUA2xDdjue8eJaUau6HmDIWtXUqkOouXM8X+r/AJlOA45IoU8KhADy5gqtHlxCqbY2dSuNHARxxMHqQb6wUbFe4urwqQioAubLzLhgOExDbu24bfmZTwCUkQ2bIip6vKWBezUCKG2ZaOAWXBYw3jUBKA0qp5rKb8wVC+wgkasjuEUcyrIeGC83MLbrGqzhgk0YjVJ14hTZXBxUo2YJSQagoZa1FKbjZpNRNLrhq82JeQTDFqYTVCwFYqi75BBOtI2rnOEdFHa8wmhzeUeyAKeiDV0LWAcUjCG3m4YzCnqy/wBQIeUBCNYrTzFTCuZfwY/cTGDdhv5mt9wLFAV4COsvsQBxD5UKIoYVq+Ff5uMATy4ZlOndlQksJ+wPMpqPB9N1HW1GdpIIzMf9JKIhHP3eLnES2A0nFr9VLXGQLXhRGlf2QFdWkeQTazfqUCKRGLotgx4gF9w/3mV5IwIZh6HKmC6iTHDOeD9zF7qXbF+4wbBcydAp0uJZUUoN5jWDbWOJeku4klrFC8uA/M54BuBMsDWZS75gEVbEXAyw1dc/1KHjKjGxlirTRcHc3EITDfKtZbH9QmScc0aIRHgZrwNPEQoAczBgNqFgoG1BVRZ5YjAVNJV3ajt5plZRd1PL+oBe5k6mJVmAH7j2XswZlhHqYHZcKEZgqoMCmANoDbUJwF54hgWE01ER1o9nVXFLSI2IcI4fiF8gMNaWcNal2LDVFBojka2f8izh0A4cU39sKomNr50ujvVX1AYWOa7IEC+SFzJUBwbyRKNYaGkww2KRuRqJQANmt7xA4GfMBlbKMYzeDExiYPAUKDNjRwjZvNF1SBu2pbeKg9w0IU19IMUI7Dfl/qJp9S7nwuEKwyuh6bj5rUGjhk2yhsGvxCQe+mpv6mBtHNzwYBlCmgmMccWTETK7Hm5ntmE0v7etQki0BQOgmeH3DdNeRiGVDnVS+o7bJUztLUvl4lT1wVtq5VpmilR8RkCyHUZDWWXOiKosMRhy1ECrHepwGDY9l0faQrgV0DBLMnFr17jBBGU5S1dUWUNUC5vUUYjebQACXdplP40SkDP7jZEEqKFXeIg5hLWZViAckRU4lvHRdZ3EMVkujfMNpZtDMKxpvOpTqDOjY4+2BGKimRZcCjWzKmS5CvENCLNbS4JVDfMejGQ5RQwtyvYe7Zx/cSTmaEQFPKLliN9wXiwuYodHxmNhLuiXHJ2QQTDSoSzGc3TEUAryIUGSZtEsDD3HxlBEUZ4uFoXghvfkmHdqjHYGM+blZ3NiLofz3CywS0qXwc1AoyVlKRV1yVn4i0SvC4KnI0YvqLBZ2RWIYPymMAMW0FgTG6I1pOScqdOUnX3CtC8tnNONIAONR/E2mtkaFZKGIbWK3uyqsKrPhxqE9QuDiUF18VqIWBF5eoQGqUY1cC5aWWMqQvl13GEHIYyEQKAUB88xIguuWF7LNwhEao/n8QOMLpbuimpYKUL7ea6Bx5gYo1rJbXqOtqcVXDM28PqUBQzGIM1qEGxbaetRAKDYrl6xG0q2y4ekgTfFMDf1ACWuLibQTVH7mtBuuZtAboso/uP31CHD5HUXppomTqnUVAHRsg6o9YuGKuN8XLMhb+IKFl21KgRT7H1KXZMVyVSXGtoccy6adPPEQ2Eo48Xx3nBDTb5gQMlDO/lBs4cMOy4jhBjj9wsrs4JVUb+PZVfmALG7VU0J+XcsksGw1OK6M8xRXVqB7dscqcpGKzZCDYdwzwnWY2YnKEIDvqN4Q8LTM+u59/EYX6hjC3pq4Vs9jqUFbYxEW64lUs4g4q28bh0ECMMRv7qw55IYU8XIqvxMc7dcS6kJ5l1Tlg4jbybTdupfUXsYVELrq8LYfDNXTLE4vHuKQzZrTpVd7/MNEBdC3eXHwQlxMULBb92q2d1icuICnQ0ux/UP2+EKGiOFJZ1wnK0ywoddYPHN3FnEC1fmMSsuqhf3jqGLvVpo57m/TVg7fHcu7acrNZg3ImNjGkCih17JUtwuGuuwvXpmO2B1jxcIVMSJUBguwKOEXgbfg5guoiAL4DglXwwGU+KjitXSufHMbdo0lQ57/wBmVy14VddmP3KRjM5jG4IvZhd1rmcxv0W6L/cpixYAlj5edTmEX0QTuXxyAjJTN9kFRAAVhazPpoQCEmeY+o6qvBp+RuOk2cWsmh01pIpaN4gigVT6hwFWL0IEbHF8l6mGQektgZSzN5uaYkmsuYKYPkzafqWBHYK3CAQLW+oHAJ7YVeG8ZJoin5aGPHuFSuc8PuACsZIKHa6i4HugMik6EPI8HqKjOFoFtQGWVx8tS9AAzCITMljuA5SU5hV3Xaxq6Yvgxe4wbIkAAzxKvbfMYKXYB+YPYBpznVf9qNWxqF6S0339HcEULpnsCnBSXniPM5xa3QUydhfEsOoCoSrrOH4ixmi0VLWRuJjLjIJ/MZq9crT7JeTZRVLPiGqgW3Jb/UX+LUtWnJ+a8TL1m1XvvuHn0iNtOtZKNkd+LBkC0xyem/uHMic7sOgDAcbYouS5Fx15+O4SFnNFAjznLPnzMddlYsDL2NHZ5hBwQHwJy7jYlwVQKNHPESgKFIirf6nHw2I4PiYq1WKMblvAtfIYt+TW5hUs4/iLYYKG14/EvMMHGay9HniM6wH0As8iua9E1zQCYN1XmayPIs+4dFNNhrGY2qEwHVSy+LxBu+fUyy5soHkvqIsoArNi1mKQLoTYDxCA8FNViu+WDRpobRg91L9cpImsmEEv7ptal5txHv8AiBlpbV8o9BQWQpYPMCIdcM/iLLAZ93AkbbWfiVUh0CuLcftgkMKGb/UuAAL1ps+9wJqm4mS9QF5FT80fiLtgyfSVFC1AIa4L8hf7h4b0woALyHf9TJnvFXBKjnm0bcv4P+StcAUNvteWKaZwDdTDTObd7/5AxVFNIwEqcxwiccRbS2W3NRbqNXeOZiWZfMBO0HuFg9Jl+iXvS36bf4hwd9th4mQTcJoL67llU/ctdLvmXQtj03sa9DA6ZSllenLQED8mWqoF5F5MeHuMisspaVywZgCxOEyNHzDA4FBbl0JnZZZVoQdYSnPqNHraPR+4tHaMIcl2sBV7g/BxC7m6TQv5xD/lGFwuOeaisruoUlD0meJb5VvCi1gbRbzsJjqqeoGS1W3k4V1EuMqqdzLAbG9bj04XFsRJbYFs1vB8VMAvgPJATRDGY/bOldS7ichqDQ87G+MH/IHUwgXapVXMrnS6bZfpyOBp3BltsLL7INzN2lAf3ARRjzZzwO98Rh0GHHr3OGCHI+DHxF3da8RUZcUtxh7xuJ1K+2R6YO6suhqvMPsLUts8UOWM9BolB22fg+CJRCnUEG9wCFNN01KcNYOv8Mx0FLDvqWjaZGlMI0GWRJZkQMxlE2jgF8EibmOa5gsy/wA3b+pUBK8MYggMfVwJeW4wrSqPuApVAHHEonJiCMKXVcRA7rGdEu/xXzL2QppO/UqWot6rx/JxB9ezOb2YD1KEqyDrxHBkGBcFcDuM4jTYi3ZNcwBn1ANUgFgOfUxA2JaPHcs7hZS+QRXqZX+ahWyqpsHPIS+NIUBT6g8Np73FeMnE6vuvuPQ8RCp1KAp9QVQVgJQt4PJKuZGVIF/59Qc8gRyRNHrcaS0SEVyTj4QcGOPmdD3rmLoAtpyXXEJJzVe8Nf8AYqOlEF28tpA0PplqfQ4fMA0+cR7N/wDyB1bUFCLvq2kctpVahbwLTAb13mJYCi2AUuPmUNNgqIiF1VVBi5AA0Jbw5dyzBsZOk3BUaEmTX9zDCHD+o2NB9fcsgCyNNzMh6ePxFqA2o1AwTCwWKKNKGV7plooLpXF+bhpP1mozylt6P9UYB3LaOWIqY12Ho9kUauBYQRALuKdiqxHhLKaT8uYvqcBa19cRCqLTmw/BzEJsKkJlQO8Z0InqHsND35fLMfTneIt9muAogMqy1dMQwrrPKdkQwLzzBNFZNnMZa1gW7gQQpyVBVtnlMEvDKuRep0V/MWA0g6JqqxzBrizC81cIMyK3EbAfiKJra7sDCSogXS786jiAxtgzdnwhouvAH4cHxCGMuIii4OoiNotpA5fEUhAFnAhV6pH4hJGVt7lmcTLbHVXn1iIhXLeYecOzc1T3KM+hB0Bj+WTk9EECtMrHbMjOekp7VBpgpb4gJZleVcBfj9TWg5gw9RVNMNAyuEhEb3q9kWUBw6AijYtY1xAcLVn5jVbVuxwXw7ik8ADbN9TLHdlu+hjfuUIrONOv5/EcQYNuUTMUe2nF+L84Io1C8xdXyXmOgOig3zddFviVECo4KM+83mFhCwCwX2d68RRhY3eosVXDQ9rgcQiMtuoUaQWtNjh9w3lF2kFHqt1nqBf7SWGoA4acHj+4Rur3cYhw3u4UgNbqPZ6L/BDsBNjK5OcruDowF4NSwLYF1q69+GXIsWawj1E0J1/UQJgq4YDnxcA8JQKX7/mPIBZ4DYe4LCEbQwP+MNDQNWH6GMW5ZchVGxDkNHI5aLhplmvpfjEopQRHazZoYbAumAjMdbhoVuIsEynJKdAC2u4zNTe3MahXHH/yPhAuBkKHsinY4Hctpi6vKu41+zCX4MTrJpqKL/JcdzGn2+DfEqJByx8Oj/YgdwQH7C19RPRgDLUCgotHIb+8QVyMpM1aj8fqAGoTVLC1eRxSeIoI2vemLX7g50iCliPemKgbXHmJNZObR77lQLvjRDqjtOBtiwlJ4mIHKu5QdVHXNiCQrbSiL73MuGRjCoC1RaIAbBXHmZYz1U/cqqKxYsQANRVxX9ykJygWeO9Qkoa4Gr6fMSHk00eIqjYUchXN5iBayl4cwpoMU+u2HSUJbPI+v3HRlYotcdLhqsgpo6PEvBtWixtfPxzBBZsICODC5fD33E1YWiiyvPOai7irhWS+Iq9L8p+US96//V5iAC0ouWutynoo7vg1rcdXejA//Ed6Ib1zBs1HqUmomxkNPf8ABB9VupfdM0D4WxK6uWQsMnIvjxCijegH5ffiBAFmAcN0t94z9xGSQFWgbOKjGwDtryrmKBVVowa54/MK2BEtQUqtqxruvcV1fKiLvfE3yMEqtDCtPhMcGHO8wdoKVuGVsFUBghAW16MQKg0VniMQqlhGBo4ZUEuWflUA8VgrK8sUVi6a65lAKU2F7ghsBvVy4yBZ0KYgxVrxCDdA7iqXIBX0SiAmXPwfz+pUTVnp65VPeJM3iCLUHEtqi4Co7HFQq0YvxGKyFWMDDO7qIBbwHmVCi4skdv8AMQZb3eZvcuqi5lPDiCTPAJQZr4mxmWDBuqhgAp2cQqhRsuADzUclWxtosD5hDpWjjipcJHEGht41CNKXsiahWBj/ALDkUDSRJW1Zs7jLefMBKMrTzqDmAWS8f0gY3csdysFU7dy1FDDepbJX2tyXxnqD3ElJTTVRnIewFHAcDHuKVNAqtypGxsq4+YFEVxc4sQ4N1j7g5Q1pFxb78+exjoVYovVavwyrNbZRLqq9ZXXUHFZkSpFKJQkKX4hMd2Qv93BikVSMyXoSlhe/k9Lg+YYQGKDRqBwUYOGCVjm+UAIwJEAozjdwI1d6sUZ3S9+4UA0R8ud8d+IUO0Ki2sGJvOUIwH8ZZUYbRT1dfl1BxWapEyKsEz2K2sQB0uoRgLgHHiC3lX4cdRGIsy6iJyEpMo9wAmsRUZ5lKxXAiVa6XGAjwSthLsyZQcDgzLZa8iC8LwKpXiGtxVIrcxgpoWsX77w/mBBCbZ7nl8EwQuhf0h+YkVZ7gRbXiKlAg+CkHiCUqe4w62WzOlKcQEW7thjJwW8QgNPt4h3sUFwGIGF4P7iXlrlgU5DF6+pS2kDwsTXYlZd1oYaWYTUG0CMCg84lG+pgI0YCLXpeDUKlxW6hSttrbiJGoYHnzGKqvH81DbFMMVLNIBsc7YohGDVRUSjpMeEFK4YKcmdx0GHioyIs94KrX21EyN3VnulydvEpvl4Hntu2/MoFN4wpkxrBdwwqsS4pxdf9/qZiZWg0Vxku7txmVThq3QMUwOMF8ErBfki1XN47fDAxgYsVc9GL+KO46o0SuhzLo2qoWKcMpUo7iKg45gKJRtYxTdPRwk4Z7y/UWMpBWsmuYk2BsQKjw4OS2/8A5FVsFjexv1mVSZxQrhKzFs2jgs/+RCsEsK7vKSo6nOYAd9lRUbqyLYzVuWEUKg4hdBtdjsbea+x8SzAmRbj3gdVzGywdxSzCAs2vFy61HcMKLgrj3L8ubzDBHDMhC0GCwKhHAjkvEVrMDiaUJyLxLg0FLMysTRgc2+YEUSrQOaNsxivYMfg9GfUQDqiUPATJkq7lwUt4gZXOPH/Il1U54jGlrMbbCYhWFi0rg7jmCFhXqCSscl7mUU0hAa7x1AsUHG5hleJtS5SsB93Ef4EDQVA8RGYBwsVBemmOBp5g4WvirygMI6vuWpfJnvqLiwaeqlvJruB80PNxpo1eZtcyloX8RzVuD/sMADxUxAd3AG7L4mIrgAQ3bfF3AQBeLA358ygGuUBxpeF19RWVuOAIN+rqqQyH2GVgUXo6U3h4gWvKDkVQctmn73VwHaqg1eMCl8zGbgw0Ms+Q1L3IIYTYTd8t/wAQGySy2zmnpY625ban+4jbIvRqVahLa1KuS/CgTb/R8koyGdAMfgjylQ8faOYuZr/P9zZG84Zc7lGmYqnJKohwrIV2U/1FNpE05PPXiJXW7CeOMQAios0VxnnOvQ9RLtWzDS5D3RZ5gxikWzZY3fdkRaziHIyZtnbyyhQ0m2JjVqmrOIybS6lWqqCEthg4XqMCFy4Mwn/gYszpipZOBdxteTdvEti5lOV6Idt+gMHa8B3Hzh12R2PB5lgIvy3GRfQ1EaaOJQSWHHMaYN4uIpSDoY2yuImAxmM5ZY1cJQpzGxLVRKwmv9/tRJbHUBxL3TzBRRcf9ljsGxJdqw9G7uWrHNTHhfUN5eKXUxPRcvUYKb9ylTqVmHl8j+EQlNNEdQUUtYGeZkJfVygAslwcBmvzDymtxqFBcNF3ecRVheYAKYTvcAJlR1AuNOirQXn1HGdKoX3xBFAVSpwsrMD1WuB9N95GZDtKAFCTGcEdJ8xLzBmCV0FcqvnbUC6KAeRwbw2XnFIdkzGq0FRfkfplFoo2PVefMKjxC3NRzerwEyYLAooBxWX1B2Abyv8ADqGAqzprT/L3BYgat2BcWd/iEuAt+S0+WF9BHhWdHkjoYC298xDjVn6qYCbOMUQyDlh5RATI7ycwAoZ3j6mKRRd8vAfMAUUtGZqg3yuPtLcVAavMBuimojqNgxgwS5eIbpm4jrsS+wDDBqU7EUUL5Uv5lSI1/EKrRfcSgtrqIzsKie5WM4ZcEvEEkvEDlheoFlsu81n/AJA+QWWuErgM+4b0lNSG/Q4IATdcFywzB/yNWWSoABwpzCeVpEAGsyxZydZmAlhuj2RINmu4AGvmODultuDzKArUxU15j3eZbRekuqqvUzRbjKrqBBA1LIArNEbNGJyUn1MEt6algLTwuoBYpUF0SLw+ItRbNGMqXdsBbMreNcoo891KnozBW8NRDp77jwBTeDMQTArA1xKwoahSWiyZNlOeag4yE8DFPcTfFprB/wDINtyO0z8ygc0BtSrPnE4zKfImY9iP4mXRrmg1/ECrAVcHvJ58RfMpVWsWqPkqGqbACxaOMcxZGgB7bx3kjBc0Eauv34mpqUKxT4/lqa16wBtqmufusSqogRaoFfcvWdTRaXkMMoKU1WCINkOoFOVZxAOrp+4V6eezQHn0Wwk41NVwm2jkeswhcTaLHw18fgt5gDmh5gBi+c3KRBqDdHUbAJKZt1iKXFwBfOeJkgCr+D/H/kASlWKlUqs3L2GdF9dZgZI8sMcwbLCXNTbuUSfqaHZ4PMN4cjaNDocEJqsXxG5zNwLcvGcwWBWVqyrYpbj3OUY/c+XlhAgy4FOGdVBUTbole7G7Ynd/xGhaDflhsmByVk/uOxgGkReBWIklWqsmCA3qIKYW7g8yUaLlBemYIsPgm6IEuReLmgbzFDSpzhip5gyyPm8ECwtncZkcKs5goQdGKi2ime4FCmnq4jAo9Qu4GHEIOEO0RQshySuKgl60crsKOPiXKufJBHtkSK5StrWQC5Ri33hFSHFhbyQ9lTu7Ya516zBNjEEHYec3H0MUmRW/KZSkIjLcFOHYhuIqmGkkMla0+S+YhUvBsKgq75wFeeYqFqgrjPO5lG5A6yfOhwQTBDNclcV2xYBhyEwOS1N64zAOYsrBb5cTWqsOeYNlvmALjNYSALVINeXQL+Y8IWuBh/r9QLUkW3luTAI3q/V5mRYZDdeDGA4wVxQsxiLoTwcVDpDKFtrbNKVeWWFlTzHTM7JBrRHNE32McoawMRK3ca95uKjMoWVxAApvpJUW5OJzkVIFBuY5Gp5AavgjgBATaTvmYJ4xhiNkCiY1dFy6h1jUBePiBTPBM38w8plJWrmnfiEdFsBfEsUrG1FrB6SZI5aljw+SWBQtlzGzJzhlaCvMSZMcQwHjPEyCvn1B1zL8zgAePPuHXQ02xibc9S4ezUUQhORG4DYHgGH8o4eWs+EwYLvdajpWnxBrH7I+Ii8M0gON5yEKDHnWfuFNKF4zcFwWOKhRVHIsXBmdc1qYBNKiYAVfolmbuGDxnsc/EXDO0bCBsMCF+Q9w2zYU5bs9rmM03uvRE0iZ6JVwSVS2gTZx6+Z9f6EdA0Sh5a2WFNqlNJq1TCpXBw077gd4PgW2fWLYSuqmLRSw6VMYu5zWRAW7ervRx3AGiw7LbfxAyUpOluE/MAy94Vf2iGajnwTmpS3bElVQDf3HJ1eKpizxjMaUxKjfAPdxjV+4mh1/QgujN8HBrtj2JQNkC3AW2+YepEAwhFb5yYRg98NQhxbdt4zCpDRCN4FUqgAGXRLvuWBMa8Cqr9SgYAwaunMS8/cQLc8QiLcaubphq7gi3uccQwaA4jdzZq3mLUtfpMWvnEDoZiHiFe9eYho+4g85gDC3UCwYU3zAOSwVz/UYTlduyr1KzTiOAHuPZDWa79xlfRlWa8Squ6uViVgFRKj23Vxi6BkdxkQuwjwEShbBKxi77gI8HZEKxrRcPFivMzUBv8Smkc9wqbVx03LgHLeRlTNGWUbxmKWljxAFZ5ghlMncyWns4hIj7LqaQPuKjIqqDcQxivph9XcomoHhs/g+YTulNirDZwET4pvcLpaHyn89dCRQFA12Xi8dWPxBg8AOhlB6fbm4UQJsC0v5ULizoYW4R69kSZqS0X3ZUZU+zjwTh4sNV0xq5S1iy1BeBuguEaoCWK9Cwy2PMBA1NAsDDiLoHQsozVJMC5urNJhw1m0dlRGz4r0fOLh484HSDijr8wyCjeiuju+fBDiTa0V095KbYdZYmdrtNIXOKq6Bdtx+2xmCmRM2+dZzD4cShcFFLBwf0jEwZMLka7RMbmUqF2Sqix3ki1x5gpnDcSg5TZ5ltNQ1lrO4DHnR/McLJmKlSnN0VMMXTU1lI8kwXNmpQMwTASN8XWHiITpSNxchHnMIF01Co5J3+ZgsPUQXAzaOLIoqtirqZyCFDZK4GZKHbmoVKX0b9xTtgO5bk3nzHAbBq2oDYvbqX0ZwtwwbcjUbLky8SrxHYAZqIhSrkmfhvZCBVYzC2QWZ9S2qjUclH7JSQ8UDqMl4LhiKvHBFNz01Bl6vKxbhlo5hR2h1CYXtrhKSwK2x9p+0EoL5Zn8pBxgy/a/UUBYj9V/MwuyDlWF/TK93UgFVAnQ32eZjX4Zduja+GV/JNLtxWPF1GWgOEns4SGk4dkvNDbTvxHSgKgAjuw9xTmAtjDnSND+LjWjlH9BLNq5WYAqNEOFOWfxLUIGHTBcifXiY/FrXsUbvGX4zqcgMgv7Xbw/EEZDmUN1/HmOKBpq/hQ1Ry1XepkhReuofZdaM5L3CZWFyjmuD8FSkulcQJq0s1WgjekNODrlV1ovJ1BWqBxTgoxFAYwKM+4eQhq+8y1fjqK3RZcW66HG41QRPCvucZOVMVaGhxHeM8w2zqVI76jNWxfDfWoQm6PMpS8/+IFoT2SjAsvyloOfEDQGyEYS7uNhW88R7MPjMDIZLL0wrkJTK3Dk1tDJKqLYM5UjiLRgGsQJALKAOB+4g5odF9QdCl5fEzTX/AMiKslYjkhpnfuIActR4qiG1v1NiLbWpY5gNv+8TCa1WemHhNn+fMWQgphRWvF/2CUSaPwShw1AAA8guGXj9/qKBZYIuKsfcukYQmSfnBAAxUy31AL9xBDa/vX8StUoIw05xiUxZUnVKDXySm8WFpar/AHD8lpmOMDavw0ywtOghsOE81xmNF1LJIDBmXQUrsikicEm9FEWWrj11r3H1sGF4BQay0/vqUyppjxq72JsSnEVEUgxClnePcOPW3mAVSOs311AxyQrwQWPH/IJLgJs1jfkOL53Hce9gKgubRa4UrRN9rCEneMBjgUVGKcDm1Ss+oWGw7Cv5KzdQDOW6q1bYUj842xEu7sS5z7s6h4a0Dx1NZ8Rhga7XFEepdW3PMz4JoHvUAq7MvzBwh8GJiKYWMdZ14g1EWccShVsc1rmI68eZTzJl7gUzbcG0OmLgVR8TYtfJEZcLhForV/8AYYXRgYr/AJgOzcEWglAA6mPjPasgOIGwyMHD6lqohseL/cQtKYuPEK8aNVmWCFV5pr+4lOVuoURdVDFD4VuPjs1CVVfMy8eDKVAjFcywEuHiLiwcsQKrjKDRK6fEADwWMB/zzBRBXPEtKDNblqw1qKlIXzMsl3CADEXMD3OBLGklmISvEsjjH6lXxFfhuMKMy/n+4ESEspqG4bJ0iY4eWWXiCsY2FPc99ZezECZRSwLtP+1CkMCteJWoeUuuQNG3qAsVyftjm+hA9DLV4eHs4PyeGPUB8tUcOesvPlAZ32E8zgufbPb1/wCZqy/zXbLt7jaCeTxEWM6uAQyF5+V5mLwYKWNiVMGM+o4aAUDCOHe48vMKpTHgZMP5uXMNlaKznOU8w7cOLm6XwS5MaXu5aAFRaN+/EK8EWOL8/FzHQLsI0bHJMFrQD/7KZJa/j3MjWM0zQQbKcXMVWiOAfuMlg/MqS9RULzN8BEsQHviW9HBcap5Ob3N0tG9eI04CbuYI/uFiruUjV6lsL2EG8dsagKYKzlcb3HEtY46mJbOe41xF9kCU0PJDmgMRJhehizYKdeI0tObhaJruoHNsxj9x2hK5uEoU5pYrMaY7JnlkiblLBfOogXd25jPZW35r+YlAJ04iumrEMAlrrzXXUUTFeWJWC/3MVFS+4aFQriIgHqVeiGg6CYWNMIQw81oyfzELRbpjKhQVYXuLCMglBepSOK6CjNtGjs+d7sLQ8/2lMKrGJl3lA6cQ1gBwSzRmKoh9I7r7fUuVqkGWC07saxUaX/AlvrEdPcM1Elitiu3n5hIEyXO6CUVKehHByRBtmlWxzfoEmbDSsVn22ZzjPOoqPoeweE9Esy3dJobtNZ0Oy5cohzV7+YKtXCrNOogdXGbiUh6zFdK5gFF1NXWszNXGAWrx/wDYYqOAqx8/iUApdajIXApYX4l5YKi1nKMDecUy0qzcuBUroggMh3N4LcYAmCpc4gsEi1EOhw4uYbhm1v6mIXLnmHO9sNvxmGisBuAq4wFdYuMbUivgz/ENQG7Gm5SHJRdERlRpwwoGnBUAZDxBNsjAqYDuXKhZgyNcMAFrNAbl/avVMpgWd3AK57R4AXuK2EZckq2kRo/jERItr3SvKotRqB08Q5aYPuJCzBxi2Uo/UEsfJEpj7Yra1EC8TuRzTOFu1JameIFoCm7V4jgjjOeSNxlnyp5Yg4SAnEY1VwDXg3j6lqpBFk+wOO4fCLsHXbu+CIgi5U5+jV+OIJA8ZVPklpZVwxRZ3FW/LJ8z0CNTNXw4gB2Xtg3lSsKrPMYYEF6iSgZUuWtS3GOeaqBd9Jo74iMkzbMlbVYLBxUWToqBQZOV4VmmoNaWCMhG3WCoeYMUbq+4JjI3a18QHJlGpayK4IkbL8wHrtthUCI5K19RFU1ZnNX6l8V0Q0oAvpVbuFd0XXuKqK2vmYiKYO1HnP8A4KJjBMpHFApdUMVSrcsp1fzDIJYwTFZjqCm8czd+Cb2vpLsSgsJZavuZBi7lkpQ3hmEBRqsSyoKxvz/mcPJQlO4JdunmXuR1LRKHTMNAuYS98wgrzUZEGsRrmjjMyAvbM2K/iVX/AERALStpVzbUJnJCTAY1mrlgtJUdmCNQPMHNgpZxdH+IdUtZN98SqFYDd8kPI0lHhinfiyIW6Lq+y4ZcaDrmK4XUta0x1BrEVVmtFEQblrHwf/MBljBVMruBIpNb6cYY8NC0fh3l4R3Y6MznhImoespXX7Jlu2Cve3xLjYhMAQOA15WWL2a+gFdVHC1lUaPrUWLjRFbhQt2P05OMkYM6Ba0pAbTL1fUv2SZd4jcCGs80yvqG9zVgcsUW5aYEKgAUd5+sw5Du7dwG3Ch+4yLLJCzYdWWY7lGzUvW9vpiwkNDivMGAAaD/AJ3BuUQ+8xpKpdxojUtdQ5awZittWgxz/NE2gFd1nGU5x84uFSxh5LbNJ6hssxfoF4UBeNjMXriYcVu84gPoM1A4QsGhR5MMVjNPcQaU/hFpLNrcEAUcJT0zSuAxBCGsGccMjzBrKzdFVVVAKjviACZ91DFipVhQuf8AUsL0DqvHuXghRf8AcwCwOznzGyDdYzDuz3AUWe5kdvEv8+6iwwOCNQsZyXj+5k6fMG0K3hhISL1qaMROeIYgpV3UQ1e3qUA7Cq/3cPlIUXL8XMvT/wBJYOBon/YAMLoc+5eNKQhvOYIlvfzH/iS5r6gvdjAb7hZwXKxqK9ChegX8pNIbL6mCnTfiAUWwttrDLTPQ2Ucg7MRKqW05jXm8viAB1td+gN+ncDKtlQ27cJ5p4hRgNtFE0nD+SFkaFWzyKbTvxHpQVzAPxM1SMCqy+GcV8DACUlAlqgusgLHrPcf1uORFXkVHmhkckV+yjlDiZpNcPmIZGTZD+mVCXEC5YOylz7iZFVRZSGaL04ldCC4qW2eSASWKeWpS2m+++4I8B0Q8gCa3G2+pWcERfoPncp0c9i7/ADLhGxViuNwxgAyKHnDyl3j1CJ2FLglBgkDfN75gsCz8in7jmhSvuYMV6jupi8e4gVE2apAhcauUAvcyiQgNMQVQAXg3FFarEd8ix8RWCFbz5qJbl8J+oBjkFzJX/wC0G7cviIFYcuU4/wCy4m/RGnkWRuMIBVqn5gERm91CAI28agt5F+oQdDUA0vuCYGWMQDx3Dc8hWYOLaiBzDDjxMqQ9RyhmXwn9B9wVoU5odzII6Niy/MJQibYf7uZpdDlGjFE4KL4goJU1WCoeyZHvuMp6oPnP8S8SvECgMm/Thliaw+GU/qUKgYXV6/IfuWYDNYOfMNRWV067pXAM+Rwy/wCG/SNOWX2WeZjgLcpuNvLqrzBbRxMMkqO35BDKoLAXrKh+LhEaG2Z4kluAHQPK6tY8DzGNqUXmNY4yhM5CUjSSbWoK1THxLgA6xuFq+2z5YiL4NmNPPzWuYrQW+Q4FbmZcZy/Z7grqJv5islpyMvDC/uKmE/Es9Rk3yf1OzIB1HEFXzOKDY4iTYUtRiuH4gRE0WJnZj8448whDCRuuJk6YBbL7NRCUmqKsxWGYBoWhazqPYGuIYuJjeoNPMwDydxis651MQEAllXvwyjziWcNJgjxHJq7ITllhfCI7aJjBN+Db+ImWeOqmFC3mABtO8TmCleoaHxzzCCsMF7IIiUolsoWD1BrA+yXeCIQbeMELgorcY1/cKqMdMsFvsI30he8wTVjIsNDuoepsfxMQc8ttEUotadDH4BNkFrNvHcbuVddyxYRs0XUxLK7yzKzLddQmWqYgl7qMSuoaQ7hABom/27lYhKi9oC3Lp+wbhXAD9HJ8XUDdfK4yU/uVKnhg1zRnTwipyU7W7XAvEXl27ggwrlq7uBAtAiC8ofOSL1K/gNsfoYKDaUivIr8/MRgHoIopjyGqH+VGcKVFoDYl8rL1GHuENYi3xtfmLmoNRR56uuHjHDBOiMlLTk5t/wDJW+AAMVjO4JGLW9iOVWW7DuB6WdPKBsNn3Nr1Cjoo1dJX8xKqBtFb33DF2thOC6znHcxCIuot9b1EdXN7crrrVSp0Ag39QtSH2u0Ooa+BauHqPcoZWLdDH1BQH8yyvmZYrEHbUWzH1MSww9x0tnAX9wxz8S+luOCEiq6vgzUEYs76nnhafUxgGeaNi/b6gFAKovT4c3MtQrblKDQeZgtAu4iNvJFDUP8Ag/UF12w+mmailiq7IwZu4tp6JUVzqE5Z4qCrD1Ms18S1Y8zDTMyugxEK+J7eg/LOcFkRycxLC7oWAN6A40/ELFSGExk3biWqh45laqFK/EVlxTPZCEpdGWVVixCVb/VTZKp+YfwSodsA4zn3A+ct93p4meglrY4aWV/RKC5jjJZq52bfJEdYnUAaqWmq3qjr268SvPauA9+I9AZCJfk7JSEHtBDL0BG2/V4i39XsC37RbdhKkCLG7Bv6N8Cyg7ZEaS73548QZ9T6BLF86YIpeGF0Sxi++CISswaadwU32QBqIFXhyebYKsoV3X368QBQNOsClbnCRSBjDZGto/x6jFZnbGzBR8AzRsQLB941zHQGYFRz1LtWwNJ/MVoIwLd5mY3QlKc4lWDRfcNV/wDElroJgMo7iKQFVviIDBvbDjLACDO5bXjUUeAYZ4uNRQabtoLfhXzEWGvQCvz/ALqA5Rhs6D+4qh4xphsl0RoIaGX4ixldA8RKtUL/AIlQETDgYiZCJm5kzIG+oFCUGXEFYAo7Oo6qoleWq1BDpUcKA9hCC8SpqsRAXBgGJJ+9vVdfUtV4rFYr7g7ABinEMFBoqiAAprO5vOLYgHJhzbCjzEvhWaCvmZOGWOvynwR+kyzLPc6n+51FHgm3O/4lblMJR5ioWrwCi+8MfkWoOrjKZXgB/mExjlRgWCEpDkArs94I1gMA8db7gdLd+V8X4lPAVvAVZfmIG3BE28G/uo8a1VaQ+tw4lgpgcqe1D43HXIo63YPy01kqBFiqbLr5a4dVAvLLXTa/3UzrCZZYzyYXGkC3mJScxbf1cM0ol7yt6hM0ck6cB358RzkZJRbPrN+IkNlWH1j23V+ZgxXzLIma5FbxgiOwM2+XW/UaSowKeeahjuBAqA+tpCQbGOayPmJJVsdXEucjZE4kOWC2ZtBzeIqXCdsUTZd3M6NuYTpLgCXn/wAQOwIQKn/cSyElBlQi09v6gZ5huYxl+X8EfBAeI+/A3caQVTuGFQQbRTUsI1lzz1HdbdhBDXq2Mx1TdTNzcqzhfEavanLL0FVohgillOmOBTXEsymIKVL0UsW3gXSxPzHRDQKHJDAQDmDWgQ0571HYFbWJLEtHNkqDwVWvmApebzAZq6gIXWoW0Q64O3liopXbZ+IUE3WYKOrn3iUXQAIvGnEcE1i4iCKQdIWgdKCxBVGpw+vbkS/4vGrAZ+4HHknSDitRwFqhdfwQ8VM0yhX8oAGyiUcpTkaBtaSNDw20GC8ws8CrqZchdj4wyt4sJgDlsoMQf66sZMBroqA8ljCcmnzUdCDV+ZRZZgUHwsRACmyA/VWcX8ESdxQn92CLBepem0M9CFQVsdk1jeY2K4Dms6asaPUUBYJiWqcA72mXtC3j6zuahUA4er/mBk1mrAuc/EJyttuTljMNVWb39SlLCvMovZc1ECZOINooE91AMsOKljZ1EvvhEwf+G2m8EV2BDPeX9pTLOZeusSme8xagYYJAPww6MYM0AF5yzn4gInKt6PEW4od3Hi9GZtqaGBUrmIbZc3W0qgmJY45nkICqhBbKLgrbKOq0mOgfxczVii83KKBbeYQ3cqWM09xQW6MjqoACuCuIAoDLLvP4lgbxUCFUb5lxpqXjPJiADuH7BgrgFEsmYRbzmKBSofZLkZcreWGCBV8FbPMymWp+B/MEhBgN1ww6uXJu/iArSLKrCcTKzr41AwyywyPfE4ABddeJkiYAhJ5f+wddHJab9uL6jGuUgujT8Et6MqrKDvzj6hxcnO8/xBCkFQ8q/JB4UoFi1gWuUhWX3LEchljhGOYtQxdNcf3C1gVbGcm/rUuLylxkLF45qvTZ1MT3unlRet/65URWGkCK1ZwG99R0FErdgrwrPvzEAgZuGuDxj7n/2Q==" data-filename="54349901_2064445256984439_1102740021548417024_n.jpg" class="note-float-right"></p><p>&nbsp;আজকের লেখার প্রসঙ্গ হল চিন্তা এবং নিজেকে দেখার দৃষ্টিভঙ্গি। আপনি \r\nআয়নায় দাঁড়িয়ে যদি দেখেন একটা মোটা কিংবা কালো মুখে কালচে দাগ, কিংবা চোখের\r\n নিচে কালি পড়া একটা কুৎসিত মানুষ তবে এই লেখা আপনার জন্য। সাইকোলজিতে একটা\r\n কথা আছে ‘<em>থটস আর অনলি থটস নট ফ্যাক্টস”</em>। এর মানে হল , চিন্তা \r\nবিষয়টা মস্তিষ্কের নিউরোনগুলোর তড়িৎরাসায়নিক সংকেতগুলো ছাড়া আসলে আর কিছু\r\n না। চিন্তা মানেই বিষয়টা সত্যি এমন না, আর এই চিন্তা নিয়ন্ত্রিত হয় বলে \r\nআমরা যা দেখি তা আসলে সত্যি না।</p><p>&nbsp;আজকের লেখার প্রসঙ্গ হল চিন্তা এবং নিজেকে দেখার দৃষ্টিভঙ্গি। আপনি \r\nআয়নায় দাঁড়িয়ে যদি দেখেন একটা মোটা কিংবা কালো মুখে কালচে দাগ, কিংবা চোখের\r\n নিচে কালি পড়া একটা কুৎসিত মানুষ তবে এই লেখা আপনার জন্য। সাইকোলজিতে একটা\r\n কথা আছে ‘<em>থটস আর অনলি থটস নট ফ্যাক্টস”</em>। এর মানে হল , চিন্তা \r\nবিষয়টা মস্তিষ্কের নিউরোনগুলোর তড়িৎরাসায়নিক সংকেতগুলো ছাড়া আসলে আর কিছু\r\n না। চিন্তা মানেই বিষয়টা সত্যি এমন না, আর এই চিন্তা নিয়ন্ত্রিত হয় বলে \r\nআমরা যা দেখি তা আসলে সত্যি না।</p><p>&nbsp;আজকের লেখার প্রসঙ্গ হল চিন্তা এবং নিজেকে দেখার দৃষ্টিভঙ্গি। আপনি \r\nআয়নায় দাঁড়িয়ে যদি দেখেন একটা মোটা কিংবা কালো মুখে কালচে দাগ, কিংবা চোখের\r\n নিচে কালি পড়া একটা কুৎসিত মানুষ তবে এই লেখা আপনার জন্য। সাইকোলজিতে একটা\r\n কথা আছে ‘<em>থটস আর অনলি থটস নট ফ্যাক্টস”</em>। এর মানে হল , চিন্তা \r\nবিষয়টা মস্তিষ্কের নিউরোনগুলোর তড়িৎরাসায়নিক সংকেতগুলো ছাড়া আসলে আর কিছু\r\n না। চিন্তা মানেই বিষয়টা সত্যি এমন না, আর এই চিন্তা নিয়ন্ত্রিত হয় বলে \r\nআমরা যা দেখি তা আসলে সত্যি না।</p><p>&nbsp;আজকের লেখার প্রসঙ্গ হল চিন্তা এবং নিজেকে দেখার দৃষ্টিভঙ্গি। আপনি \r\nআয়নায় দাঁড়িয়ে যদি দেখেন একটা মোটা কিংবা কালো মুখে কালচে দাগ, কিংবা চোখের\r\n নিচে কালি পড়া একটা কুৎসিত মানুষ তবে এই লেখা আপনার জন্য। সাইকোলজিতে একটা\r\n কথা আছে ‘<em>থটস আর অনলি থটস নট ফ্যাক্টস”</em>। এর মানে হল , চিন্তা \r\nবিষয়টা মস্তিষ্কের নিউরোনগুলোর তড়িৎরাসায়নিক সংকেতগুলো ছাড়া আসলে আর কিছু\r\n না। চিন্তা মানেই বিষয়টা সত্যি এমন না, আর এই চিন্তা নিয়ন্ত্রিত হয় বলে \r\nআমরা যা দেখি তা আসলে সত্যি না।</p><p>&nbsp;আজকের লেখার প্রসঙ্গ হল চিন্তা এবং নিজেকে দেখার দৃষ্টিভঙ্গি। আপনি \r\nআয়নায় দাঁড়িয়ে যদি দেখেন একটা মোটা কিংবা কালো মুখে কালচে দাগ, কিংবা চোখের\r\n নিচে কালি পড়া একটা কুৎসিত মানুষ তবে এই লেখা আপনার জন্য। সাইকোলজিতে একটা\r\n কথা আছে ‘<em>থটস আর অনলি থটস নট ফ্যাক্টস”</em>। এর মানে হল , চিন্তা \r\nবিষয়টা মস্তিষ্কের নিউরোনগুলোর তড়িৎরাসায়নিক সংকেতগুলো ছাড়া আসলে আর কিছু\r\n না। চিন্তা মানেই বিষয়টা সত্যি এমন না, আর এই চিন্তা নিয়ন্ত্রিত হয় বলে \r\nআমরা যা দেখি তা আসলে সত্যি না।</p><p><br></p>', 1, '2019-07-22');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE IF NOT EXISTS `blog_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(26) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`id`, `category_name`) VALUES
(1, 'Story'),
(2, 'Poem'),
(3, 'Education'),
(4, 'Tours & travels'),
(5, 'Fun & jokes'),
(6, 'Lifestyle');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comment`
--

CREATE TABLE IF NOT EXISTS `blog_comment` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `comment` varchar(556) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_comment`
--

INSERT INTO `blog_comment` (`id`, `blog_id`, `comment`, `created_by`, `created_date`) VALUES
(1, 2, 'this is a test comment.', 1, '2019-07-08'),
(2, 2, 'Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram. Qui sequitur', 1, '2019-07-08'),
(3, 5, 'jkhjkhjkhkjhjkh', 1, '2019-07-22');

-- --------------------------------------------------------

--
-- Table structure for table `district_govt`
--

CREATE TABLE IF NOT EXISTS `district_govt` (
  `id` int(10) unsigned NOT NULL,
  `districtname` varchar(56) NOT NULL,
  `divisionid` int(10) unsigned NOT NULL,
  `govt_district_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `district_govt`
--

INSERT INTO `district_govt` (`id`, `districtname`, `divisionid`, `govt_district_id`) VALUES
(1, 'Bagerhat', 4, NULL),
(2, 'Bandarban', 2, NULL),
(3, 'Barguna', 1, NULL),
(4, 'Barisal', 1, NULL),
(5, 'Bhola', 1, NULL),
(6, 'Bogra', 6, NULL),
(7, 'Brahmanbaria', 2, NULL),
(8, 'Chandpur', 2, NULL),
(9, 'Chapai Nababganj', 6, NULL),
(10, 'Chittagong', 2, NULL),
(11, 'Chuadanga', 4, NULL),
(12, 'Comilla', 2, NULL),
(13, 'Cox''s Bazar', 2, NULL),
(14, 'Dhaka', 3, NULL),
(15, 'Dinajpur', 7, NULL),
(16, 'Faridpur', 3, NULL),
(17, 'Feni', 2, NULL),
(18, 'Gaibandha', 7, NULL),
(19, 'Gazipur', 3, NULL),
(20, 'Gopalganj', 3, NULL),
(21, 'Habiganj', 8, NULL),
(22, 'Jamalpur', 5, NULL),
(23, 'Jessore', 4, NULL),
(24, 'Jhalokati', 1, NULL),
(25, 'Jhenaidah', 4, NULL),
(26, 'Joypurhat', 6, NULL),
(27, 'Khagrachhari', 2, NULL),
(28, 'Khulna', 4, NULL),
(29, 'Kishoregonj', 3, NULL),
(30, 'Kurigram', 6, NULL),
(31, 'Kushtia', 4, NULL),
(32, 'Lakshmipur', 2, NULL),
(33, 'Lalmonirhat', 7, NULL),
(34, 'Madaripur', 3, NULL),
(35, 'Magura', 4, NULL),
(36, 'Manikganj', 3, NULL),
(37, 'Maulvibazar', 8, NULL),
(38, 'Meherpur', 4, NULL),
(39, 'Munshiganj', 3, NULL),
(40, 'Mymensingh', 5, NULL),
(41, 'Naogaon', 6, NULL),
(42, 'Narail', 4, NULL),
(43, 'Narayanganj', 3, NULL),
(44, 'Narsingdi', 3, NULL),
(45, 'Natore', 6, NULL),
(46, 'Netrakona', 5, NULL),
(47, 'Nilphamari', 7, NULL),
(48, 'Noakhali', 2, NULL),
(49, 'Pabna', 6, NULL),
(50, 'Panchagarh', 7, NULL),
(51, 'Patuakhali', 1, NULL),
(52, 'Pirojpur', 1, NULL),
(53, 'Rajbari', 3, NULL),
(54, 'Rajshahi', 6, NULL),
(55, 'Rangamati', 2, NULL),
(56, 'Rangpur', 7, NULL),
(57, 'Satkhira', 4, NULL),
(58, 'Shariatpur', 3, NULL),
(59, 'Sherpur', 5, NULL),
(60, 'Sirajganj', 6, NULL),
(61, 'Sunamganj', 8, NULL),
(62, 'Sylhet', 8, NULL),
(63, 'Tangail', 3, NULL),
(64, 'Thakurgaon', 7, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `division_govt`
--

CREATE TABLE IF NOT EXISTS `division_govt` (
  `id` int(10) NOT NULL,
  `divisionname` varchar(50) NOT NULL,
  `govt_division_id` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division_govt`
--

INSERT INTO `division_govt` (`id`, `divisionname`, `govt_division_id`) VALUES
(1, 'Barisal', ''),
(2, 'Chittagong', ''),
(3, 'Dhaka', ''),
(4, 'Khulna', ''),
(5, 'Mymensingh', ''),
(6, 'Rajshahi', ''),
(7, 'Rangpur', ''),
(8, 'Sylhet', '');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL,
  `event_tilte` varchar(56) DEFAULT NULL,
  `place` varchar(26) DEFAULT NULL,
  `file` varchar(86) DEFAULT NULL,
  `details` varchar(1056) CHARACTER SET utf8 DEFAULT NULL,
  `status` smallint(2) DEFAULT '0',
  `date` date NOT NULL,
  `added_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `event_tilte`, `place`, `file`, `details`, `status`, `date`, `added_by`) VALUES
(1, 'test eventtest', 'shahbag', 'Hydrangeas.jpg', '<span style="background-color: rgb(255, 255, 0);">The .table class adds basic styling (light padding and only horizontal dividers) to a table:The .table class adds basic styling (light padding </span>and only horizontal dividers) to a table:The .table class adds basic styling (light padding and only horizontal dividers) to a table:The .table class adds basic styling (light padding and only horizontal dividers) to a table:The .table class adds basic styling (light padding and only horizontal dividers) to a table:The .table class adds basic styling (light padding and only horizontal dividers) to a table:', 1, '2019-07-20', 1),
(2, 'test 2', 'shahbag', 'Desert.jpg', 'The .table class adds basic styling (light padding and only horizontal dividers) to a table:The .table class adds basic styling (light padding and only horizontal dividers) to a table:dfdsfThe .table class adds basic styling (light padding and only horizontal dividers) to a table:The .table class adds basic styling (light padding and only horizontal dividers) to a table:The .table class adds basic styling (light padding and only horizontal dividers) to a table:', 1, '2019-07-20', 1),
(3, '#????????? #????_????????', 'shahbag', '', '৭৭) ওসি পাকুন্দিয়া- ০১৭১৩৩৭৩৪৯১<br>\r\n			৭৮) ওসি অষ্টগ্রাম- ০১৭১৩৩৭৩৪৯২<br>\r\n			৭৯) ওসি নেত্রকোনা- ০১৭১৩৩৭৩৫০৫<br>\r\n			৮০) ওসি বারহাট্টা- ০১৭১৩৩৭৩৫০৬<br>\r\n			৮১) ওসি কলমাকান্দা- ০১৭১৩৩৭৩৫০৭<br>\r\n			৮২) ওসি আটপাড়া- ০১৭১৩৩৭৩৫০৮<br>\r\n			৮৩) ওসি দুর্গাপুর- ০১৭১৩৩৭৩৫০৯<br>\r\n			৮৪) ওসি পূর্বধলা- ০১৭১৩৩৭৩৫১০<br>\r\n			৮৫) ওসি কেন্দুয়া- ০১৭১৩৩৭৩৫১১<br>\r\n			৮৬) ওসি মদন- ০১৭১৩৩৭৩৫১২<br>\r\n			৮৭) ওসি মোহনগঞ্জ- ০১৭১৩৩৭৩৫১৩<br>\r\n			৮৮) ওসি খালিজুরি- ০১৭১৩৩৭৩৫১৪<br>\r\n			৮৯) ওসি শেরপুর- ০১৭১৩৩৭৩৫২৩<br>\r\n			৯০) ওসি নকলা- ০১৭১৩৩৭৩৫২৪<br>\r\n			৯১) ওসি নলিতাবাড়ী- ০১৭১৩৩৭৩৫২৫<br>\r\n			৯২) ওসি শ্রীবর্দী- ০১৭১৩৩৭৩৫২৬<br>\r\n			৯৩) ওসি ঝিনাইগাতি- ০১৭১৩৩৭৩৫২৭<br>\r\n			৯৪) ওসি জামালপুর- ০১৭১৩৩৭৩৫৩৮<br>\r\n			৯৫) ওসি মেলান্দহ- ০১৭১৩৩৭৩৫৩৯<br>\r\n			৯৬) ওসি সরিষাবাড়ী- ০১৭১৩৩৭৩৫৪০<br>\r\n			৯৭) ওসি দেওয়ানগঞ্জ- ০১৭১৩৩৭৩৫৪১<br>\r\n			৯৮) ওসি ইসলামপুর- ০১৭১৩৩৭৩৫৪২', 1, '2019-07-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL,
  `image_path` varchar(56) DEFAULT NULL,
  `image_title` varchar(55) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_path`, `image_title`, `description`, `created_date`) VALUES
(1, '65899107_2806673166070527_2312533769396944896_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01'),
(2, '65714592_2806672649403912_7902909312639434752_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01'),
(3, '65908266_2806672586070585_2647712789543518208_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01'),
(4, '65538001_10217592212748953_8168948262787612672_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01'),
(5, '65395401_2254269471553313_8744474013595598848_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01'),
(6, '65887752_2211739158942865_6672708066338668544_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01'),
(7, '66083995_624333701386406_6311011170782281728_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01'),
(8, '65638239_624333224719787_321457879536107520_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01'),
(9, '65461655_624332214719888_7293807500376145920_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01'),
(10, '64743274_472185700206381_6125636555471060992_n.jpg', 'fb group images', 'This photo is taken from our social facebook group.', '2019-07-01');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL,
  `name` varchar(16) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `message` varchar(156) DEFAULT NULL,
  `date` date NOT NULL,
  `status` smallint(2) DEFAULT '0',
  `replied_text` varchar(256) DEFAULT NULL,
  `replied_by` int(2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `phone`, `message`, `date`, `status`, `replied_text`, `replied_by`) VALUES
(1, 'dsdsd', 2147483647, 'assasasjghadhghasd', '2019-07-09', 0, NULL, NULL),
(2, 'sdfsdf', 3444, 'asddssd', '2019-07-09', 0, NULL, NULL),
(3, 'dfdf', 34343434, '3434344                   ', '2019-07-09', 0, NULL, NULL),
(4, 'sdsd', 2147483647, 'sdsdsdsd', '2019-08-27', 1, NULL, NULL),
(5, 'test', 1686799560, 'test', '2019-08-28', 1, 'ffghfhgfhgf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `profession`
--

CREATE TABLE IF NOT EXISTS `profession` (
  `id` int(11) NOT NULL,
  `p_name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profession`
--

INSERT INTO `profession` (`id`, `p_name`) VALUES
(1, 'Accountant '),
(2, 'Air force '),
(3, 'Architect'),
(4, ' Army'),
(5, ' Banker'),
(6, ' Business Owner'),
(7, ' Chef Consultant'),
(8, ' Dentist '),
(9, 'Designer'),
(10, ' Dietitian '),
(11, 'Electrician '),
(12, 'Engineer'),
(13, ' Event Planner '),
(14, 'Farmer'),
(15, ' Film Maker'),
(16, ' Fire Service'),
(17, ' Government Service'),
(18, 'Hairdresser '),
(19, 'Journalist'),
(20, ' Judge'),
(21, 'Lawyer'),
(22, ' Mechanic'),
(23, ' Model'),
(24, ' Musician'),
(25, ' Navy'),
(26, ' Nutritionist'),
(28, ' Performing Artist '),
(29, 'Pharmacist'),
(30, ' Photographer'),
(31, ' Physician '),
(32, 'Physiotherapist'),
(33, ' Police'),
(34, ' Private Service'),
(35, ' Psychologist '),
(36, 'Scientist '),
(37, 'Surveyor '),
(38, 'Tailor '),
(39, 'Teacher'),
(40, ' Technician'),
(41, ' Trader '),
(42, 'Veterinarian'),
(43, ' Visual Artist'),
(44, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `profession_field`
--

CREATE TABLE IF NOT EXISTS `profession_field` (
  `id` int(11) NOT NULL,
  `field_name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profession_field`
--

INSERT INTO `profession_field` (`id`, `field_name`) VALUES
(1, 'Agro Industry'),
(2, ' Airline'),
(3, ' Travel & Tourism'),
(4, ' Automobile'),
(5, ' Industrial '),
(6, 'Machinery Banking '),
(7, 'Other Financial Institution '),
(8, 'Creative Arts'),
(9, 'Customer Support & Call Centre'),
(10, ' Education Energy'),
(11, ' Power & Fuel Engineering '),
(12, 'Architecture '),
(13, 'Export Import'),
(14, ' Finance & Accounting Fire'),
(15, ' Safety & Protection Food '),
(16, 'Beverage'),
(17, ' Garments & Textile'),
(18, ' Government'),
(19, 'Semi-Government'),
(20, ' Autonomous '),
(21, 'Hospital & Diagnostic Center '),
(22, 'Hotel Management,'),
(23, 'Hospitality & Tourism'),
(24, ' Human Resource Management & Adm'),
(25, 'IT & Telecommunication'),
(26, ' Manufacturing (Light & Heavy In'),
(27, 'Media & Event'),
(28, ' Merchandiser '),
(29, 'NGO '),
(30, ' Development Sector '),
(31, 'Pharmaceuticals '),
(32, 'Real Estate'),
(33, ' Research & Consultancy'),
(34, ' Sales & Marketing'),
(35, ' Security & Support'),
(36, ' Service Supply'),
(37, ' Chain Management & Logistic'),
(38, ' Others'),
(39, ' Legal & Regulatory Afaairs');

-- --------------------------------------------------------

--
-- Table structure for table `userrole`
--

CREATE TABLE IF NOT EXISTS `userrole` (
  `id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userrole`
--

INSERT INTO `userrole` (`id`, `role_name`, `description`) VALUES
(1, 'Admin', 'admin'),
(2, 'General Member', 'member'),
(3, 'Moderator', 'member');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `division` smallint(2) DEFAULT NULL,
  `district` smallint(2) DEFAULT NULL,
  `school` varchar(120) DEFAULT NULL,
  `gender` varchar(26) DEFAULT NULL,
  `edu_board` varchar(26) DEFAULT NULL,
  `present_loc` varchar(26) DEFAULT NULL,
  `fblink` varchar(156) DEFAULT NULL,
  `present_address` varchar(156) DEFAULT NULL,
  `permanent_address` varchar(156) DEFAULT NULL,
  `ac_group` text,
  `dob` date DEFAULT NULL,
  `Profession` varchar(26) DEFAULT NULL,
  `profession_field` varchar(26) DEFAULT NULL,
  `specialization` varchar(26) DEFAULT NULL,
  `personal_doc` varchar(120) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `verification_code` varchar(11) DEFAULT NULL,
  `blood_donor` smallint(2) DEFAULT NULL,
  `blood_group` varchar(16) DEFAULT NULL,
  `image_path` varchar(100) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `verified_by` int(2) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `division`, `district`, `school`, `gender`, `edu_board`, `present_loc`, `fblink`, `present_address`, `permanent_address`, `ac_group`, `dob`, `Profession`, `profession_field`, `specialization`, `personal_doc`, `status`, `role`, `verification_code`, `blood_donor`, `blood_group`, `image_path`, `member_id`, `verified_by`, `created_date`) VALUES
(1, 'shahed', 'cseshahed@gmail.com', '01686799560', 4, 35, 'ST high School2', NULL, '', 'Barisal', 'https://www.facebook.com/shahed.cse', 'sesad22', 'sdasdasd22', 'Arts', '1992-04-11', 'Tailor ', NULL, 'gcdgsd', 'sssssswsw.jpg', 1, 1, NULL, 1, '3', '22008384_1486217104764728_8884244791188773764_n.jpg', 99000001, NULL, '2019-08-26 08:12:50'),
(14, 'sadasd', 'aaas@gm.com', '345435345', 1, 3, 'Clg hggg sccjhgjhg', NULL, NULL, '', NULL, '', '', 'Science', '1970-01-01', 'Business,it,Survielence', NULL, NULL, 'Hydrangeas.jpg', 1, 2, '301414', NULL, '4', 'nofile.jpg', 99000003, NULL, '2019-08-26 06:48:14'),
(16, 'Fakhrul islamss', 'shahedsds@clouditbd.com', '01723455249', 2, 10, 'Clg hggg sccjhgjhg', 'male', 'barisal', 'Chandpur', NULL, '', '', 'Science', '1970-01-01', 'Business,it,Survielence', NULL, NULL, 'Desert.jpg', 1, 3, '323321', NULL, '5', 'nofile.jpg', 99000002, NULL, '2019-08-26 06:01:23'),
(17, 'sdsd', 'shahed@clouditbd.com', '01723455248', 1, 5, 'ST high School', 'male', 'jessore', 'Chittagong', 'hgfghfdhg', '', '', 'Science', '2019-08-20', 'Engineer', NULL, 'gcdgsd', 'New Picture.png', 1, 2, '424542', 1, '4', 'New Picture.png', 99, NULL, '2019-08-26 08:12:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allblood_group`
--
ALTER TABLE `allblood_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_comment`
--
ALTER TABLE `blog_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district_govt`
--
ALTER TABLE `district_govt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division_govt`
--
ALTER TABLE `division_govt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profession`
--
ALTER TABLE `profession`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profession_field`
--
ALTER TABLE `profession_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allblood_group`
--
ALTER TABLE `allblood_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `blog_comment`
--
ALTER TABLE `blog_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `district_govt`
--
ALTER TABLE `district_govt`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `division_govt`
--
ALTER TABLE `division_govt`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profession`
--
ALTER TABLE `profession`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `profession_field`
--
ALTER TABLE `profession_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `userrole`
--
ALTER TABLE `userrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
